#include "aoc.h"
#include <x86intrin.h>

typedef union pt
{
	struct { i16 X, Y; };
	u32 Bits;
} pt;

typedef struct map
{
	// assumed to be a power of two
	u32 Shift;
	idx Capacity;
	idx Occupancy;
	idx MaxPSL;
	pt *Pos;
	u8 *Mask;
} map;

static inline bool Valid(pt P)
{
	bool Valid = (P.X != INT16_MAX);
	return Valid;
}

#define SWAP(L, R) do { typeof(L) Tmp = (L); (L) = (R); (R) = Tmp; } while (0)

static inline idx
StartPosOf(map *Map, pt P)
{
	u64 Hash = P.Bits;

	u64 Shift = Map->Shift;
	Hash ^= Hash >> Shift;
	u64 Mul = (Hash * UINT64_C(11400714819323198485));
	u64 Result = (Mul >> Shift);
	assert(Result < (u64) Map->Capacity);

	return (idx)(Result);
}

static inline idx
NextCandidatePos(map *Map, idx Pos)
{
	idx Result = (Pos + 1) & (Map->Capacity - 1);
	return Result;
}

static inline idx
ProbeSequenceLengthOf(map *Map, pt P, idx Pos)
{
	idx PSL = -1;

	if (Valid(P))
	{
		idx StartPos = StartPosOf(Map, P);
		PSL = Pos - StartPos;

		if (PSL < 0)
		{
			PSL += Map->Capacity;
		}
	}

	return PSL;
}

static inline void
ShuffleDown(map *Map, idx CurrentPos, idx CurrentPSL)
{
	pt CurrentP = Map->Pos[CurrentPos];
	u8  CurrentMask  = Map->Mask[CurrentPos];
	while (CurrentPSL >= 0)
	{
		CurrentPSL += 1;
		idx NextPos = NextCandidatePos(Map, CurrentPos);
		pt NextP = Map->Pos[NextPos];
		idx NextPSL = ProbeSequenceLengthOf(Map, NextP, NextPos);
		if (NextPSL < CurrentPSL)
		{
			Map->MaxPSL = MAX(CurrentPSL, Map->MaxPSL);
			SWAP(Map->Pos[NextPos].Bits, CurrentP.Bits);
			SWAP(Map->Mask[NextPos], CurrentMask);
			CurrentPSL = NextPSL;
		}
		CurrentPos = NextPos;
	}
}

static inline u8 *
Get_Int(map *Map, pt P)
{
	assert(Map->Occupancy < Map->Capacity);
	idx StartPos = StartPosOf(Map, P);
	idx PSL = 0;
	idx HighestPSL = Map->MaxPSL + 1;
	for (idx Idx = StartPos;
	     PSL <= HighestPSL;
	     Idx = (PSL++, NextCandidatePos(Map, Idx)))
	{
		assert((PSL == 0) || (Idx != StartPos));
		pt SavedP = Map->Pos[Idx];
		if (!Valid(SavedP))
		{
			assert(Map->Occupancy < Map->Capacity);

			Map->Pos[Idx] = P;
			Map->Mask[Idx] = 0;
			Map->Occupancy += 1;
			Map->MaxPSL = MAX(PSL, Map->MaxPSL);
			return &Map->Mask[Idx];
		}
		else if (SavedP.Bits == P.Bits)
		{
			assert(SavedP.X == P.X && P.Y == SavedP.Y);
			return &Map->Mask[Idx];
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedP, Idx);
			assert(SavedPSL != -1);

			if (SavedPSL < PSL)
			{
				assert(Map->Occupancy < Map->Capacity);

				ShuffleDown(Map, Idx, SavedPSL);
				Map->Pos[Idx] = P;
				Map->Mask[Idx] = 0;
				Map->Occupancy += 1;
				Map->MaxPSL = MAX(PSL, Map->MaxPSL);
				return &Map->Mask[Idx];
			}
		}

	}
	assert(!"Should not happen");
	__builtin_unreachable();
}

static inline map Map_New(idx Capacity)
{
	map Map;

	Map.Shift     = sizeof(Capacity) * 8 - __builtin_ctzll(Capacity);
	Map.Capacity  = Capacity;
	Map.Occupancy = 0;
	Map.MaxPSL    = 0;
	Map.Pos       = calloc(Capacity, sizeof(*Map.Pos));
	Map.Mask      = calloc(Capacity, sizeof(*Map.Mask));

	for (idx I = 0;
	     I < Capacity;
	     ++I)
	{
		Map.Pos[I].X = INT16_MAX;
		Map.Pos[I].Y = INT16_MAX;
	}

	return Map;
}

static pt const Neighbors[8] =
{
	{.X = -1, .Y = -1},
	{.X =  0, .Y = -1},
	{.X =  1, .Y = -1},
	{.X = -1, .Y =  0},
	{.X =  1, .Y =  0},
	{.X = -1, .Y =  1},
	{.X =  0, .Y =  1},
	{.X =  1, .Y =  1},
};

static inline void Add(map *Map, pt Pos)
{

	LOOP(I, 8)
	{
		pt Delta = Neighbors[I];
		pt P = Pos;
		P.X += Delta.X;
		P.Y += Delta.Y;

		u8 *Mask = Get_Int(Map, P);
		idx Idx = Mask - Map->Mask;
		assert(Map->Pos[Idx].X == P.X && Map->Pos[Idx].Y == P.Y);
		u8 PMask = 1 << (7 - I);
		assert(!(*Mask & PMask));
		*Mask |= PMask;
	}
}

static inline u8 Get(map *Map, pt Pos)
{
	u8 *Mask = Get_Int(Map, Pos);
	idx Idx = Mask - Map->Mask;
	assert(Map->Pos[Idx].X == Pos.X && Map->Pos[Idx].Y == Pos.Y);
	return *Mask;
}

static inline void Move(map *M, pt Cur, pt Next)
{
	LOOP(I, 8)
	{
		pt Delta = Neighbors[I];
		pt P = Cur;
		P.X += Delta.X;
		P.Y += Delta.Y;

		u8 *Mask = Get_Int(M, P);
		u8 PMask = 1 << (7 - I);
		idx Idx = Mask - M->Mask;
		assert(M->Pos[Idx].X == P.X && M->Pos[Idx].Y == P.Y);
		assert(*Mask & PMask);
		*Mask &= ~PMask;
	}

	Add(M, Next);
}

typedef pt item;

#include "stack.h"

typedef struct data
{
	idx NumElves;
	pt *Elves;
} data;

static inline data ParseInput(str_view Text)
{
	data Result;

	stack Elves = Stack_New(128);

	idx X = 0;
	idx Y = 0;

	LOOP(I, Text.Length)
	{
		switch (Text.Content[I])
		{
			break;case '.': X += 1;
			break;case '#': Stack_Push(&Elves, (pt){.X = X, .Y = Y}); X += 1;
			break;case '\n': X = 0; Y += 1;
			break;default: assert(0);
		}
	}
	Result.NumElves = Elves.Size;
	Result.Elves    = Elves.Items;

	return Result;
}

static inline u8 *Try(map *W, pt Pos)
{
	u8 *Ptr = Get_Int(W, Pos);
	if (*Ptr)
	{
		*Ptr = 2;
	}
	else
	{
		*Ptr = 1;
	}
	return Ptr;
}

static inline idx Ceil2(idx X)
{
	idx Result;
	if (X <= 1)
	{
		Result = 1;
	}
	else
	{
		Result = INT64_C(1) << (64 - __builtin_clzl(X-1));
	}
	return Result;
}

enum direction
{
	NORTH,
	SOUTH,
	WEST,
	EAST,
};

static u8 const Mask[4] =
{
	[NORTH] = 0b0000'0111,
	[SOUTH] = 0b1110'0000,
	[WEST]  = 0b0010'1001,
	[EAST]  = 0b1001'0100,
};

static pt const Delta[4] =
{
	[NORTH] = {.X =  0, .Y = -1},
	[SOUTH] = {.X =  0, .Y =  1},
	[WEST]  = {.X = -1, .Y =  0},
	[EAST]  = {.X =  1, .Y =  0},
};

static PART(Part1)
{
	data Parsed = ParseInput(Text);

	map Occupied = Map_New(Ceil2(2 * 9 * Parsed.NumElves));
	map Wishlist = Map_New(Ceil2(2 * Parsed.NumElves));

	LOOP(I, Parsed.NumElves)
	{
		Add(&Occupied, Parsed.Elves[I]);
	}

	idx *MovingElves  = malloc(sizeof(*MovingElves) * Parsed.NumElves);
	pt *NextPositions = malloc(sizeof(*NextPositions) * Parsed.NumElves);
	u8 **Tries        = malloc(sizeof(*Tries) * Parsed.NumElves);
	LOOP(Round, 10)
	{
		idx MovingElfCount = 0;
		LOOP(I, Parsed.NumElves)
		{
			pt CurrentPos = Parsed.Elves[I];
			u8 Neighbors = Get(&Occupied, CurrentPos);
			if (Neighbors)
			{
				LOOP(DirIdx, 4)
				{
					idx Dir = (DirIdx + Round) % 4;
					if (!(Neighbors & Mask[Dir]))
					{
						pt NextPos = CurrentPos;
						NextPos.X += Delta[Dir].X;
						NextPos.Y += Delta[Dir].Y;
						Try(&Wishlist, NextPos);
						MovingElves[MovingElfCount] = I;
						NextPositions[MovingElfCount] = NextPos;
						MovingElfCount += 1;
						break;
					}
				}
			}
		}
		LOOP(I, MovingElfCount)
		{
			idx Elf = MovingElves[I];
			pt NextPos = NextPositions[I];
			Tries[I] = Get_Int(&Wishlist, NextPos);
			if (*Tries[I] == 1)
			{
				Move(&Occupied, Parsed.Elves[Elf], NextPos);
				Parsed.Elves[Elf] = NextPos;
			}
		}
		LOOP(I, MovingElfCount)
		{
			idx Idx = Tries[I] - Wishlist.Mask;
			Wishlist.Pos[Idx]  = (pt) { .X = INT16_MAX, .Y = INT16_MAX };
		}
		Wishlist.Occupancy = 0;
		Wishlist.MaxPSL = 0;
	}

	idx MinX = INT64_MAX, MaxX = INT64_MIN;
	idx MinY = INT64_MAX, MaxY = INT64_MIN;
	LOOP(I, Parsed.NumElves)
	{
		pt P = Parsed.Elves[I];
		MinX = MIN(MinX, P.X);
		MaxX = MAX(MaxX, P.X);
		MinY = MIN(MinY, P.Y);
		MaxY = MAX(MaxY, P.Y);
	}

	idx Result = (MaxX - MinX + 1) * (MaxY - MinY + 1) - Parsed.NumElves;
	fprintf(stderr, "%ld\n", Result);
	return 0;
}

static PART(Part2)
{
	data Parsed = ParseInput(Text);
	map Wishlist = Map_New(Ceil2(2 * Parsed.NumElves));
	map Occupied = Map_New(Ceil2(2 * 9 * Parsed.NumElves));

	LOOP(I, Parsed.NumElves)
	{
		Add(&Occupied, Parsed.Elves[I]);
	}

	idx *MovingElves  = malloc(sizeof(*MovingElves) * Parsed.NumElves);
	pt *NextPositions = malloc(sizeof(*NextPositions) * Parsed.NumElves);
	u8 **Tries = malloc(sizeof(*Tries) * Parsed.NumElves);
	idx Round = 0;
	idx MovingElfCount;
	do
	{
		MovingElfCount = 0;
		LOOP(I, Parsed.NumElves)
		{
			pt CurrentPos = Parsed.Elves[I];
			u8 Neighbors = Get(&Occupied, CurrentPos);
			if (Neighbors)
			{
				LOOP(DirIdx, 4)
				{
					idx Dir = (DirIdx + Round) % 4;
					if (!(Neighbors & Mask[Dir]))
					{
						pt NextPos = CurrentPos;
						NextPos.X += Delta[Dir].X;
						NextPos.Y += Delta[Dir].Y;
						Try(&Wishlist, NextPos);
						MovingElves[MovingElfCount] = I;
						NextPositions[MovingElfCount] = NextPos;
						MovingElfCount += 1;
						break;
					}
				}
			}
		}
		LOOP(I, MovingElfCount)
		{
			idx Elf = MovingElves[I];
			pt NextPos = NextPositions[I];
			Tries[I] = Get_Int(&Wishlist, NextPos);
			if (*Tries[I] == 1)
			{
				Move(&Occupied, Parsed.Elves[Elf], NextPos);
				Parsed.Elves[Elf] = NextPos;
			}
		}
		LOOP(I, MovingElfCount)
		{
			idx Idx = Tries[I] - Wishlist.Mask;
			Wishlist.Pos[Idx]  = (pt) { .X = INT16_MAX, .Y = INT16_MAX };
		}
		Wishlist.Occupancy = 0;
		Wishlist.MaxPSL = 0;
		Round += 1;
	} while (MovingElfCount);

	fprintf(stderr, "%ld\n", Round);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
