token_array TokenArray_New(idx Capacity)
{
	token_array New;

	New.Capacity = Capacity;
	New.Count = 0;
	New.Tokens = malloc(sizeof(*New.Tokens) * Capacity);

	return New;
}

void TokenArray_Push(token_array *Array, token Tok)
{
	if (Array->Count == Array->Capacity)
	{
		idx NewCapacity = Array->Capacity * 2;
		token *Tokens = realloc(Array->Tokens, sizeof(*Array->Tokens) * NewCapacity);
		assert(Tokens);

		Array->Capacity = NewCapacity;
		Array->Tokens = Tokens;
	}

	assert(Array->Count < Array->Capacity);

	Array->Tokens[Array->Count++] = Tok;
}

bool TokenArray_Drop(token_array *Array, idx Num)
{
	bool Result;
	if (Array->Count >= Num)
	{
		Array->Count -= Num;
		Array->Capacity -= Num;
		Array->Tokens += Num;
		Result = true;
	}
	else
	{
		Result = false;
	}

	return Result;
}
