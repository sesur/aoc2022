#include "aoc.h"
#include <assert.h>

typedef struct token
{
	idx Count;
	u8 *Items;
} token;

#include "token_array.h"

token_array ParseText(str_view Text)
{
	token_array Parsed = TokenArray_New(40);
	char const *Current = Text.Content;
	char const *End     = Text.Content + Text.Length;

	while (Current < End)
	{
		char const *Newline = Current;
		while (Newline != End && *Newline != '\n') Newline += 1;

		idx Count = Newline - Current;
		u8 *Items = malloc(Count);

		for (idx I = 0;
		     I < Count;
		     ++I)
		{
			char C = Current[I];
			if (C >= 'a')
			{
				Items[I] = C - 'a';
			}
			else
			{
				Items[I] = C - 'A' + 26;
			}
		}

		token Tok = (token) {Count, Items};
		TokenArray_Push(&Parsed, Tok);
		Current = Newline + 1;
	}

	return Parsed;
}

PART(Part1)
{
	token_array Parsed = ParseText(Text);

	idx PrioritySum = 0;

	for (idx Rucksack = 0;
	     Rucksack < Parsed.Count;
	    ++Rucksack)
	{
		token Tok = Parsed.Tokens[Rucksack];
		assert(!(Tok.Count & 1));
		idx Half = Tok.Count >> 1;
		idx Table[52] = {0};
		for (idx Item = 0;
		     Item < Tok.Count;
		     ++Item)
		{
			Table[Tok.Items[Item]] |= 1 << (Item < Half);
		}

		for (idx Priority = 0;
		     Priority < 52;
		     ++Priority)
		{
			if (Table[Priority] == 0b11)
			{
				PrioritySum += Priority + 1;
			}
		}
	}

	fprintf(stderr, "Priority Sum: %ld\n", PrioritySum);

	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseText(Text);

	idx GroupSum = 0;
	idx NumGroups = Parsed.Count / 3;
	
	for (idx Group = 0;
	     Group < NumGroups;
	     ++Group)
	{
		idx Table[52] = {0};
		for (idx Elf = 0;
		     Elf < 3;
		     ++Elf)
		{
			token Tok = Parsed.Tokens[3 * Group + Elf];
			for (idx Item = 0;
			     Item < Tok.Count;
			     ++Item)
			{
				Table[Tok.Items[Item]] |= 1 << Elf;
			}
		}

		idx NumFound = 0;
		for (idx Priority = 0;
		     Priority < 52;
		     ++Priority)
		{
			if (Table[Priority] == 0b111)
			{
				NumFound += 1;
				GroupSum += Priority + 1;
			}
		}

		assert(NumFound == 1);
	}

	fprintf(stderr, "Group Sum: %ld\n", GroupSum);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
