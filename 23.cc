#include <vector>
#include <unordered_map>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <cstdint>
#include <cstring>

#define LOOP(Var, Num) for (idx Var = 0; Var < (Num); Var += 1)

typedef ptrdiff_t idx;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;

struct str
{
	idx Length;
	char *Content;
};

struct str_view
{
	char const *Content;
	idx Length;
};


struct pt
{
	i16 X, Y;
	auto operator<=>(pt const& P) const = default;
};

template <>
struct std::hash<pt>
{
	inline std::size_t operator()(pt const& P) const noexcept
	{
		using std::size_t;
		using std::hash;

		i32 Val;
		memcpy(&Val, &P, sizeof(Val));


		return hash<i32>{}(Val);
	}
};

using map = std::unordered_map<pt, u8>;
using data = std::vector<pt>;

void Add(map& Map, pt Pos)
{
	pt Neighbors[8] =
	{
		{-1, -1},
		{ 0, -1},
		{ 1, -1},
		{-1,  0},
		{ 1,  0},
		{-1,  1},
		{ 0,  1},
		{ 1,  1},
	};

	LOOP(I, 8)
	{
		pt Delta = Neighbors[I];
		pt P = Pos;
		P.X += Delta.X;
		P.Y += Delta.Y;

		u8 &Mask = Map[P];
		u8 PMask = 1 << (7 - I);
		assert(!(Mask & PMask));
		Mask |= PMask;
	}
}

u8 Get(map& Map, pt Pos)
{
	return Map[Pos];
}

void Move(map& M, pt Cur, pt Next)
{
	pt Neighbors[8] =
	{
		{-1, -1},
		{ 0, -1},
		{ 1, -1},
		{-1,  0},
		{ 1,  0},
		{-1,  1},
		{ 0,  1},
		{ 1,  1},
	};

	LOOP(I, 8)
	{
		pt Delta = Neighbors[I];
		pt P = Cur;
		P.X += Delta.X;
		P.Y += Delta.Y;

		u8 &Mask = M[P];
		u8 PMask = 1 << (7 - I);
		assert(Mask & PMask);
		Mask &= ~PMask;
	}

	Add(M, Next);
}

data ParseInput(str_view Text)
{
	std::vector<pt> Result;
	Result.reserve(128);

	i16 X = 0;
	i16 Y = 0;

	LOOP(I, Text.Length)
	{
		switch (Text.Content[I])
		{
			break;case '.': X += 1;
			break;case '#': Result.push_back(pt{X, Y}); X += 1;
			break;case '\n': X = 0; Y += 1;
			break;default: assert(0);
		}
	}

	return Result;
}

void Try(map& W, pt Pos)
{
	u8& Ptr = W[Pos];
	if (Ptr)
	{
		Ptr = 2;
	}
	else
	{
		Ptr = 1;
	}
}

bool Check(map& W, pt Pos)
{
	u8 Val = W.at(Pos);
	assert(Val);
	return Val == 1;
}

void Reset(map& M)
{
	M.clear();
}

enum direction
{
	NORTH,
	SOUTH,
	WEST,
	EAST,
};

#define PART(Name) int Name(str_view Text)
typedef PART(part);


PART(Part1)
{
	data Parsed = ParseInput(Text);

	map Wishlist;
	Wishlist.reserve(2 * Parsed.size());
	map Occupied;
	Occupied.reserve(2 * 9 * Parsed.size());

	u8 Mask[4] =
	{
		[NORTH] = 0b0000'0111,
		[SOUTH] = 0b1110'0000,
		[WEST]  = 0b0010'1001,
		[EAST]  = 0b1001'0100,
	};
	pt Delta[4] =
	{
		[NORTH] = {  0, -1 },
		[SOUTH] = {  0,  1 },
		[WEST]  = { -1,  0 },
		[EAST]  = {  1,  0 },
	};
	for (pt Pos : Parsed)
	{
		Add(Occupied, Pos);
	}

	std::vector<idx> MovingElves;
	MovingElves.reserve(Parsed.size());
	std::vector<pt> NextPositions;
	NextPositions.reserve(Parsed.size());
	LOOP(Round, 10)
	{
		idx MovingElfCount = 0;
		LOOP(I, Parsed.size())
		{
			pt CurrentPos = Parsed[I];
			u8 Neighbors = Get(Occupied, CurrentPos);
			if (Neighbors)
			{
				LOOP(DirIdx, 4)
				{
					idx Dir = (DirIdx + Round) % 4;
					if (!(Neighbors & Mask[Dir]))
					{
						pt NextPos = CurrentPos;
						NextPos.X += Delta[Dir].X;
						NextPos.Y += Delta[Dir].Y;
						Try(Wishlist, NextPos);
						MovingElves.push_back(I);
						NextPositions.push_back(NextPos);
						break;
					}
				}
			}
		}
		LOOP(I, MovingElves.size())
		{
			idx Elf = MovingElves[I];
			pt NextPos = NextPositions[I];
			if (Check(Wishlist, NextPos))
			{
				Move(Occupied, Parsed[Elf], NextPos);
				Parsed[Elf] = NextPos;
			}
		}
		MovingElves.clear();
		NextPositions.clear();
		Wishlist.clear();
	}

	i16 MinX = INT16_MAX, MaxX = INT16_MIN;
	i16 MinY = INT16_MAX, MaxY = INT16_MIN;
	for (pt P : Parsed)
	{
		MinX = std::min(MinX, P.X);
		MaxX = std::max(MaxX, P.X);
		MinY = std::min(MinY, P.Y);
		MaxY = std::max(MaxY, P.Y);
	}

	idx Result = (MaxX - MinX + 1) * (MaxY - MinY + 1) - Parsed.size();
	fprintf(stderr, "%ld\n", Result);
	return 0;
}

PART(Part2)
{
	data Parsed = ParseInput(Text);

	map Wishlist;
	Wishlist.reserve(2 * Parsed.size());
	map Occupied;
	Occupied.reserve(2 * 9 * Parsed.size());

	u8 Mask[4] =
	{
		[NORTH] = 0b0000'0111,
		[SOUTH] = 0b1110'0000,
		[WEST]  = 0b0010'1001,
		[EAST]  = 0b1001'0100,
	};
	pt Delta[4] =
	{
		[NORTH] = {  0, -1 },
		[SOUTH] = {  0,  1 },
		[WEST]  = { -1,  0 },
		[EAST]  = {  1,  0 },
	};
	for (pt Pos : Parsed)
	{
		Add(Occupied, Pos);
	}

	std::vector<idx> MovingElves;
	MovingElves.reserve(Parsed.size());
	std::vector<pt> NextPositions;
	NextPositions.reserve(Parsed.size());
	idx Round = 0;
	do
	{
		MovingElves.clear();
		NextPositions.clear();
		Wishlist.clear();
		idx MovingElfCount = 0;
		LOOP(I, Parsed.size())
		{
			pt CurrentPos = Parsed[I];
			u8 Neighbors = Get(Occupied, CurrentPos);
			if (Neighbors)
			{
				LOOP(DirIdx, 4)
				{
					idx Dir = (DirIdx + Round) % 4;
					if (!(Neighbors & Mask[Dir]))
					{
						pt NextPos = CurrentPos;
						NextPos.X += Delta[Dir].X;
						NextPos.Y += Delta[Dir].Y;
						Try(Wishlist, NextPos);
						MovingElves.push_back(I);
						NextPositions.push_back(NextPos);
						break;
					}
				}
			}
		}
		LOOP(I, MovingElves.size())
		{
			idx Elf = MovingElves[I];
			pt NextPos = NextPositions[I];
			if (Check(Wishlist, NextPos))
			{
				Move(Occupied, Parsed[Elf], NextPos);
				Parsed[Elf] = NextPos;
			}
		}
		Round+=1;
	} while (MovingElves.size());

	fprintf(stderr, "%ld\n", Round);
	return 0;
}

// PART(Part2)
// {
// 	data Parsed = ParseInput(Text);
// 	map Wishlist = Map_New(2 * Parsed.NumElves);
// 	map Occupied = Map_New(2 * 9 * Parsed.NumElves);

// 	u8 Mask[4] =
// 	{
// 		[NORTH] = 0b0000'0111,
// 		[SOUTH] = 0b1110'0000,
// 		[WEST]  = 0b0010'1001,
// 		[EAST]  = 0b1001'0100,
// 	};
// 	pt Delta[4] =
// 	{
// 		[NORTH] = {  0, -1 },
// 		[SOUTH] = {  0,  1 },
// 		[WEST]  = { -1,  0 },
// 		[EAST]  = {  1,  0 },
// 	};
// 	LOOP(I, Parsed.NumElves)
// 	{
// 		Add(&Occupied, Parsed.Elves[I]);
// 	}

// 	idx *MovingElves  = malloc(sizeof(*MovingElves) * Parsed.NumElves);
// 	pt *NextPositions = malloc(sizeof(*NextPositions) * Parsed.NumElves);
// 	u8 **Tries = malloc(sizeof(*Tries) * Parsed.NumElves);
// 	idx Round = 0;
// 	idx MovingElfCount;
// 	do
// 	{
// 		MovingElfCount = 0;
// 		LOOP(I, Parsed.NumElves)
// 		{
// 			pt CurrentPos = Parsed.Elves[I];
// 			u8 Neighbors = Get(&Occupied, CurrentPos);
// 			if (Neighbors)
// 			{
// 				LOOP(DirIdx, 4)
// 				{
// 					idx Dir = (DirIdx + Round) % 4;
// 					if (!(Neighbors & Mask[Dir]))
// 					{
// 						pt NextPos = CurrentPos;
// 						NextPos.X += Delta[Dir].X;
// 						NextPos.Y += Delta[Dir].Y;
// 						Tries[MovingElfCount] = Try(&Wishlist, NextPos);
// 						MovingElves[MovingElfCount] = I;
// 						NextPositions[MovingElfCount] = NextPos;
// 						MovingElfCount += 1;
// 						break;
// 					}
// 				}
// 			}
// 		}
// 		LOOP(I, MovingElfCount)
// 		{
// 			idx Elf = MovingElves[I];
// 			pt NextPos = NextPositions[I];
// 			if (*Tries[I] == 1)
// 			{
// 				Move(&Occupied, Parsed.Elves[Elf], NextPos);
// 				Parsed.Elves[Elf] = NextPos;
// 			}
// 			idx Idx = (Tries[I] - Wishlist.Mask);
// 			Wishlist.Pos[Idx].X = INT16_MAX;
// 			Wishlist.Pos[Idx].Y = INT16_MAX;
// 		}
// 		Wishlist.Count = 0;
// 		Round += 1;
// 	} while (MovingElfCount);

// 	fprintf(stderr, "%ld\n", Round);
// 	return 0;
// }

part *Parts[] = { &Part1, &Part2 };

#include <unistd.h>
#include <time.h>
#define ARRAYCOUNT(Arr) (sizeof(Arr) / sizeof(*(Arr)))

str
LoadFile
(
	str_view Path
)
{
	str Result = {0};

	FILE *File = fopen(Path.Content, "r");

	if (File)
	{
		int Error = fseek(File, 0, SEEK_END);

		assert(!Error);

		long Length = ftell(File);

		rewind(File);

		char *Buffer = (char *) malloc(Length);

		int Read = fread(Buffer, 1, Length, File);

		assert(Read == Length);

		Result.Content = Buffer;
		Result.Length = Length;
	}
	else
	{
		perror(NULL);
	}

	return Result;
}

str_view
FromCStr
(
	char *Str
)
{
	str_view Result;
	Result.Content = Str;
	Result.Length = strlen(Str);
	return Result;
}

int
main
(
	int Count,
	char **Arguments
)
{
	int Result;
	if (Count == 3)
	{
		idx Part = *Arguments[1] - '1';
		str Text = LoadFile(FromCStr(Arguments[2]));
		str_view View = (str_view) { .Content = Text.Content, .Length = Text.Length };
		assert(Part >= 0 && Part < (idx) ARRAYCOUNT(Parts));
		struct timespec Start, End;
		assert(clock_gettime(CLOCK_MONOTONIC, &Start) == 0);
		asm volatile("": : :"memory");
		Result = Parts[Part](View);
		asm volatile("": : :"memory");
		assert(clock_gettime(CLOCK_MONOTONIC, &End) == 0);
		idx Sec = End.tv_sec - Start.tv_sec;
		idx Nano = End.tv_nsec - Start.tv_nsec;
		idx Time = Sec * 1e9 + Nano;
		idx NSec = Time % 1000; Time /= 1000;
		idx USec = Time % 1000; Time /= 1000;
		idx MSec = Time % 1000; Time /= 1000;
		idx DSec = Time % 1000; Time /= 1000;
		idx Min = Time;

		printf("%02ld:%02ld.%03ld-%03ld'%03ld\n", Min, DSec, MSec, USec, NSec);
	}
	else
	{
		assert(Count > 0);
		printf("Usage: '%s <part> <file>'\n", Arguments[0]);
		Result = -1;
	}

	return Result;
}
