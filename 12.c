#include "aoc.h"

typedef struct grid
{
	idx Height, Width;
	char *Data;
} grid;

typedef struct pt
{
	idx X, Y;
} pt;

typedef struct problem
{
	grid Grid;
	pt Start, End;
} problem;

problem ParseInput(str_view Text)
{
	str_view Line = NextLine(&Text);
	idx Width = Line.Length;
	idx Capacity = Width * Width;
	char (*Data)[Width] = malloc(Capacity);
	idx Height = 0;
	idx MaxHeight = Width;
	pt Start = {-1, -1};
	pt End   = {-1, -1};
	for (;
	     Line.Length > 0;
	     Line = NextLine(&Text))
	{
		if (Height == MaxHeight)
		{
			MaxHeight *= 2;
			Data = realloc(Data, MaxHeight * Width);
			assert(Data);
		}

		assert(Line.Length == Width);
		for (idx X = 0;
		     X < Width;
		     X += 1)
		{
			char Entry = Line.Content[X];
			if (Entry == 'S')
			{
				assert(Start.X == -1);
				Start.X = X;
				Start.Y = Height;
				Data[Height][X] = 'a';
			}
			else if (Entry == 'E')
			{
				assert(End.X == -1);
				End.X = X;
				End.Y = Height;
				Data[Height][X] = 'z';
			}
			else
			{
				Data[Height][X] = Entry;
			}
		}

		Height += 1;
	}

	problem Result;

	Result.Grid = (grid) {.Height = Height, .Width = Width, .Data = (char *) Data };
	Result.Start = Start;
	Result.End   = End;

	return Result;
}

idx FindPlace(idx Count, idx Sorted[Count], idx Entry)
{
	if (Count == 0) return 0;
	idx Start = 0;
	idx End = Count;

	while (Start < End)
	{
		idx Middle = (Start + End) / 2;
		if (Entry < Sorted[Middle])
		{
			End = Middle;
		}
		else if (Entry > Sorted[Middle])
		{
			Start = Middle + 1;
		}
		else
		{
			idx Current = Middle;
			while (Current < Count - 1 && Sorted[Current+1] == Entry)
				Current += 1;
			return Current;
		}
	}
	assert(Start == End);

	return Start;
}

PART(Part1)
{
	problem Parsed = ParseInput(Text);

	grid Grid = Parsed.Grid;
	pt Start = Parsed.Start;
	pt End   = Parsed.End;

	char (*Data)[Grid.Width] = (char (*)[Grid.Width])Grid.Data;

	idx Capacity = 512;
	idx Size = 0;
	idx Bottom = 0;
	pt *Stack = malloc(Capacity * sizeof(*Stack));
	idx *Val = malloc(Capacity * sizeof(*Val));
#define DEQUEUE() Bottom++
#define ENQUEUE(Pt, Entry) do {						\
		idx NewEntry = (Entry);					\
		pt NewPt = (Pt);					\
		if (Size == Capacity)					\
		{							\
			if (Bottom == 0)				\
			{						\
				Capacity *= 2;				\
				Stack = realloc(Stack, Capacity * sizeof(*Stack)); \
				Val = realloc(Val, Capacity * sizeof(*Val)); \
			}						\
			else						\
			{						\
				for (idx I = Bottom; I < Size; ++I)	\
				{					\
					Stack[I-Bottom] = Stack[I];	\
					Val[I-Bottom] = Val[I];		\
				}					\
				Size -= Bottom;				\
				Bottom = 0;				\
			}						\
		}							\
		idx Place = Size;					\
		Stack[Place] = NewPt;					\
		Val[Place] = NewEntry;					\
		Size += 1;						\
	} while (0)

	idx (*CurrentBest)[Grid.Width] = malloc(Grid.Height * Grid.Width * sizeof(idx));
	for (idx Y = 0;
	     Y < Grid.Height;
	     Y += 1)
	{
		for (idx X = 0;
		     X < Grid.Width;
		     X += 1)
		{
			CurrentBest[Y][X] = INT64_MAX;
		}
	}

	ENQUEUE(Start, 0);
	while (Size > 0)
	{
		idx Idx = DEQUEUE();
		pt Current = Stack[Idx];
		idx CurrentElevation = Data[Current.Y][Current.X];
		idx V = Val[Idx];

		if (V >= CurrentBest[Current.Y][Current.X]) continue;
		else CurrentBest[Current.Y][Current.X] = V;

		if (Current.X == End.X && Current.Y == End.Y)
		{
			break;
		}
		pt Neighbors[4] = {
			{ Current.X - 1, Current.Y },
			{ Current.X + 1, Current.Y },
			{ Current.X, Current.Y - 1 },
			{ Current.X, Current.Y + 1 },
		};

		for (idx I = 0;
		     I < 4;
		     I += 1)
		{
			if (Neighbors[I].X >= 0 && Neighbors[I].Y >= 0
			   && Neighbors[I].X < Grid.Width && Neighbors[I].Y < Grid.Height)
			{
				char TargetElevation = Data[Neighbors[I].Y][Neighbors[I].X];
				char Delta = TargetElevation - CurrentElevation;
				if (Delta <= 1 && CurrentBest[Neighbors[I].Y][Neighbors[I].X] > V + 1)
				{
					ENQUEUE(Neighbors[I], V + 1);
				}
			}
		}
	}
	fprintf(stderr, "%ld\n", CurrentBest[End.Y][End.X]);
	return 0;
#undef ENQUEUE
#undef DEQUEUE
}

PART(Part2)
{
	problem Parsed = ParseInput(Text);

	grid Grid = Parsed.Grid;
	pt End   = Parsed.End;

	char (*Data)[Grid.Width] = (char (*)[Grid.Width])Grid.Data;

	idx Capacity = 512;
	idx Size = 0;
	idx Bottom = 0;
	pt *Stack = malloc(Capacity * sizeof(*Stack));
	idx *Val = malloc(Capacity * sizeof(*Val));
#define DEQUEUE() Bottom++
#define ENQUEUE(Pt, Entry) do {						\
		idx NewEntry = (Entry);					\
		pt NewPt = (Pt);					\
		if (Size == Capacity)					\
		{							\
			if (Bottom == 0)				\
			{						\
				Capacity *= 2;				\
				Stack = realloc(Stack, Capacity * sizeof(*Stack)); \
				Val = realloc(Val, Capacity * sizeof(*Val)); \
			}						\
			else						\
			{						\
				for (idx I = Bottom; I < Size; ++I)	\
				{					\
					Stack[I-Bottom] = Stack[I];	\
					Val[I-Bottom] = Val[I];		\
				}					\
				Size -= Bottom;				\
				Bottom = 0;				\
			}						\
		}							\
		idx Place = Size;					\
		Stack[Place] = NewPt;					\
		Val[Place] = NewEntry;					\
		Size += 1;						\
	} while (0)

	idx (*CurrentBest)[Grid.Width] = malloc(Grid.Height * Grid.Width * sizeof(idx));
	for (idx Y = 0;
	     Y < Grid.Height;
	     Y += 1)
	{
		for (idx X = 0;
		     X < Grid.Width;
		     X += 1)
		{
			CurrentBest[Y][X] = INT64_MAX;
		}
	}

	ENQUEUE(End, 0);
	idx Result = -1;
	while (Size > 0)
	{
		idx Idx = DEQUEUE();
		pt Current = Stack[Idx];
		idx CurrentElevation = Data[Current.Y][Current.X];
		idx V      = Val[Idx];

		if (V >= CurrentBest[Current.Y][Current.X]) continue;
		else CurrentBest[Current.Y][Current.X] = V;

		if (Data[Current.Y][Current.X] == 'a')
		{
			Result = V;
			break;
		}
		pt Neighbors[4] = {
			{ Current.X - 1, Current.Y },
			{ Current.X + 1, Current.Y },
			{ Current.X, Current.Y - 1 },
			{ Current.X, Current.Y + 1 },
		};

		for (idx I = 0;
		     I < 4;
		     I += 1)
		{
			if (Neighbors[I].X >= 0 && Neighbors[I].Y >= 0
			   && Neighbors[I].X < Grid.Width && Neighbors[I].Y < Grid.Height)
			{
				char TargetElevation = Data[Neighbors[I].Y][Neighbors[I].X];
				char Delta = TargetElevation - CurrentElevation;
				if (Delta >= -1 && CurrentBest[Neighbors[I].Y][Neighbors[I].X] > V + 1)
				{
					ENQUEUE(Neighbors[I], V + 1);
				}
			}
		}
	}
	fprintf(stderr, "%ld\n", Result);
	return 0;
#undef ENQUEUE
#undef DEQUEUE
}

part *Parts[] = { &Part1, &Part2 };
