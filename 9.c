#include "aoc.h"
#include <x86intrin.h>
#undef assert
#define assert(...) if (!(__VA_ARGS__)) { __builtin_unreachable(); } (void) 0

typedef enum dir
{
	Dir_UP,
	Dir_DOWN,
	Dir_RIGHT,
	Dir_LEFT,
} dir;

typedef struct token
{
	dir Direction;
	idx Steps;
} token;

#include "token_array.h"

static inline token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	dir Directions[] =
		{
			['R'] = Dir_RIGHT,
			['D'] = Dir_DOWN,
			['U'] = Dir_UP,
			['L'] = Dir_LEFT
		};
	for (str_view Line = NextLine(&Text);
	     Line.Length > 2;
	     Line = NextLine(&Text))
	{
		token Tok;
		str_view DirStr = ReadUntil(&Line, ' ');
		assert(DirStr.Length == 1);
		char Dir = DirStr.Content[0];
		assert(Dir == 'R' || Dir == 'U' || Dir == 'L' || Dir == 'D');
		char Steps = ReadInt(Line);
		Tok.Direction = Directions[(idx)Dir];
		Tok.Steps = Steps;
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct pt
{
	i16 X, Y;
} pt;

static inline i32 GetSign(idx Delta)
{
	i32 Result = 0;
	if (Delta < 0)
	{
		Result = -1;
	}
	else if (Delta > 0)
	{
		Result = 1;
	}
	return Result;
}

static inline pt FollowDir(pt Head, pt Tail, bool *Changed)
{
	idx DeltaX = Head.X - Tail.X;
	idx DeltaY = Head.Y - Tail.Y;
	idx Dist = DeltaX * DeltaX + DeltaY * DeltaY;
	if (Dist >= 4)
	{
		Tail.X += GetSign(DeltaX);
		Tail.Y += GetSign(DeltaY);
	}
	else
	{
		*Changed = false;
	}
	return Tail;
}

typedef struct map
{
	idx Capacity;
	idx Count;
	idx Shift;
	pt *Points;
} map;

static inline map Map_New(idx Capacity)
{
	map M;
	M.Capacity = Capacity;
	M.Count    = 0;
	M.Points   = malloc(Capacity * sizeof(*M.Points));
	M.Shift    = sizeof(Capacity) * 8 - __builtin_ctz(Capacity);
	for (idx I = 0;
	     I < Capacity;
	     ++I)
	{
		M.Points[I].X = INT16_MAX;
		M.Points[I].Y = INT16_MAX;
	}
	return M;
}

static inline bool Valid(pt Pt)
{
	bool Valid = (Pt.X != INT16_MAX);
	return Valid;
}

static inline bool Eq(pt A, pt B)
{
	bool Equal = (A.X == B.X) && (A.Y == B.Y);
	return Equal;
}

static inline u64 Hash(u64 Val, idx Shift)
{
	Val ^= Val >> Shift;
	// 11400714819323198485 ~ 2^64 / Phi
	u64 Result = (Val * UINT64_C(11400714819323198485)) >> Shift;

	return Result;

}

static inline i32 MoveMask32(__m256i Val)
{
	__m256 AsFloats = _mm256_cvtepi32_ps(Val);
	i32 Result = _mm256_movemask_ps(AsFloats);
	return Result;
}

void AddPos_Int(map *Map, pt Pt)
{
	assert(Map->Count < Map->Capacity);
	assert(Valid(Pt));
	u32 Bits;
	static_assert(sizeof(Pt) == sizeof(Bits));
	memcpy(&Bits, &Pt, sizeof(Pt));

	pt Inv = (pt) { .X = INT16_MAX, .Y = INT16_MAX };
	u32 InvBits;
	static_assert(sizeof(Inv) == sizeof(InvBits));
	memcpy(&InvBits, &Inv, sizeof(Inv));

	u64 Orig = Hash(Bits, Map->Shift) & (Map->Capacity - 1);
	u64 H = Orig & ~0b111;
	assert(H <= Orig && Orig < H + 8);
	__m256i *Needle = (__m256i *) &Map->Points[H];
	__m256i Target = _mm256_set1_epi32(Bits);
	__m256i Invalid = _mm256_set1_epi32(InvBits);
	idx Position = -1;
	do
	{
		__m256i Data = _mm256_loadu_si256(Needle);
		__m256i TargetMask = _mm256_cmpeq_epi32(Data, Target);
		__m256i InvalidMask = _mm256_cmpeq_epi32(Data, Invalid);
		i32 IsTarget = MoveMask32(TargetMask);
		i32 IsInvalid = MoveMask32(InvalidMask);
		if (IsTarget)
		{
			assert(__builtin_popcount(IsTarget) == 1);
			return;
		}
		if (IsInvalid)
		{
			idx Offset = __builtin_ctz(IsInvalid);
			Position = Offset + (idx) ((pt *) Needle - Map->Points);
		}

		Needle += 1;
		if (Needle == (__m256i *) &Map->Points[Map->Capacity])
		{
			Needle = (__m256i *) Map->Points;
		}
	} while (Position == -1);
	assert(!Valid(Map->Points[Position]));
	Map->Points[Position] = Pt;
	Map->Count += 1;
}

static inline void TryEnlarging(map *Map)
{
	if (Map->Count >= 0.75 * Map->Capacity)
	{
		idx NewCapacity = Map->Capacity * 2;
		map NewMap = Map_New(NewCapacity);

		for (idx I = 0;
		     I < Map->Capacity;
		     ++I)
		{
			if (Valid(Map->Points[I]))
			{
				AddPos_Int(&NewMap, Map->Points[I]);
			}
		}
		assert(NewMap.Count == Map->Count);
		free(Map->Points);
		*Map = NewMap;
	}
}

static inline void AddPos(map *Map, pt Pt)
{
	TryEnlarging(Map);
	AddPos_Int(Map, Pt);
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	map Map = Map_New(8192);
	pt Head = (pt) {0, 0};
	pt Tail = (pt) {0, 0};
	AddPos(&Map, Tail);
	pt Offsets[] =
	{
		[Dir_UP] = { 0, 1 },
		[Dir_DOWN] = { 0, -1 },
		[Dir_LEFT] = { -1, 0 },
		[Dir_RIGHT] = { 1, 0 }
	};
	for (idx Motion = 0;
	     Motion < Parsed.Count;
	     Motion += 1)
	{
		token Tok = Parsed.Tokens[Motion];
		dir Direction = Tok.Direction;
		idx Steps     = Tok.Steps;
		pt Offset = Offsets[Direction];
		for (idx Ign = 0;
		     Ign < Steps;
		     Ign += 1)
		{
			bool Change = true;
			Head.X += Offset.X;
			Head.Y += Offset.Y;
			Tail = FollowDir(Head, Tail, &Change);
			if (Change)
			{
				AddPos(&Map, Tail);
			}
		}
	}
	fprintf(stderr, "%ld", Map.Count);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	map Map = Map_New(8192);
	pt Points[10] = {0};
	AddPos(&Map, Points[9]);
#define Head Points[0]
#define Tail Points[9]
	pt Offsets[] =
	{
		[Dir_UP] = { 0, 1 },
		[Dir_DOWN] = { 0, -1 },
		[Dir_LEFT] = { -1, 0 },
		[Dir_RIGHT] = { 1, 0 }
	};
	for (idx Motion = 0;
	     Motion < Parsed.Count;
	     Motion += 1)
	{
		token Tok = Parsed.Tokens[Motion];
		dir Direction = Tok.Direction;
		idx Steps     = Tok.Steps;
		pt Offset = Offsets[Direction];
		for (idx Ign = 0;
		     Ign < Steps;
		     Ign += 1)
		{
			Head.X += Offset.X;
			Head.Y += Offset.Y;
			bool Step = true;
			for (idx I = 1;
			     I < 10;
			     ++I)
			{
				Points[I] = FollowDir(Points[I-1], Points[I], &Step);
				if (!Step)
				{
					break;
				}
			}
			if (Step)
			{
				AddPos(&Map, Tail);
			}
		}
	}
	fprintf(stderr, "%ld", Map.Count);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
