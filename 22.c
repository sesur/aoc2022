#include "aoc.h"
#include "math.h"

typedef enum facing
{
	RIGHT,
	DOWN,
	LEFT,
	UP
} facing;

str_view NextParagraph(str_view *Text)
{
	char const *Start = Text->Content;
	idx Length;

	bool HasParagraph = false;
	for (Length = 0;
	     Length < Text->Length - 1;
	     Length += 1)
	{
		if (Start[Length] == '\n' && Start[Length + 1] == '\n')
		{
			HasParagraph = true;
			break;
		}
	}
	str_view Found;
	if (HasParagraph)
	{
		Found.Content = Start;
		Found.Length = Length;
		Text->Content += Length + 2;
		Text->Length -= Length + 2;
	}
	else
	{
		Found = *Text;
		Text->Content += Text->Length;
		Text->Length = 0;
	}
	return Found;
}

typedef enum elem
{
	NOTHING,
	FLOOR,
	WALL,
} elem;

typedef enum rotation
{
	CW, CCW
} rotation;

typedef union command
{
	rotation Rot;
	i8     Move;
} command;

typedef struct map
{
	idx Width, Height;
	elem *Grid;
	idx NumCommands;
	command *Commands;
} map;

map ParseInput(str_view Text)
{
	str_view Paragraph = NextParagraph(&Text);
	str_view Commands = NextLine(&Text);
	str_view Copy = Paragraph;

	idx Width = 0;
	idx Height = 0;

	for (str_view Line = NextLine(&Copy);
	     !Empty(Line);
	     Line = NextLine(&Copy))
	{
		Width = MAX(Width, Line.Length);
		Height += 1;
	}

	elem (*Grid)[Width] = calloc(sizeof(*Grid), Height);
	idx Y = 0, X = 0;
	for (idx I = 0;
	     I < Paragraph.Length;
	     I += 1)
	{
		switch (Paragraph.Content[I])
		{
			break;case ' ': Grid[Y][X++] = NOTHING;
			break;case '.': Grid[Y][X++] = FLOOR;
			break;case '#': Grid[Y][X++] = WALL;
			break;case '\n': { Y += 1; X = 0; }
			break;default: assert(0);
		}
		assert(X <= Width);
	}
	assert((Y == Height && X == 0) || Y == (Height - 1));

	idx NumCommands = 0;
	command *Coms = malloc(Commands.Length * sizeof(*Coms));
	while (!Empty(Commands))
	{
		if (NumCommands % 2 == 0)
		{
			Coms[NumCommands++].Move = SkipNumber(&Commands);
		}
		else
		{
			switch(Commands.Content[0])
			{
				break;case 'L': Coms[NumCommands++].Rot = CCW;
				break;case 'R': Coms[NumCommands++].Rot = CW;
				break;default: assert(0);
			}

			Skip(&Commands, 1);
		}
	}
	map M;
	M.Width = Width;
	M.Height = Height;
	M.Grid = (elem *) Grid;
	M.NumCommands = NumCommands;
	M.Commands = Coms;
	return M;
}

PART(Part1)
{
	map Parsed = ParseInput(Text);

	elem (*Grid)[Parsed.Width] = (elem (*)[Parsed.Width]) Parsed.Grid;

	idx Y = 0;
	idx X = -1;

	LOOP(I, Parsed.Width)
	{
		if (Grid[0][I] == FLOOR)
		{
			X = I;
			break;
		}
	}
	assert(X != -1);

	facing CurrentFacing = RIGHT;
	LOOP(I, Parsed.NumCommands)
	{
		if (I % 2 == 0)
		{
			// movement
			idx Move = Parsed.Commands[I].Move;
			switch (CurrentFacing)
			{
				break;case RIGHT:
				{
					REPEAT(Move)
					{
						idx NextX = (X + 1) % Parsed.Width;
						if (Grid[Y][NextX] == NOTHING)
						{
							NextX = 0;
							while (Grid[Y][NextX] == NOTHING)
							{
								NextX += 1;
							}
						}
						switch (Grid[Y][NextX])
						{
							break;case FLOOR: { X = NextX; }
							break;case WALL:  { Move = 0; }
							break;default: assert(0);
						}
					}
				}
				break;case DOWN:
				{
					REPEAT(Move)
					{
						idx NextY = (Y + 1) % Parsed.Height;
						if (Grid[NextY][X] == NOTHING)
						{
							NextY = 0;
							while (Grid[NextY][X] == NOTHING)
							{
								NextY += 1;
							}
						}
						switch (Grid[NextY][X])
						{
							break;case FLOOR: { Y = NextY; }
							break;case WALL:  { Move = 0; }
							break;default: assert(0);
						}
					}
				}
				break;case LEFT:
				{
					REPEAT(Move)
					{
						idx NextX = (X + Parsed.Width - 1) % Parsed.Width;
						if (Grid[Y][NextX] == NOTHING)
						{
							NextX = Parsed.Width - 1;
							while (Grid[Y][NextX] == NOTHING)
							{
								NextX -= 1;
							}
						}
						switch (Grid[Y][NextX])
						{
							break;case FLOOR: { X = NextX; }
							break;case WALL:  { Move = 0; }
							break;default: assert(0);
						}
					}
				}
				break;case UP:
				{
					REPEAT(Move)
					{
						idx NextY = (Y + Parsed.Height - 1) % Parsed.Height;
						if (Grid[NextY][X] == NOTHING)
						{
							NextY = Parsed.Height - 1;
							while (Grid[NextY][X] == NOTHING)
							{
								NextY -= 1;
							}
						}
						switch (Grid[NextY][X])
						{
							break;case FLOOR: { Y = NextY; }
							break;case WALL:  { Move = 0; }
							break;default: assert(0);
						}
					}
				}
			}
		}
		else
		{
			// turn
			switch (Parsed.Commands[I].Rot)
			{
				break;case CW:  CurrentFacing = (CurrentFacing + 1) % 4;
				break;case CCW: CurrentFacing = (CurrentFacing + 3) % 4;
			}
		}

	}

	idx Value = 1000 * (Y + 1) + 4 * (X + 1) + CurrentFacing;
	fprintf(stderr, "%ld\n", Value);
	return 0;
}

typedef struct pt
{
	i8 X, Y;
} pt;

typedef struct cubept
{
	i8 X, Y, Z;
} cubept;

cubept Cross(cubept A, cubept B)
{
	cubept Result;
	Result.X = A.Y * B.Z - A.Z * B.Y;
	Result.Y = A.Z * B.X - A.X * B.Z;
	Result.Z = A.X * B.Y - A.Y * B.X;
	return Result;
}

cubept RotateCCW(cubept Dir, cubept Base)
{
	return Cross(Dir, Base);
}

cubept RotateCW(cubept Dir, cubept Base)
{
	return Cross(Base, Dir);
}

typedef struct
{
	cubept Col[3];
} mat;

mat RotateDown(mat Cur)
{
	mat Result;
	Result.Col[0] = Cur.Col[0];
	Result.Col[1] = RotateCCW(Cur.Col[1], Cur.Col[0]);
	Result.Col[2] = RotateCCW(Cur.Col[2], Cur.Col[0]);

	return Result;
}

mat RotateUp(mat Cur)
{
	mat Result;
	Result.Col[0] = Cur.Col[0];
	Result.Col[1] = RotateCW(Cur.Col[1], Cur.Col[0]);
	Result.Col[2] = RotateCW(Cur.Col[2], Cur.Col[0]);

	return Result;
}

mat RotateRight(mat Cur)
{
	mat Result;
	Result.Col[0] = RotateCW(Cur.Col[0], Cur.Col[1]);
	Result.Col[1] = Cur.Col[1];
	Result.Col[2] = RotateCW(Cur.Col[2], Cur.Col[1]);

	return Result;
}

mat RotateLeft(mat Cur)
{
	mat Result;
	Result.Col[0] = RotateCCW(Cur.Col[0], Cur.Col[1]);
	Result.Col[1] = Cur.Col[1];
	Result.Col[2] = RotateCCW(Cur.Col[2], Cur.Col[1]);

	return Result;
}

mat RotateInDir(mat Cur, idx Dir)
{
	mat Result;
	switch (Dir)
	{
		break;case RIGHT: Result = RotateRight(Cur);
		break;case DOWN: Result = RotateDown(Cur);
		break;case LEFT: Result = RotateLeft(Cur);
		break;case UP: Result = RotateUp(Cur);
		break;default: assert(0);
	}
	return Result;
}

i8 *Get(i8 Cube[3][3][3], cubept Pos)
{
	idx NonZero = (Pos.X != 0) + (Pos.Y != 0) + (Pos.Z != 0);
	assert(NonZero == 1);

	i8 *Result = &Cube[Pos.Z+1][Pos.Y+1][Pos.X+1];

	return Result;
}

typedef struct item
{
	i8 Face;
	mat Rotation;
} item;

#include "stack.h"

idx Inner(cubept A, cubept B)
{
	idx Result = A.X * B.X + A.Y * B.Y + A.Z * B.Z;
	return Result;
}

pt GetPosOnFace(cubept Pos, mat Rot, idx FaceSize)
{
	cubept Right = Rot.Col[0];
	cubept Down  = Rot.Col[1];
	cubept Ones = { 1, 1, 1 };

	idx X = Inner(Right, Pos);
	idx Y = Inner(Down, Pos);

	if (Inner(Right, Ones) < 0) X += FaceSize - 1;
	if (Inner(Down, Ones) < 0) Y += FaceSize - 1;

	assert(X >= 0 && X < FaceSize);
	assert(Y >= 0 && Y < FaceSize);
	pt Result = {X, Y};
	return Result;
}

typedef struct state
{
	cubept Pos;
	cubept Dir;
	i8 Face;
} state;

idx FaceSizeOf(map M)
{
	elem (*Grid)[M.Width] = (elem (*)[M.Width]) M.Grid;
	double NonEmpty = 0;
	LOOP(Y, M.Height)
	{
		LOOP(X, M.Width)
		{
			NonEmpty += (Grid[Y][X] == NOTHING);
		}
	}
	double Sixth   = NonEmpty / 6;
	double Sqrt    = sqrt(Sixth);

	return (idx) Sqrt;
}

PART(Part2)
{
	map Parsed = ParseInput(Text);
	idx FaceSize = FaceSizeOf(Parsed);

	idx FHeight = Parsed.Height / FaceSize;
	idx FWidth  = Parsed.Width  / FaceSize;
	assert(FHeight * FaceSize == Parsed.Height);
	assert(FWidth * FaceSize == Parsed.Width);

	i8 (*FaceNum)[FWidth] = malloc(sizeof(FaceNum) * FHeight);
	elem (*Grid)[Parsed.Width] = (elem (*)[Parsed.Width]) Parsed.Grid;

	pt FacePos[6];
	{
		idx CurrentFace = 0;
		LOOP(Y, FHeight)
		{
			LOOP(X, FWidth)
			{
				if (Grid[Y * FaceSize][X * FaceSize] != NOTHING)
				{
					FaceNum[Y][X] = CurrentFace;
					FacePos[CurrentFace] = (pt) { .X = X, .Y = Y };
					CurrentFace += 1;
				}
				else
				{
					FaceNum[Y][X] = -1;
				}
			}
		}
		assert(CurrentFace == 6);
	}

	LOOP(I, 6)
	{
		pt P = FacePos[I];
		assert(FaceNum[P.Y][P.X] == I);
	}

	stack S = Stack_New(6);
	Stack_Push(&S, (item) {
			.Face = 0,
			.Rotation = {{ {1, 0, 0}, {0, 1, 0}, {0, 0, 1} }}});

	i8 Cube[3][3][3];

	memset(Cube, -1, sizeof(Cube));

	pt Delta[4] =
	{
		[UP]    = {  0, -1 },
		[DOWN]  = {  0, 1 },
		[LEFT]  = { -1,  0 },
		[RIGHT] = {  1,  0 },
	};

	mat FaceRotation[6];
	while (!Stack_Empty(S))
	{
		item Cur = Stack_Pop(&S);

		i8 Face = Cur.Face;
		FaceRotation[Face] = Cur.Rotation;
		cubept CubePos = Cur.Rotation.Col[2];
		i8 *C = Get(Cube, CubePos);
		assert(*C == -1);
		*C = Face;
		pt CurPos = FacePos[Face];
		LOOP(Dir, 4)
		{

			pt NextPos = CurPos;
			NextPos.X += Delta[Dir].X;
			NextPos.Y += Delta[Dir].Y;

			if (NextPos.X < 0 || NextPos.X >= FWidth
			   || NextPos.Y < 0 || NextPos.Y >= FHeight)
			{
				continue;
			}
			item Next;

			Next.Face     = FaceNum[NextPos.Y][NextPos.X];
			Next.Rotation = RotateInDir(Cur.Rotation, Dir);
			cubept NextCubePos = Next.Rotation.Col[2];

			if (Next.Face != -1 && *Get(Cube, NextCubePos) == -1)
			{
				Stack_Push(&S, Next);
			}
		}

	}

	LOOP(Z, 3) LOOP(Y, 3) LOOP(X, 3)
	{
		idx NotOne = (Z != 1) + (Y != 1) + (X != 1);
		if (NotOne == 1)
		{
			assert(Cube[Z][Y][X] != -1);
		}
	}


	state Current = {
		.Pos = { 0, 0, FaceSize }, // = FaceSize * FaceRotation[0].Col[2]
		.Dir = { 1, 0, 0 },        // = FaceRotation[0].Col[0]
		.Face = 0,
	};

	LOOP(I, Parsed.NumCommands)
	{
		if (I % 2 == 0)
		{
			// movement
			idx Move = Parsed.Commands[I].Move;
			REPEAT(Move)
			{
				state Next = Current;
				Next.Pos.X += Current.Dir.X;
				Next.Pos.Y += Current.Dir.Y;
				Next.Pos.Z += Current.Dir.Z;

				if ((Current.Dir.X && (Next.Pos.X < 0 || Next.Pos.X >= FaceSize)) ||
				    (Current.Dir.Y && (Next.Pos.Y < 0 || Next.Pos.Y >= FaceSize)) ||
				    (Current.Dir.Z && (Next.Pos.Z < 0 || Next.Pos.Z >= FaceSize)))
				{
					cubept Base = FaceRotation[Current.Face].Col[2];
					Next.Dir.X = -Base.X;
					Next.Dir.Y = -Base.Y;
					Next.Dir.Z = -Base.Z;

					Next.Pos.X += Next.Dir.X;
					Next.Pos.Y += Next.Dir.Y;
					Next.Pos.Z += Next.Dir.Z;

					Next.Face = *Get(Cube, Current.Dir);
					assert(Next.Face != -1);
				}

				pt FPos = GetPosOnFace(Next.Pos, FaceRotation[Next.Face], FaceSize);

				idx X = FacePos[Next.Face].X * FaceSize + FPos.X;
				idx Y = FacePos[Next.Face].Y * FaceSize + FPos.Y;
				elem NextElem = Grid[Y][X];
				assert(NextElem != NOTHING);
				if (NextElem != WALL)
				{
					Current = Next;
				}
				else
				{
					break;
				}
			}
		}
		else
		{
			// turn
			cubept Base = FaceRotation[Current.Face].Col[2];
			switch (Parsed.Commands[I].Rot)
			{
				break;case CW:  Current.Dir = RotateCW(Current.Dir, Base);
				break;case CCW: Current.Dir = RotateCCW(Current.Dir, Base);
			}
		}

	}

	pt FPos = GetPosOnFace(Current.Pos, FaceRotation[Current.Face], FaceSize);

	idx X = FacePos[Current.Face].X * FaceSize + FPos.X;
	idx Y = FacePos[Current.Face].Y * FaceSize + FPos.Y;

	idx RotRight = Inner(Current.Dir, FaceRotation[Current.Face].Col[0]);
	idx RotDown  = Inner(Current.Dir, FaceRotation[Current.Face].Col[1]);

	idx RotVal;
	if (RotRight)
	{
		// 0 for right (1), 2 for left (-1)
		RotVal = -2 * RotRight;
	}
	else
	{
		// 1 for down (1), 3 for up (-1)
		assert(RotDown);
		RotVal = 1 - 2 * RotDown;
	}
	idx Value = 1000 * (Y + 1) + 4 * (X + 1) + RotVal;
	fprintf(stderr, "%ld\n", Value);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
