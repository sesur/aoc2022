#include "aoc.h"
#include <assert.h>

typedef struct range
{
	idx Begin, End;
} range;
typedef struct token
{
	range Shift[2];
} token;

#include "token_array.h"

token_array ParseText(str_view Text)
{
	token_array Parsed = TokenArray_New(40);

	for (str_view Line = NextLine(&Text);
	     Line.Length != 0;
	     Line = NextLine(&Text))
	{
		token Tok;
		Tok.Shift[0].Begin = ReadInt(ReadUntil(&Line, '-'));
		Tok.Shift[0].End = ReadInt(ReadUntil(&Line, ','));
		Tok.Shift[1].Begin = ReadInt(ReadUntil(&Line, '-'));
		Tok.Shift[1].End = ReadInt(Line);
		TokenArray_Push(&Parsed, Tok);
	}

	return Parsed;
}

bool Contained(range First, range Second)
{
	bool IsContained = (First.End <= Second.End) && (First.Begin >= Second.Begin);

	return IsContained;
}

bool Overlap(range First, range Second)
{
	bool DoesOverlap = (First.Begin <= Second.End) && (Second.Begin <= First.End);
	return DoesOverlap;
}

PART(Part1)
{
	token_array Parsed = ParseText(Text);

	idx NumBad = 0;

	for (idx Pair = 0;
	     Pair < Parsed.Count;
	     ++Pair)
	{
		token Tok = Parsed.Tokens[Pair];

		range Fst = Tok.Shift[0];
		range Scd = Tok.Shift[1];

		if (Contained(Fst, Scd) || Contained(Scd, Fst))
		{
			NumBad += 1;
		}
	}

	fprintf(stderr, "NumBad: %ld\n", NumBad);

	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseText(Text);

	idx NumBad = 0;

	for (idx Pair = 0;
	     Pair < Parsed.Count;
	     ++Pair)
	{
		token Tok = Parsed.Tokens[Pair];

		range Fst = Tok.Shift[0];
		range Scd = Tok.Shift[1];

		if (Overlap(Fst, Scd) || Contained(Scd, Fst))
		{
			NumBad += 1;
		}
	}

	fprintf(stderr, "NumBad: %ld\n", NumBad);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
