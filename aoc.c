#include <unistd.h>
#include <time.h>

str_view
FromCStr
(
	char *Str
)
{
	str_view Result;
	Result.Content = Str;
	Result.Length = strlen(Str);
	return Result;
}

str_view
LoadFile
(
	str_view Path
)
{
	str_view Result = {0};

	FILE *File = fopen(Path.Content, "r");

	if (File)
	{
		int Error = fseek(File, 0, SEEK_END);

		assert(!Error);

		long Length = ftell(File);

		rewind(File);

		char *Buffer = malloc(Length);

		int Read = fread(Buffer, 1, Length, File);

		assert(Read == Length);

		Result.Content = Buffer;
		Result.Length = Length;
	}
	else
	{
		perror(NULL);
	}

	return Result;
}

int
main
(
	int Count,
	char **Arguments
)
{
	int Result;
	if (Count == 3)
	{
		idx Part = *Arguments[1] - '1';
		str_view Text = LoadFile(FromCStr(Arguments[2]));
		if (Text.Content == NULL)
		{
			return 1;
		}
		assert(Part >= 0 && Part < (idx) ARRAYCOUNT(Parts));
		struct timespec Start, End;
		assert(clock_gettime(CLOCK_MONOTONIC, &Start) == 0);
		asm volatile("": : :"memory");
		Result = Parts[Part](Text);
		asm volatile("": : :"memory");
		assert(clock_gettime(CLOCK_MONOTONIC, &End) == 0);
		idx Sec = End.tv_sec - Start.tv_sec;
		idx Nano = End.tv_nsec - Start.tv_nsec;
		idx Time = Sec * 1e9 + Nano;
		idx NSec = Time % 1000; Time /= 1000;
		idx USec = Time % 1000; Time /= 1000;
		idx MSec = Time % 1000; Time /= 1000;
		idx DSec = Time % 1000; Time /= 1000;
		idx Min = Time;

		printf("%02ld:%02ld.%03ld-%03ld'%03ld\n", Min, DSec, MSec, USec, NSec);
	}
	else
	{
		assert(Count > 0);
		printf("Usage: '%s <part> <file>'\n", Arguments[0]);
		Result = -1;
	}

	return Result;
}

str_view ReadUntil(str_view *Text, char Sep)
{
	char const *Start = Text->Content;
	idx Length;

	for (Length = 0;
	     Length < Text->Length;
	     Length += 1)
	{
		if (Start[Length] == Sep)
		{
			break;
		}
	}
	str_view Found;
	Found.Content = Start;
	Found.Length = Length;
	Text->Content += Length + 1;
	Text->Length -= Length + 1;
	return Found;
}

str_view NextLine(str_view *Text)
{
	return ReadUntil(Text, '\n');
}

void SkipUntil(str_view *Text, char Sep)
{
	str_view Ignored = ReadUntil(Text, Sep);
	(void) Ignored;
}

idx ReadInt(str_view Str)
{
	idx Result = 0;
	idx Sign;
	char const *Current = Str.Content;

	if (*Current == '-')
	{
		Current += 1;
		Sign = -1;
	}
	else
	{
		Sign = 1;
	}

	while (Current != Str.Content + Str.Length)
	{
		assert(*Current <= '9' && *Current >= '0');
		Result = 10 * Result + *Current - '0';
		Current = Current + 1;
	}
	return Sign * Result;
}

bool ViewEq(str_view A, str_view B)
{
	bool Equal = (A.Length == B.Length) && (0 == strncmp(A.Content, B.Content, A.Length));
	return Equal;
}

void Skip(str_view *View, idx Num)
{
	View->Length -= Num;
	View->Content += Num;
}

bool Empty(str_view View)
{
	return View.Length <= 0;
}

idx SkipNumber(str_view *View)
{
	idx Result = 0;
	char const *Content = View->Content;
	idx Length = View->Length;
	idx NumDigits = 0;
	idx Sign = 1;
	if (Length > 0 && Content[0] == '-')
	{
		Sign = -1;
		NumDigits += 1;
	}
	else if (Length > 0 && Content[0] == '+')
	{
		Sign = 1;
		NumDigits += 1;
	}

	while (NumDigits < Length)
	{
		if (Content[NumDigits] >= '0' && Content[NumDigits] <= '9')
		{
			Result = Result * 10 + Content[NumDigits] - '0';
			NumDigits += 1;
		}
		else
		{
			break;
		}
	}
	View->Content += NumDigits;
	View->Length -= NumDigits;
	return Sign * Result;
}

idx Max_idx(idx A, idx B)
{
	idx Result;
	if (B >= A) Result = B;
	else        Result = A;
	return Result;
}

idx Min_idx(idx A, idx B)
{
	idx Result;
	if (B >= A) Result = A;
	else        Result = B;
	return Result;
}

i32 Max_i32(i32 A, i32 B)
{
	i32 Result;
	if (B >= A) Result = B;
	else        Result = A;
	return Result;
}

i32 Min_i32(i32 A, i32 B)
{
	i32 Result;
	if (B >= A) Result = A;
	else        Result = B;
	return Result;
}
