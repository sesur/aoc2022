#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef ptrdiff_t idx;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;

typedef struct str_view
{
	char const *Content;
	idx Length;
} str_view;

#define PART(Name) int Name(str_view Text)
typedef PART(part);

#define ARRAYCOUNT(Arr) (sizeof(Arr) / sizeof(*(Arr)))

static inline str_view ReadUntil(str_view *Text, char Sep);
static inline str_view NextLine(str_view *Text);
static inline void SkipUntil(str_view *Text, char Sep);
static inline void Skip(str_view *View, idx Num);
static inline bool Empty(str_view View);
static inline idx ReadInt(str_view Str);
static inline bool ViewEq(str_view A, str_view B);
static inline idx SkipNumber(str_view *View);

#define CONCAT_(A, B) A ## B
#define CONCAT(A, B) CONCAT_(A, B)
#define MACROVAR(Name) CONCAT(Name, __LINE__)
#define LOOP(Var, Num) for (idx Var = 0; Var < (Num); Var += 1)
#define REPEAT(Num) LOOP(MACROVAR(I), Num)

idx Max_idx(idx A, idx B);
idx Min_idx(idx A, idx B);
i32 Max_i32(i32 A, i32 B);
i32 Min_i32(i32 A, i32 B);
#define MAX(A, B) _Generic((A+B), i32: Max_i32, idx: Max_idx)(A, B)
#define MIN(A, B) _Generic((A+B), i32: Max_i32, idx: Min_idx)(A, B)
