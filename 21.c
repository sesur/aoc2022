#include "aoc.h"
#include <x86intrin.h>

typedef enum operation
{
	SCREAM,
	ADD,
	SUB,
	DIV,
	MUL
} operation;

typedef struct token
{
	u32 Name;
	union
	{
		struct { u32 Left, Right; };
		idx Number;
	};
	operation Op;
} token;

#include "token_array.h"

u32 AsNumber(str_view Text)
{
	assert(Text.Length == 4);
	u32 Result = 0;
	for (idx I = 0;
	     I < Text.Length;
	     I += 1)
	{
		u32 Current = Text.Content[I] - 'a';
		assert(Current <= 'z' - 'a');
		Result |= Current << (5 * I);
		assert((Result >> (5 * I) & 0b11111) == Current);
	}


	return Result;
}

operation AsOperation(str_view Text)
{
	assert(Text.Length == 1);
	operation Op;
	switch (Text.Content[0])
	{
		break;case '+': Op = ADD;
		break;case '-': Op = SUB;
		break;case '/': Op = DIV;
		break;case '*': Op = MUL;
		break;default: assert(!"Bad op");
	}
	return Op;
}

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	while (!Empty(Text))
	{
		token Tok;
		str_view Name = ReadUntil(&Text, ':');
		Skip(&Text, 1);
		char const *Needle = Text.Content;
		idx Number = SkipNumber(&Text);
		Tok.Name = AsNumber(Name);
		if (Needle == Text.Content)
		{
			// not a number
			str_view Left  = ReadUntil(&Text, ' ');
			str_view Op    = ReadUntil(&Text, ' ');
			str_view Right = ReadUntil(&Text, '\n');

			Tok.Left = AsNumber(Left);
			Tok.Right = AsNumber(Right);
			Tok.Op = AsOperation(Op);
		}
		else
		{
			// is a number
			Tok.Number = Number;
			Tok.Op = SCREAM;
			ReadUntil(&Text, '\n');
		}

		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct map
{
	idx Capacity;
	idx Count;
	idx Shift;
	u32 *Name;
	u32 *Index;
} map;

static inline map Map_New(idx Capacity)
{
	map M;
	M.Capacity = Capacity;
	M.Count    = 0;
	M.Name     = malloc(Capacity * sizeof(*M.Name));
	M.Index    = malloc(Capacity * sizeof(*M.Index));
	M.Shift    = sizeof(Capacity) * 8 - __builtin_ctz(Capacity);
	for (idx I = 0;
	     I < Capacity;
	     ++I)
	{
		M.Name[I]  = UINT32_MAX;
		M.Index[I] = UINT32_MAX;
	}
	return M;
}

static inline bool Valid(u32 Name)
{
	bool Valid = (Name != UINT32_MAX);
	return Valid;
}

static inline u64 Hash(u64 Val, idx Shift)
{
	Val ^= Val >> Shift;
	// 11400714819323198485 ~ 2^64 / Phi
	u64 Result = (Val * UINT64_C(11400714819323198485)) >> Shift;

	return Result;

}

static inline i32 MoveMask32(__m256i Val)
{
	__m256 AsFloats = _mm256_cvtepi32_ps(Val);
	i32 Result = _mm256_movemask_ps(AsFloats);
	return Result;
}

static inline u32 TryAddName_Int(map *Map, u32 Name, u32 Idx)
{
	assert(Map->Count < Map->Capacity);
	assert(Valid(Name));

	u32 Bits = Name;
	u32 InvBits = UINT32_MAX;
	u64 Orig = Hash(Bits, Map->Shift) & (Map->Capacity - 1);
	u64 H = Orig & ~0b111;
	assert(H <= Orig && Orig < H + 8);
	__m256i *Needle = (__m256i *) &Map->Name[H];
	__m256i Target = _mm256_set1_epi32(Bits);
	__m256i Invalid = _mm256_set1_epi32(InvBits);
	idx Position = -1;
	do
	{
		__m256i Data = _mm256_loadu_si256(Needle);
		__m256i TargetMask = _mm256_cmpeq_epi32(Data, Target);
		__m256i InvalidMask = _mm256_cmpeq_epi32(Data, Invalid);
		i32 IsTarget = MoveMask32(TargetMask);
		i32 IsInvalid = MoveMask32(InvalidMask);
		if (IsTarget)
		{
			assert(__builtin_popcount(IsTarget) == 1);
			idx Offset = __builtin_ctz(IsTarget);
			Position = Offset + (idx) ((u32 *) Needle - Map->Name);
			assert(Map->Name[Position] == Name);
			return Map->Index[Position];
		}
		if (IsInvalid)
		{
			idx Offset = __builtin_ctz(IsInvalid);
			Position = Offset + (idx) ((u32 *) Needle - Map->Name);
		}

		Needle += 1;
		if (Needle == (__m256i *) &Map->Name[Map->Capacity])
		{
			Needle = (__m256i *) Map->Name;
		}
	} while (Position == -1);
	assert(!Valid(Map->Name[Position]));
	Map->Name[Position] = Name;
	Map->Index[Position] = Idx;
	Map->Count += 1;
	return Idx;
}

static inline void TryEnlarging(map *Map)
{
	if (Map->Count >= 0.75 * Map->Capacity)
	{
		idx NewCapacity = Map->Capacity * 2;
		map NewMap = Map_New(NewCapacity);

		for (idx I = 0;
		     I < Map->Capacity;
		     ++I)
		{
			if (Valid(Map->Name[I]))
			{
				u32 Result = TryAddName_Int(&NewMap, Map->Name[I], Map->Index[I]);
				assert(Result == Map->Index[I]);
			}
		}
		assert(NewMap.Count == Map->Count);
		free(Map->Name);
		free(Map->Index);
		*Map = NewMap;
	}
}

static inline u32 TryAddName(map *Map, u32 Name, u32 Idx)
{
	TryEnlarging(Map);
	return TryAddName_Int(Map, Name, Idx);
}

u32 Rename(token_array Array)
{
	map M = Map_New(Array.Count * 2);
	str_view RootStr = { .Content = "root", .Length = 4 };
	u32 Root = AsNumber(RootStr);
	u32 RootIdx = UINT32_MAX;

	LOOP(I, Array.Count)
	{
		token Tok = Array.Tokens[I];
		if (Tok.Name == Root)
		{
			assert(RootIdx == UINT32_MAX);
			RootIdx = I;
		}

		TryAddName(&M, Tok.Name, I);
	}

	LOOP(I, Array.Count)
	{
		token *Tok = &Array.Tokens[I];
		if (Tok->Op != SCREAM)
		{
			u32 Left = TryAddName(&M, Tok->Left, Array.Count);
			assert(Left != Array.Count);
			Tok->Left = Left;
			u32 Right = TryAddName(&M, Tok->Right, Array.Count);
			assert(Right != Array.Count);
			Tok->Right = Right;
		}
	}

	assert(RootIdx != UINT32_MAX);
	return RootIdx;
}

idx Compute(operation Op, idx Left, idx Right)
{
	idx Result;
	switch(Op)
	{
		break;case ADD: Result = Left + Right;
		break;case SUB: Result = Left - Right;
		break;case MUL: Result = Left * Right;
		break;case DIV: Result = Left / Right;
		break;default: assert(0);
	}
	return Result;
}

idx Solve(u32 Current, token *Tokens)
{
	token Tok = Tokens[Current];
	if (Tok.Op == SCREAM)
	{
		return Tok.Number;
	}
	else
	{
		idx Left = Solve(Tok.Left, Tokens);
		idx Right = Solve(Tok.Right, Tokens);
		return Compute(Tok.Op, Left, Right);
	}
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	u32 Root = Rename(Parsed);

	idx Result = Solve(Root, Parsed.Tokens);
	fprintf(stderr, "%ld\n", Result);
	return 0;
}

bool Reduce(u32 Current, token *Tokens)
{
	str_view HumanStr = { .Length = 4, .Content = "humn" };
	u32 Human = AsNumber(HumanStr);
	token Tok = Tokens[Current];
	if (Tok.Name == Human)
	{
		return true;
	}
	else if (Tok.Op == SCREAM)
	{
		// nothing
		return false;
	}
	else
	{
		bool Left = Reduce(Tok.Left, Tokens);
		bool Right = Reduce(Tok.Right, Tokens);

		if (Left | Right)
		{
			assert(Left ^ Right);
			return true;
		}
		else
		{
			idx Num = Compute(Tok.Op,
					  Tokens[Tok.Left].Number,
					  Tokens[Tok.Right].Number);

			Tokens[Current].Op = SCREAM;
			Tokens[Current].Number = Num;
			return false;

		}
	}
}

idx MakeEqual(idx Desired, u32 Current, token *Tokens)
{
	str_view HumanStr = { .Length = 4, .Content = "humn" };
	u32 Human = AsNumber(HumanStr);

	token Tok   = Tokens[Current];
	if (Tok.Name == Human) return Desired;

	token Left  = Tokens[Tok.Left];
	token Right = Tokens[Tok.Right];

	if (Left.Op == SCREAM && Left.Name != Human)
	{
		// this means the human is right
		idx NewDesired;
		switch (Tok.Op)
		{
			break;case ADD: NewDesired = Desired - Left.Number;
			break;case SUB: NewDesired = Left.Number - Desired;
			break;case MUL: NewDesired = Desired / Left.Number;
			break;case DIV: NewDesired = Left.Number / Desired;
			break;default: assert(0);
		}
		return MakeEqual(NewDesired, Tok.Right, Tokens);
	}
	else
	{
		// this means the human is left
		assert(Right.Op == SCREAM);
		idx NewDesired;
		switch (Tok.Op)
		{
			break;case ADD: NewDesired = Desired - Right.Number;
			break;case SUB: NewDesired = Right.Number + Desired;
			break;case MUL: NewDesired = Desired / Right.Number;
			break;case DIV: NewDesired = Right.Number * Desired;
			break;default: assert(0);
		}
		return MakeEqual(NewDesired, Tok.Left, Tokens);
	}
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	u32 Root = Rename(Parsed);

	bool HumanLeft = Reduce(Parsed.Tokens[Root].Left, Parsed.Tokens);
	bool HumanRight = Reduce(Parsed.Tokens[Root].Right, Parsed.Tokens);
	assert(HumanLeft ^ HumanRight);

	idx Equal;
	u32 Start;
	if (HumanLeft)
	{
		token Tok = Parsed.Tokens[Parsed.Tokens[Root].Right];
		assert(Tok.Op == SCREAM);
		Equal = Tok.Number;
		Start = Parsed.Tokens[Root].Left;
	}
	else
	{
		token Tok = Parsed.Tokens[Parsed.Tokens[Root].Left];
		assert(Tok.Op == SCREAM);
		Equal = Tok.Number;
		Start = Parsed.Tokens[Root].Right;
	}

	idx Human = MakeEqual(Equal, Start, Parsed.Tokens);
	fprintf(stderr, "%ld\n", Human);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
