#include "aoc.h"
#include <assert.h>

u32 CharNum(char C)
{
	assert('a' <= C && C <= 'z');
	idx Shift = C - 'a';
	u32 Result = 1u << Shift;

	return Result;
}

PART(Part1)
{
	idx PacketStart = -1;
	u32 Data = CharNum(Text.Content[0]) ^ CharNum(Text.Content[1]) ^ CharNum(Text.Content[2]);

	for (idx I = 3;
	     I < Text.Length;
	     I += 1)
	{
		Data ^= CharNum(Text.Content[I]);
		idx Num = __builtin_popcount(Data);
		if (Num == 4)
		{
			PacketStart = I + 1;
			break;
		}
		Data ^= CharNum(Text.Content[I - 3]);
	}

	fprintf(stderr, "PacketStart: %ld\n", PacketStart);

	return 0;
}

PART(Part2)
{
	idx LookingLength = 14;

	idx MessageStart;
	u32 Data = 0;

	for (MessageStart = 0;
	     MessageStart < LookingLength - 1;
	     MessageStart += 1)
	{
		Data ^= CharNum(Text.Content[MessageStart]);
	}

	for (;
	     MessageStart < Text.Length;
	     MessageStart += 1)
	{
		Data ^= CharNum(Text.Content[MessageStart]);
		idx Num = __builtin_popcount(Data);
		if (Num == LookingLength)
		{
			break;
		}
		Data ^= CharNum(Text.Content[MessageStart - LookingLength + 1]);
	}

	assert(MessageStart != Text.Length);
	fprintf(stderr, "MessageStart: %ld\n", MessageStart + 1);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
