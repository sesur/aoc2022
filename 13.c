#include "aoc.h"

typedef struct packet packet;

struct packet
{
	idx Count;
	union {
		idx Val;
		packet *Data;
	};
};

typedef struct token
{
	packet Left;
	packet Right;
} token;

#include "token_array.h"

static inline packet ReadPacket(str_view *Line)
{
	assert(Line->Content[0] == '[');

	idx Capacity = 16;
	idx Size = 0;
	packet *Data = malloc(Capacity * sizeof(*Data));

	SkipUntil(Line, '[');
	while (Line->Length > 0 && Line->Content[0] != ']')
	{
		if (Size == Capacity)
		{
			Capacity *= 2;
			Data = realloc(Data, Capacity * sizeof(*Data));
			assert(Data);
		}
		if (Line->Content[0] == '[')
		{
			Data[Size] = ReadPacket(Line);
		}
		else
		{
			Data[Size].Count = -1;
			Data[Size].Val = SkipNumber(Line);
			if (Line->Content[0] == ',') Skip(Line, 1);
		}
		Size += 1;
	}

	assert(Line->Length > 0 && Line->Content[0] == ']');
	Skip(Line, 1);
	packet Result;
	Result.Count = Size;
	Result.Data = Data;
	return Result;
}

static inline token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (str_view Line = NextLine(&Text);
	     Line.Length != 0;
	     Line = NextLine(&Text))
	{
		token Tok;
		Tok.Left = ReadPacket(&Line);
		str_view Next = NextLine(&Text);
		Tok.Right = ReadPacket(&Next);
		SkipUntil(&Text, '\n');
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

static inline idx Compare(packet Left, idx I)
{
	while (Left.Count > 0 && Left.Data[0].Count != -1)
	{
		Left = Left.Data[0];
	}


	if (Left.Count == 0)
	{
		return 1;
	}
	else
	{
		if (I > Left.Data[0].Val)
		{
			return 1;
		}
		else if (I < Left.Data[0].Val)
		{
			return -1;
		}
		else
		{
			if (Left.Count > 1)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
	}
}

static inline idx InOrder(packet Left, packet Right)
{
	idx Bigger = Right.Count - Left.Count;
	idx Count = (Left.Count <= Right.Count) ? Left.Count : Right.Count;

	for (idx I = 0;
	     I < Count;
	     I += 1)
	{
		if (Left.Data[I].Count != -1)
		{
			if (Right.Data[I].Count != -1)
			{
				idx Ok = InOrder(Left.Data[I], Right.Data[I]);
				if (Ok)
				{
					return Ok;
				}
			}
			else
			{
				idx Ok = Compare(Left.Data[I], Right.Data[I].Val);
				if (Ok)
				{
					return Ok;
				}
			}
		}
		else
		{
			if (Right.Data[I].Count != -1)
			{
				idx Ok = -Compare(Right.Data[I], Left.Data[I].Val);
				if (Ok)
				{
					return Ok;
				}
			}
			else
			{
				idx Ok = Right.Data[I].Val - Left.Data[I].Val;
				if (Ok)
				{
					return Ok;
				}
			}
		}
	}

	return Bigger;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	idx Correct = 0;
	for (idx Pair = 0;
	     Pair < Parsed.Count;
	     Pair += 1)
	{
		token Tok = Parsed.Tokens[Pair];
		packet Left = Tok.Left;
		packet Right = Tok.Right;
		idx Order = InOrder(Left, Right);
		assert(Order);
		if (Order > 0)
		{
			Correct += (Pair + 1);
		}
	}
	fprintf(stderr, "%ld\n", Correct);

	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);

	packet T = { .Count = -1, .Val = 2 };
	packet S = { .Count = -1, .Val = 6 };
	packet Two = { .Count = 1, .Data = &T };
	packet Six = { .Count = 1, .Data = &S };
	packet Div1 = { .Count = 1, .Data = &Two };
	packet Div2 = { .Count = 1, .Data = &Six };

	idx Count[2] = {1,2};
	assert(InOrder(Div1, Div2) > 0);
	for (idx Pair = 0;
	     Pair < Parsed.Count;
	     Pair += 1)
	{
		token Tok = Parsed.Tokens[Pair];
		packet Left = Tok.Left;
		packet Right = Tok.Right;
		if (InOrder(Left, Div1) > 0)
		{
			Count[0] += 1;
			Count[1] += 1;
		}
		else if (InOrder(Left, Div2) > 0)
		{
			Count[1] += 1;
		}
		if (InOrder(Right, Div1) > 0)
		{
			Count[0] += 1;
			Count[1] += 1;
		}
		else if (InOrder(Right, Div2) > 0)
		{
			Count[1] += 1;
		}
	}
	fprintf(stderr, "%ld\n", Count[0] * Count[1]);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
