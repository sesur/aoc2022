#include "aoc.h"

typedef idx token;

#include "token_array.h"

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (;
	     !Empty(Text);
	     SkipUntil(&Text, '\n'))
	{
		token Tok;
		Tok = SkipNumber(&Text);
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct node node;
struct node
{
	node *Left, *Right;
};

void Remove(node *Node)
{
	Node->Left->Right = Node->Right;
	Node->Right->Left = Node->Left;
	Node->Left = NULL;
	Node->Right = NULL;
}

void AddLeft(node *Node, node *ToAdd)
{
	ToAdd->Left = Node->Left;
	ToAdd->Right = Node;
	ToAdd->Left->Right = ToAdd;
	ToAdd->Right->Left = ToAdd;
}

void AddRight(node *Node, node *ToAdd)
{
	ToAdd->Right = Node->Right;
	ToAdd->Left = Node;
	ToAdd->Left->Right = ToAdd;
	ToAdd->Right->Left = ToAdd;
}
void MoveLeft(node *Node, idx Num)
{
	if (Num == 0) return;
	node *Target = Node->Left;
	Remove(Node);
	LOOP(I, Num-1)
	{
		Target = Target->Left;
	}
	assert(Target != Node);
	AddLeft(Target, Node);
}

void MoveRight(node *Node, idx Num)
{
	if (Num == 0) return;
	node *Target = Node->Right;
	Remove(Node);
	LOOP(I, Num-1)
	{
		Target = Target->Right;
	}
	assert(Target != Node);
	AddRight(Target, Node);
}

node *CreateRing(idx Count)
{
	node *Nodes = malloc(Count * sizeof(*Nodes));
	node Sentinel;
	Sentinel.Left = &Sentinel;
	Sentinel.Right = &Sentinel;
	node *Last = &Sentinel;
	LOOP(I, Count)
	{
		AddRight(Last, &Nodes[I]);
		Last = &Nodes[I];
	}
	Remove(&Sentinel);
	return Nodes;
}
PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	idx Count = Parsed.Count;
	idx *Numbers = Parsed.Tokens;
	idx ZeroIdx = -1;
	node *Nodes = CreateRing(Count);
	LOOP(I, Count)
	{
		idx Num = Numbers[I];
		if (Num == 0)
		{
			assert(ZeroIdx == -1);
			ZeroIdx = I;
		}
		idx Movements = Num % (Count - 1);
		if (Num < 0)
		{
			MoveLeft(&Nodes[I], -Movements);
		}
		else
		{
			MoveRight(&Nodes[I], Movements);
		}
	}
	assert(ZeroIdx != -1);
	assert(Numbers[ZeroIdx] == 0);
	node *Current = &Nodes[ZeroIdx];
	idx Sum = 0;
	idx Step = 1000;
	LOOP(I, 3)
	{
		LOOP(J, Step)
		{
			Current = Current->Right;
		}
		idx Idx = Current - Nodes;
		idx Num = Numbers[Idx];
		Sum += Num;
	}
	fprintf(stderr, "%ld\n", Sum);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	idx Key = 811589153;
	idx Count = Parsed.Count;
	idx *Numbers = Parsed.Tokens;
	idx ZeroIdx = -1;
	LOOP(I, Count)
	{
		if (Numbers[I] == 0)
		{
			assert(ZeroIdx == -1);
			ZeroIdx = I;
		}
		Numbers[I] *= Key;
	}
	assert(ZeroIdx != -1);
	assert(Numbers[ZeroIdx] == 0);
	node *Nodes = CreateRing(Count);
	LOOP(LoopCount, 10)
	{
		LOOP(I, Count)
		{
			idx Num = Numbers[I];
			idx Movements = Num % (Count - 1);
			if (Num < 0)
			{
				MoveLeft(&Nodes[I], -Movements);
			}
			else
			{
				MoveRight(&Nodes[I], Movements);
			}
		}
	}
	node *Current = &Nodes[ZeroIdx];
	idx Sum = 0;
	idx Step = 1000;
	LOOP(I, 3)
	{
		LOOP(J, Step)
		{
			Current = Current->Right;
		}
		idx Idx = Current - Nodes;
		idx Num = Numbers[Idx];
		Sum += Num;
	}
	fprintf(stderr, "%ld\n", Sum);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
