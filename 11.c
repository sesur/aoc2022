#include "aoc.h"

typedef struct vec
{
	idx Capacity;
	idx Size;
	idx *Get;
} vec;

typedef struct op
{
	enum { ADD, MUL, SQ } Type;
	idx Num;
} op;

typedef struct token
{
	vec Items;
	idx Test;
	i32 True;
	i32 False;
	op Operation;
} token;

#include "token_array.h"

static inline vec Vec_New(idx Capacity)
{
	vec V;
	V.Capacity = Capacity;
	V.Size = 0;
	V.Get = malloc(sizeof(*V.Get) * Capacity);
	return V;
}

static inline void Vec_Push(vec *V, idx Item)
{
	if (V->Size == V->Capacity)
	{
		idx NewCapacity = V->Capacity * 2;
		idx *NewGet = realloc(V->Get, sizeof(*V->Get) * NewCapacity);
		assert(NewGet);
		V->Get = NewGet;
		V->Capacity = NewCapacity;
	}

	V->Get[V->Size++] = Item;
}

static inline token ParseMonkey(str_view *Text, idx Expected)
{
	token Tok;

	vec Items = Vec_New(128);
	SkipUntil(Text, ' ');
	str_view Id = ReadUntil(Text, ':');
	SkipUntil(Text, ':'); SkipUntil(Text, ' ');
	str_view ItemLine = NextLine(Text);
	while (ItemLine.Length > 0)
	{
		str_view Num = ReadUntil(&ItemLine, ','); ReadUntil(&ItemLine, ' ');
		Vec_Push(&Items, ReadInt(Num));
	}
	str_view OpLine  = NextLine(Text);
	SkipUntil(&OpLine, 'd'); SkipUntil(&OpLine, ' ');
	str_view Op = ReadUntil(&OpLine, ' ');
	assert(Op.Length == 1);
	str_view NumText = NextLine(&OpLine);
	assert(NumText.Length > 0);
	op Operation;
	if (NumText.Content[0] == 'o')
	{
		Operation.Type = SQ;
		Operation.Num = 0;
	}
	else
	{
		Operation.Type = (Op.Content[0] == '+') ? ADD : MUL;
		Operation.Num  = ReadInt(NumText);
	}
	str_view TestLine  = NextLine(Text);
	SkipUntil(&TestLine, 'y'); SkipUntil(&TestLine, ' ');
	str_view TrueLine  = NextLine(Text);
	SkipUntil(&TrueLine, 'y'); SkipUntil(&TrueLine, ' ');
	str_view FalseLine = NextLine(Text);
	SkipUntil(&FalseLine, 'y'); SkipUntil(&FalseLine, ' ');


	Tok.Items = Items;
	Tok.Test = ReadInt(TestLine);
	Tok.True = ReadInt(TrueLine);
	Tok.False = ReadInt(FalseLine);
	Tok.Operation = Operation;
	assert(Expected == ReadInt(Id));
	assert(Tok.True != Expected);
	assert(Tok.False != Expected);
	return Tok;
}

static inline token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	while (Text.Length > 0)
	{
		token Tok = ParseMonkey(&Text, Result.Count);
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

static inline idx RunOp(op Operation, idx Num)
{
	idx Result;
	switch (Operation.Type)
	{
		break;case SQ: Result = Num * Num;
		break;case ADD: Result = Num + Operation.Num;
		break;case MUL: Result = Num * Operation.Num;
		break;default: assert(!"Bad op");
	}
	return Result;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	i32 *Inspected = calloc(Parsed.Count, sizeof(*Inspected));
	for (idx Turn = 0;
	     Turn < 20;
	     Turn += 1)
	{
		for (idx Monkey = 0;
		     Monkey < Parsed.Count;
		     Monkey += 1)
		{
			token *M = &Parsed.Tokens[Monkey];
			vec *Items = &M->Items;
			Inspected[Monkey] += Items->Size;
			for (idx Item = 0;
			     Item < Items->Size;
			     Item += 1)
			{
				idx Worry = Items->Get[Item];
				idx New = RunOp(M->Operation, Worry);
				idx Bored = New / 3;
				idx Target = (New % M->Test == 0) ? M->True : M->False;
				Vec_Push(&Parsed.Tokens[Target].Items, Bored);
			}
			Items->Size = 0;
		}
	}

	idx Max = 0, Next = 0;
	for (idx Monkey = 0;
	     Monkey < Parsed.Count;
	     Monkey += 1)
	{
		if (Inspected[Monkey] > Max)
		{
			Next = Max;
			Max = Inspected[Monkey];
		}
		else if (Inspected[Monkey] > Next)
		{
			Next = Inspected[Monkey];
		}
	}
	idx Prod = Max * Next;
	fprintf(stderr, "%ld * %ld = %ld\n", Max, Next, Prod);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);

	idx Prod = 1;
	i32 *Inspected = calloc(Parsed.Count, sizeof(*Inspected));
	i8 *True       = calloc(Parsed.Count, sizeof(*True));
	i8 *False      = calloc(Parsed.Count, sizeof(*False));
	i8 *Test       = calloc(Parsed.Count, sizeof(*Test));
	op *Operation  = calloc(Parsed.Count, sizeof(*Operation));

	for (idx Monkey = 0;
	     Monkey < Parsed.Count;
	     Monkey += 1)
	{
		token Tok         = Parsed.Tokens[Monkey];
		Prod              = Prod * Tok.Test;
		Test[Monkey]      = Tok.Test;
		True[Monkey]      = Tok.True;
		False[Monkey]     = Tok.False;
		Operation[Monkey] = Tok.Operation;
	}

	for (idx Monkey = 0;
	     Monkey < Parsed.Count;
	     Monkey += 1)
	{
		vec Items = Parsed.Tokens[Monkey].Items;
		idx Count = Items.Size;
		for (idx Item = 0;
		     Item < Count;
		     Item += 1)
		{
			idx Current = Monkey;
			idx Val = Items.Get[Item];
			for (idx Turn = 0;
			     Turn < 10'000;
			     )
			{
				Inspected[Current] += 1;
				idx New = RunOp(Operation[Current], Val);
				idx Bored;
				if (New > Prod) Bored = New % Prod;
				else Bored = New;
				idx Next;
				if (New % Test[Current] == 0)
				{
					Next = True[Current];
				}
				else
				{
					Next = False[Current];
				}
				Turn += (Current > Next);
				Current = Next;
				Val = Bored;
			}
		}
	}
	idx Max = 0, Next = 0;
	for (idx Monkey = 0;
	     Monkey < Parsed.Count;
	     Monkey += 1)
	{
		if (Inspected[Monkey] > Max)
		{
			Next = Max;
			Max = Inspected[Monkey];
		}
		else if (Inspected[Monkey] > Next)
		{
			Next = Inspected[Monkey];
		}
	}
	idx Result = Max * Next;
	fprintf(stderr, "%ld * %ld = %ld\n", Max, Next, Result);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
