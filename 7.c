#include "aoc.h"

u32 CharNum(char C)
{
	assert('a' <= C && C <= 'z');
	idx Shift = C - 'a';
	u32 Result = 1u << Shift;

	return Result;
}

typedef enum command_type
{
	Command_LS,
	Command_CD,
} command_type;

typedef struct command
{
	command_type Type;
	str_view Data;
} command;

typedef struct output
{
	idx Size; // -1 means dir
	str_view Name;
} output;

typedef struct token
{
	bool IsOutput;
	union
	{
		command Com;
		output  Out;
	};
} token;

#include "token_array.h"

token_array ParseInput(str_view View)
{
	token_array Result = TokenArray_New(100);

	for (str_view Line = NextLine(&View);
	     Line.Length != 0;
	     Line = NextLine(&View))
	{
		token Tok;
		if (Line.Content[0] == '$')
		{
			str_view Skip    = ReadUntil(&Line, ' ');
			str_view Command = ReadUntil(&Line, ' ');
			str_view Data    = Line;
			(void) Skip;
			Tok.IsOutput = false;
			Tok.Com.Data = Data;
			Tok.Com.Type = (Command.Content[0] == 'l') ? Command_LS : Command_CD;
		}
		else
		{
			str_view Size = ReadUntil(&Line, ' ');
			str_view Name = Line;

			Tok.IsOutput = true;
			Tok.Out.Size = (Size.Content[0] == 'd') ? -1 : ReadInt(Size);
			Tok.Out.Name = Name;
		}
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct dir_entry dir_entry;
struct dir_entry
{
	idx Size;
	str_view Name;
	dir_entry *Prev, *Child, *Parent;
	bool IsDir;
};

dir_entry *File(dir_entry *Empty, dir_entry *Parent, idx Size, str_view Name)
{
	Empty->Size = Size;
	Empty->Name = Name;
	Empty->Prev = NULL;
	Empty->Parent = Parent;
	Empty->IsDir = false;
	return Empty;
}

dir_entry *Dir(dir_entry *Empty, dir_entry *Parent, str_view Name)
{
	Empty->Size = 0;
	Empty->Name = Name;
	Empty->Prev = Empty->Child = NULL;
	Empty->Parent = Parent;
	Empty->IsDir = true;
	return Empty;
}

dir_entry *FindChild(dir_entry *Current, str_view Name)
{
	dir_entry *Found = NULL;
	for (dir_entry *Child = Current->Child;
	     Child != NULL;
	     Child = Child->Prev)
	{
		if (ViewEq(Child->Name, Name))
		{
			Found = Child;
			break;
		}
	}

	return Found;
}

dir_entry *BuildDir(token_array Parsed, idx *Count)
{
	idx EntryMax = Parsed.Count, EntryCount = 0;
	dir_entry *AllEntries = malloc(sizeof(*AllEntries) * EntryMax);
	dir_entry *Root = Dir(&AllEntries[EntryCount++], NULL, (str_view) { .Content = "/", .Length = 1 });
	dir_entry *Current = Root;
	for (idx Line = 0;
	     Line < Parsed.Count;
	     )
	{
		assert(Current->IsDir);
		token Tok = Parsed.Tokens[Line];
		assert(!Tok.IsOutput);
		command Com = Tok.Com;
		switch (Com.Type)
		{
			break;case Command_CD:
			{
				assert(Com.Data.Length > 0);
				if (Com.Data.Length == 1 && Com.Data.Content[0] == '/')
				{
					Current = Root;
				}
				else if (Com.Data.Length == 2
					 && Com.Data.Content[0] == '.'
					 && Com.Data.Content[1] == '.')
				{
					Current = Current->Parent;
					assert(Current);
				}
				else
				{
					Current = FindChild(Current, Com.Data);
					assert(Current);
					assert(Current->IsDir);
				}
				Line += 1;
			}
			break;case Command_LS:
			{
				assert(Com.Data.Length <= 0);
				idx OutputEnd = Line + 1;

				while (OutputEnd < Parsed.Count && Parsed.Tokens[OutputEnd].IsOutput)
				{
					token Tok = Parsed.Tokens[OutputEnd];
					output Out = Tok.Out;
					assert(EntryCount < EntryMax);
					assert(FindChild(Current, Out.Name) == NULL);
					dir_entry *New;
					if (Out.Size == -1)
					{
						New = Dir(&AllEntries[EntryCount++],
							  Current,
							  Out.Name);
					}
					else
					{
						New = File(&AllEntries[--EntryMax],
							   Current,
							   Out.Size, Out.Name);
					}
					New->Prev = Current->Child;
					Current->Child = New;
					OutputEnd += 1;
				}
				Line = OutputEnd;
			}
		}
	}

	for (idx File = EntryMax;
	     File < Parsed.Count;
	     ++File)
	{
		dir_entry *Entry = &AllEntries[File];
		assert(!Entry->IsDir);
		assert(Entry->Parent->IsDir);
		Entry->Parent->Size += Entry->Size;
	}
	for (idx EntryIdx = EntryCount - 1;
	     EntryIdx > 0;
	     --EntryIdx)
	{
		dir_entry *Entry = &AllEntries[EntryIdx];

		assert(Entry->IsDir);
		assert(Entry->Parent->IsDir);
		Entry->Parent->Size += Entry->Size;
	}

	*Count = EntryCount;
	return Root;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	idx EntryCount;
	dir_entry *AllEntries = BuildDir(Parsed, &EntryCount);

	idx Sum = 0;
	for (idx EntryIdx = 0;
	     EntryIdx < EntryCount;
	     ++EntryIdx)
	{
		dir_entry *Entry = &AllEntries[EntryIdx];
		assert(Entry->IsDir);
		if (Entry->Size <= 100'000)
		{
			Sum += Entry->Size;
		}
	}

	fprintf(stderr, "%ld\n", Sum);
	free(AllEntries);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	idx EntryCount;
	dir_entry *Root = BuildDir(Parsed, &EntryCount);

	idx MemoryTotal = 70'000'000;
	idx MemoryNeeded = 30'000'000;
	idx MemoryUsed = Root->Size;
	idx MemoryFree = MemoryTotal - MemoryUsed;
	idx ToFree = MemoryNeeded - MemoryFree;
	assert(ToFree > 0);

	idx Smallest = Root->Size;
	assert(Smallest >= ToFree);
	for (idx EntryIdx = 1;
	     EntryIdx < EntryCount;
	     ++EntryIdx)
	{
		dir_entry *Entry = &Root[EntryIdx];

		assert(Entry->IsDir);
		if (Entry->Size >= ToFree && Entry->Size < Smallest)
			Smallest = Entry->Size;
	}
	fprintf(stderr, "%ld\n", Smallest);
	free(Root);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
