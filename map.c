#define SWAP(L, R) do { typeof(L) Tmp = (L); (L) = (R); (R) = Tmp; } while (0)

static inline u64
BitsFrom(from F)
{
	u64 Bits = 0;
	static_assert(sizeof(F) <= sizeof(Bits));
	memcpy(&Bits, &F, sizeof(F));

	return Bits;
}

static inline idx
StartPosOf(map *Map, from F)
{
	u64 Hash = BitsFrom(F);

	u64 Shift = Map->Shift;
	Hash ^= Hash >> Shift;
	u64 Mul = (Hash * UINT64_C(11400714819323198485));
	u64 Result = (Mul >> Shift);
	assert(Result < (u64) Map->Capacity);

	return (idx)(Result);
}

static inline idx
NextCandidatePos(map *Map, idx Pos)
{
	idx Result = (Pos + 1) & (Map->Capacity - 1);
	return Result;
}

static inline idx
ProbeSequenceLengthOf(map *Map, from F, idx Pos)
{
	idx PSL = -1;

	if (Map_Valid(F))
	{
		idx StartPos = StartPosOf(Map, F);
		PSL = Pos - StartPos;

		if (PSL < 0)
		{
			PSL += Map->Capacity;
		}
	}

	return PSL;
}

static inline void
ShuffleDown(map *Map, idx CurrentPos, idx CurrentPSL)
{
	from CurrentFrom = Map->From[CurrentPos];
	to   CurrentTo   = Map->To[CurrentPos];
	while (CurrentPSL >= 0)
	{
		CurrentPSL += 1;
		idx NextPos = NextCandidatePos(Map, CurrentPos);
		from NextFrom = Map->From[NextPos];
		idx NextPSL = ProbeSequenceLengthOf(Map, NextFrom, NextPos);
		if (NextPSL < CurrentPSL)
		{
			Map->MaxPSL = MAX(CurrentPSL, Map->MaxPSL);
			SWAP(Map->From[NextPos], CurrentFrom);
			SWAP(Map->To[NextPos], CurrentTo);
			CurrentPSL = NextPSL;
		}
		CurrentPos = NextPos;
	}
}

to *Map_Get(map *Map, from F)
{
	idx StartPos = StartPosOf(Map, F);
	idx PSL = 0;
	idx HighestPSL = Map->MaxPSL + 1;
	for (idx Idx = StartPos;
	     PSL <= HighestPSL;
	     Idx = (PSL++, NextCandidatePos(Map, Idx)))
	{
		assert((PSL == 0) || (Idx != StartPos));
		from SavedF = Map->From[Idx];
		if (!Map_Valid(SavedF))
		{
			assert(Map->Occupancy < Map->Capacity);

			Map->From[Idx] = F;
			Map->To[Idx] = Map_Default;
			Map->Occupancy += 1;
			Map->MaxPSL = MAX(PSL, Map->MaxPSL);
			return &Map->To[Idx];
		}
		else if (Map_Eq(SavedF, F))
		{
			return &Map->To[Idx];
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedF, Idx);
			assert(SavedPSL != -1);

			if (SavedPSL < PSL)
			{
				assert(Map->Occupancy < Map->Capacity);

				ShuffleDown(Map, Idx, SavedPSL);
				Map->From[Idx] = F;
				Map->To[Idx] = Map_Default;
				Map->Occupancy += 1;
				Map->MaxPSL = MAX(PSL, Map->MaxPSL);
				return &Map->To[Idx];
			}
		}

	}
	assert(!"Should not happen");
	__builtin_unreachable();
}

bool Map_TryInserting(map *Map, from F)
{
	idx StartPos = StartPosOf(Map, F);
	idx PSL = 0;
	idx HighestPSL = Map->MaxPSL + 1;
	for (idx Idx = StartPos;
	     PSL <= HighestPSL;
	     Idx = (PSL++, NextCandidatePos(Map, Idx)))
	{
		assert((PSL == 0) || (Idx != StartPos));
		from SavedF = Map->From[Idx];
		if (!Map_Valid(SavedF))
		{
			assert(Map->Occupancy < Map->Capacity);
			Map->From[Idx] = F;
			Map->To[Idx] = Map_Default;
			Map->Occupancy += 1;
			Map->MaxPSL = MAX(PSL, Map->MaxPSL);
			return true;
		}
		else if (Map_Eq(SavedF, F))
		{
			return false;
		}
		else
		{
			idx SavedPSL = ProbeSequenceLengthOf(Map, SavedF, Idx);
			assert(SavedPSL != -1);

			if (SavedPSL < PSL)
			{
				assert(Map->Occupancy < Map->Capacity);
				ShuffleDown(Map, Idx, SavedPSL);
				Map->From[Idx] = F;
				Map->To[Idx] = Map_Default;
				Map->Occupancy += 1;
				Map->MaxPSL = MAX(PSL, Map->MaxPSL);

				return true;
			}
		}

	}
	assert(!"Should not happen");
	__builtin_unreachable();
}

map Map_New(idx Capacity)
{
	map Map;

	assert((Capacity & (Capacity - 1)) == 0);
	assert(Capacity != 0);

	Map.Shift     = sizeof(Capacity) * 8 - __builtin_ctzll(Capacity);
	Map.Capacity  = Capacity;
	Map.Occupancy = 0;
	Map.MaxPSL    = 0;
	Map.From      = malloc(Capacity * sizeof(*Map.From));
	Map.To        = malloc(Capacity * sizeof(*Map.To));

	for (idx I = 0;
	     I < Capacity;
	     ++I)
	{
		Map.From[I] = Map_Invalid;
	}

	return Map;
}

#undef SWAP
