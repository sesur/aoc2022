#include "aoc.h"

typedef idx token;

#include "token_array.h"
idx SkipSnafu(str_view *Text)
{
	idx Number = 0;
	idx Current = 0;
	while (Current < Text->Length)
	{
		switch (Text->Content[Current++])
		{
			break;case '=': Number = Number * 5 + -2;
			break;case '-': Number = Number * 5 + -1;
			break;case '0': Number = Number * 5 + 0;
			break;case '1': Number = Number * 5 + 1;
			break;case '2': Number = Number * 5 + 2;
			break;default:
			{
				goto end;
			}
		}
	}
end:
	Skip(Text, Current);
	return Number;
}

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	while (!Empty(Text))
	{
		TokenArray_Push(&Result, SkipSnafu(&Text));
	}

	return Result;
}

char *AsSnafu(idx Num)
{
	assert(Num >= 0);
#define MAX_LENGTH 29
	static char Buffer[MAX_LENGTH] = {0};
	static char Digit[] =
	{
		'0', '1', '2', '=', '-',
	};

	idx CurrentDigit = MAX_LENGTH - 1;
#define DIGIT(X) do {					\
		assert(CurrentDigit > 0);		\
		assert(X >= 0 && X < 5);	\
		Buffer[--CurrentDigit] = Digit[(X)];	\
	} while (0)
	do
	{
		idx D = Num % 5;
		DIGIT(D);
		idx Value = (D > 2) ? D - 5 : D;
		Num -= Value;
		Num /= 5;
	} while (Num > 0);

	return Buffer + CurrentDigit;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	idx Sum = 0;
	LOOP(I, Parsed.Count)
	{
		Sum += Parsed.Tokens[I];
	}

	fprintf(stderr, "%s\n", AsSnafu(Sum));
	return 0;
}

part *Parts[] = { &Part1 };
