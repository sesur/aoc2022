#include "aoc.h"

typedef struct token
{
	idx Name;
	idx Pressure;

	idx Count;
	idx *Connections;
} token;

#include "token_array.h"
idx NameNumber(str_view Name)
{
	assert(Name.Length == 2);
	idx Lower = Name.Content[0] - 'A';
	idx Upper = Name.Content[1] - 'A';
	assert((Lower & ~0b11111) == 0);
	assert((Upper & ~0b11111) == 0);
	idx Result = Lower | Upper << 5;
	assert((Result & 0b11111) == Lower);
	assert((Result >> 5) == Upper);
	return Result;
}

void SkipUntilTunnel(str_view *Text)
{
	char const *Content = Text->Content;
	idx Length = Text->Length;

	while (Length > 0)
	{
		char Current = *Content;
		if (Current >= 'A' && Current <= 'Z') break;
		Length -= 1;
		Content  += 1;
	}

	Text->Length = Length;
	Text->Content = Content;
}

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (str_view Line = NextLine(&Text);
	     !Empty(Line);
	     Line = NextLine(&Text))
	{
		token Tok;
		SkipUntil(&Line, ' ');
		str_view Name = ReadUntil(&Line, ' ');
		SkipUntil(&Line, '=');
		idx Pressure = SkipNumber(&Line);
		SkipUntilTunnel(&Line);
		idx Capacity = 8;
		idx Count = 0;
		idx *Connections = malloc(Capacity * sizeof(*Connections));
		while (!Empty(Line))
		{
			if (Capacity == Count)
			{
				Capacity *= 2;
				Connections = realloc(Connections, Capacity * sizeof(*Connections));
			}
			str_view Tunnel = ReadUntil(&Line, ',');
			Skip(&Line, 1); // ' '
			Connections[Count++] = NameNumber(Tunnel);
		}
		Tok.Name = NameNumber(Name);
		Tok.Pressure = Pressure;
		Tok.Count = Count;
		Tok.Connections = Connections;
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct state
{
	idx Loc;
	idx MinutesLeft;
	idx PressureRelieved;
	u64 Visited;
} state;

idx Solve(state StartState, idx PosCount, idx PosPressure[PosCount],
	  idx Count, idx Costs[][Count], idx Pressure[Count])
{
	idx MaxRelieve = 0;
	idx Capacity = 128;
	idx Size = 0;
	state *Stack = malloc(Capacity * sizeof(*Stack));

#define POP() Stack[--Size]
#define PUSH(State) do {						\
		if (Size == Capacity)					\
		{							\
			Capacity *= 2;					\
			Stack = realloc(Stack, Capacity * sizeof(*Stack)); \
		}							\
		idx Place = Size;					\
		Stack[Place] = (State);					\
		Size += 1;						\
	} while (0)

	PUSH(StartState);
	while (0 != Size)
	{
		state Current = POP();
		idx Loc = Current.Loc;
		idx Relieved = Current.PressureRelieved;
		idx MinutesLeft = Current.MinutesLeft;
		u64 Visited = Current.Visited | (UINT64_C(1) << Loc);
		for (idx I = 0;
		     I < PosCount;
		     I += 1)
		{
			idx Next = PosPressure[I];
			if ((Visited & (UINT64_C(1) << Next)) == 0)
			{
				// 0 cost == unreachable
				assert(Costs[Loc][Next] != INT64_MAX);
				assert(Costs[Loc][Next] > 0);
				idx MinutesWhenEntered = MinutesLeft - Costs[Loc][Next];
				idx MinutesAfterRelease = MinutesWhenEntered - 1;
				if (MinutesAfterRelease > 0)
				{
					idx PossibleRelease = Relieved + Pressure[Next] * MinutesAfterRelease;
					if (PossibleRelease > MaxRelieve) MaxRelieve = PossibleRelease;
					state NextState;
					NextState.Loc = Next;
					NextState.MinutesLeft = MinutesAfterRelease;
					NextState.PressureRelieved = PossibleRelease;
					NextState.Visited = Visited;
					PUSH(NextState);
				}
			}
		}
	}
	return MaxRelieve;
#undef PUSH
#undef POP
}

void ComputeCosts(idx Count,
		  idx Costs[][Count],
		  idx Connections[Count],
		  idx Counts[Count], idx Starts[Count])
{
	for (idx X = 0; X < Count; ++X)
	{
		for (idx Y = 0; Y < Count; ++Y)
		{
			Costs[X][Y] = INT64_MAX;
		}
	}

	for (idx StartPos = 0; StartPos < Count; ++StartPos)
	{
		for (idx I = 0; I < Counts[StartPos]; ++I)
		{
			Costs[StartPos][Connections[Starts[StartPos] + I]] = 1;
		}
	}
	for (idx StartPos = 0; StartPos < Count; ++StartPos)
	{
		Costs[StartPos][StartPos] = 0;
	}

	for (idx K = 0; K < Count; ++K)
	{
		for (idx I = 0; I < Count; ++I)
		{
			if (Costs[I][K] == INT64_MAX) continue;
			for (idx J = 0; J < Count; ++J)
			{
				if (Costs[K][J] == INT64_MAX) continue;
				idx NewCost = Costs[I][K] + Costs[K][J];
				if (Costs[I][J] > NewCost) Costs[I][J] = NewCost;
			}
		}
	}
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	assert(Parsed.Count < 64);
	idx Renames[1 << 10];
	idx ConnectionCount = 0;
	for (idx I = 0;
	     I < Parsed.Count;
	     I += 1)
	{
		token Tok = Parsed.Tokens[I];
		assert(Tok.Name < (1 << 10));
		Renames[Tok.Name] = I;
		ConnectionCount += Tok.Count;
	}

	idx *Start  = malloc(Parsed.Count * sizeof(*Start));
	idx *Count = malloc(Parsed.Count * sizeof(*Count));
	idx *Pressure = malloc(Parsed.Count * sizeof(*Pressure));
	idx *Connections = malloc(ConnectionCount * sizeof(*Connections));
	idx CurPos = -1;

	{
		idx NumConnection = 0;

		for (idx I = 0;
		     I < Parsed.Count;
		     I += 1)
		{
			token Tok = Parsed.Tokens[I];
			if (Tok.Name == 0)
			{
				assert(CurPos == -1);
				CurPos = I;
			}
			Start[I] = NumConnection;
			Count[I] = Tok.Count;
			Pressure[I] = Tok.Pressure;

			for (idx J = 0;
			     J < Tok.Count;
			     J += 1)
			{
				Connections[NumConnection + J] = Renames[Tok.Connections[J]];
			}
			NumConnection += Tok.Count;
		}
	}
	assert(CurPos != -1);

	idx MaxRelieve = 0;

	idx (*Costs)[Parsed.Count] = calloc(sizeof(*Costs), Parsed.Count);
	ComputeCosts(Parsed.Count, Costs, Connections, Count, Start);

	idx PosCount = 0;
	idx *PosPressure = malloc(sizeof(*PosPressure) * Parsed.Count);
	for (idx I = 0;
	     I < Parsed.Count;
	     I += 1)
	{
		if (Pressure[I]) PosPressure[PosCount++] = I;
	}
	state StartState = { .Loc = CurPos, .MinutesLeft = 30, .PressureRelieved = 0, .Visited = 0 };
	MaxRelieve = Solve(StartState, PosCount, PosPressure,
			   Parsed.Count, Costs, Pressure);

	fprintf(stderr, "%ld\n", MaxRelieve);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	assert(Parsed.Count < 64);
	idx Renames[1 << 10];
	idx ConnectionCount = 0;
	for (idx I = 0;
	     I < Parsed.Count;
	     I += 1)
	{
		token Tok = Parsed.Tokens[I];
		assert(Tok.Name < (1 << 10));
		Renames[Tok.Name] = I;
		ConnectionCount += Tok.Count;
	}

	idx *Start  = malloc(Parsed.Count * sizeof(*Start));
	idx *Count = malloc(Parsed.Count * sizeof(*Count));
	idx *Pressure = malloc(Parsed.Count * sizeof(*Pressure));
	idx *Connections = malloc(ConnectionCount * sizeof(*Connections));
	idx CurPos = -1;

	{
		idx NumConnection = 0;

		for (idx I = 0;
		     I < Parsed.Count;
		     I += 1)
		{
			token Tok = Parsed.Tokens[I];
			if (Tok.Name == 0)
			{
				assert(CurPos == -1);
				CurPos = I;
			}
			Start[I] = NumConnection;
			Count[I] = Tok.Count;
			Pressure[I] = Tok.Pressure;

			for (idx J = 0;
			     J < Tok.Count;
			     J += 1)
			{
				Connections[NumConnection + J] = Renames[Tok.Connections[J]];
			}
			NumConnection += Tok.Count;
		}
	}
	assert(CurPos != -1);

	idx MaxRelieve = 0;

	idx (*Costs)[Parsed.Count] = calloc(sizeof(*Costs), Parsed.Count);
	ComputeCosts(Parsed.Count, Costs, Connections, Count, Start);

	idx PosCount = 0;
	idx *AllPosPressure = malloc(sizeof(*AllPosPressure) * Parsed.Count);
	idx *PosPressure = malloc(sizeof(*PosPressure) * Parsed.Count);
	for (idx I = 0;
	     I < Parsed.Count;
	     I += 1)
	{
		if (Pressure[I]) AllPosPressure[PosCount++] = I;
	}

	idx NumTries = INT64_C(1) << PosCount;
	idx *Maximums = calloc(NumTries, sizeof(*Maximums));
	for (idx I = 0;
	     I < NumTries;
	    ++I)
	{
		idx ValveCount = __builtin_popcount(I);
		assert(ValveCount <= 15);
		idx Max = 0;
		for (idx P = 0; P < PosCount; ++P)
		{
			if (I & (1 << P))
			{
				PosPressure[Max++] = AllPosPressure[P];
			}
		}
		assert(Max == ValveCount);
		state StartState = { .Loc = CurPos, .MinutesLeft = 26, .PressureRelieved = 0, .Visited = 0 };
		Maximums[I] = Solve(StartState, Max, PosPressure,
				    Parsed.Count, Costs, Pressure);

	}

	u64 Mask = NumTries - 1;
	assert(__builtin_popcount(Mask) == PosCount);
	for (idx I = 1;
	     I < NumTries - 1;
	     ++I)
	{
		idx J = (~I) & Mask;
		idx Relieve = Maximums[I] + Maximums[J];
		if (Relieve > MaxRelieve) MaxRelieve = Relieve;
	}

	fprintf(stderr, "%ld\n", MaxRelieve);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
