#include "aoc.h"
#include <assert.h>

typedef struct token
{
	char Enemy;
	char Answer;
} token;

#include "token_array.h"

token_array ParseText(str_view Text)
{
	token_array Parsed = TokenArray_New(40);
	char const *Current = Text.Content;
	char const *End     = Text.Content + Text.Length;

	while (Current + 2 < End)
	{
		token Tok;
		Tok.Enemy = Current[0];
		assert(Current[1] == ' ');
		Tok.Answer = Current[2];
		Current += 4;
		TokenArray_Push(&Parsed, Tok);
	}
	return Parsed;
}

typedef enum status
{
	Status_LOSS = 0,
	Status_DRAW = 1,
	Status_WIN = 2
} status;

typedef enum play
{
	Play_ROCK,
	Play_PAPER,
	Play_SCISSOR,
} play;

play Play_FromEnemy(char Play)
{
	assert(Play >= 'A' && Play <= 'C');
	play Result = Play - 'A';
	return Result;
}

play Play_FromAnswer(char Play)
{
	assert(Play >= 'X' && Play <= 'Z');

	play Result = Play - 'X';
	return Result;
}

status Status[3][3] = {
	[Play_SCISSOR] = {[Play_SCISSOR] = Status_DRAW, [Play_ROCK] = Status_WIN,  [Play_PAPER] = Status_LOSS},
	[Play_ROCK]    = {[Play_SCISSOR] = Status_LOSS, [Play_ROCK] = Status_DRAW, [Play_PAPER] = Status_WIN},
	[Play_PAPER]   = {[Play_SCISSOR] = Status_WIN,  [Play_ROCK] = Status_LOSS, [Play_PAPER] = Status_DRAW},
};

idx Value[3] = {[Play_SCISSOR] = 3, [Play_ROCK] = 1, [Play_PAPER] = 2};

PART(Part1)
{
	token_array Parsed = ParseText(Text);

	idx WinPoints = 0;
	idx RoundPoints = 0;

	for (idx Round = 0;
	     Round < Parsed.Count;
	     ++Round)
	{
		play Attack = Play_FromEnemy(Parsed.Tokens[Round].Enemy);
		play Defense = Play_FromAnswer(Parsed.Tokens[Round].Answer);
		WinPoints += 3 * Status[Attack][Defense];
		RoundPoints += Value[Defense];
	}

	idx Points = WinPoints + RoundPoints;

	fprintf(stderr, "Points: %ld\n", Points);

	return 0;
}

status Status_FromChar(char Play)
{
	assert(Play >= 'X' && Play <= 'Z');

	status Result = Play - 'X';
	return Result;
}

PART(Part2)
{
	token_array Parsed = ParseText(Text);

	idx WinPoints = 0;
	idx RoundPoints = 0;

	play Plays[3][3] =
		{
			[Status_WIN] =
			{ [Play_ROCK] = Play_PAPER,
			  [Play_SCISSOR] = Play_ROCK,
			  [Play_PAPER] = Play_SCISSOR },
			[Status_LOSS] =
			{ [Play_ROCK] = Play_SCISSOR,
			  [Play_SCISSOR] = Play_PAPER,
			  [Play_PAPER] = Play_ROCK },
			[Status_DRAW] =
			{ [Play_ROCK] = Play_ROCK,
			  [Play_SCISSOR] = Play_SCISSOR,
			  [Play_PAPER] = Play_PAPER },
		};

	for (idx Round = 0;
	     Round < Parsed.Count;
	     ++Round)
	{
		play Attack = Play_FromEnemy(Parsed.Tokens[Round].Enemy);
		status SupposedStatus = Status_FromChar(Parsed.Tokens[Round].Answer);
		play Defense = Plays[SupposedStatus][Attack];
		WinPoints += 3 * Status[Attack][Defense];
		RoundPoints += Value[Defense];
	}

	idx Points = WinPoints + RoundPoints;

	fprintf(stderr, "Points: %ld\n", Points);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
