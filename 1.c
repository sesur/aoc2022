#include "aoc.h"
#include <x86intrin.h>

typedef struct elf
{
	idx Count;
	idx *Calories;
} elf;

typedef struct token
{
	idx Number;
} token;

#define TOK(Num) ((token) { .Number = (Num) })
token const SEP = {-1};

bool IsSeparator(token Tok)
{
	bool IsToken = Tok.Number == SEP.Number;

	return IsToken;
}

#include "token_array.h"

token_array ParseText(str_view Text)
{
	token_array Array = TokenArray_New(50);

	char const *Cursor = Text.Content;
	char const *End    = Text.Content + Text.Length;

	while (Cursor != End)
	{
		if (*Cursor == '\n')
		{
			TokenArray_Push(&Array, SEP);
			Cursor += 1;
		}
		else
		{
			idx Number = atoi(Cursor);
			TokenArray_Push(&Array, TOK(Number));
			char const *Newline = Cursor + 1;
			while ((Newline < (End - 1)) && (*Newline != '\n'))
			{
				Newline += 1;
			}
			Cursor = Newline + 1;
		}
	}

	return Array;
}

elf ParseElf(token_array Array)
{
	elf Result = {0};

	idx NumFoods = 0;
	for (idx Idx = 0;
	     Idx < Array.Count;
	     ++Idx)
	{
		if (IsSeparator(Array.Tokens[Idx]))
		{
			break;
		}
		else
		{
			NumFoods += 1;
		}
	}

	idx *Calories = malloc(sizeof(*Calories) * NumFoods);

	for (idx Food = 0;
	     Food < NumFoods;
	     ++Food)
	{
		Calories[Food] = Array.Tokens[Food].Number;
	}

	Result.Count = NumFoods;
	Result.Calories = Calories;

	return Result;
}

idx CalorieCount(elf Elf)
{
	idx Count = 0;

	for (idx Food = 0;
	     Food < Elf.Count;
	     ++Food)
	{
		Count += Elf.Calories[Food];
	}

	return Count;
}

PART(Part1)
{
	token_array Array = ParseText(Text), Rest = Array;

	elf Current = ParseElf(Array);
	idx MaxCalories = CalorieCount(Current);
	TokenArray_Drop(&Rest, Current.Count);

	while (TokenArray_Drop(&Rest, 1))
	{
		Current = ParseElf(Rest);
		idx Calories = CalorieCount(Current);
		if (Calories > MaxCalories)
		{
			MaxCalories = Calories;
		}
		TokenArray_Drop(&Rest, Current.Count);
	}

	fprintf(stderr, "Maximum: %ld\n", MaxCalories);
	return 0;
}

PART(Part2)
{
	token_array Array = ParseText(Text);

	idx *Calories = malloc(sizeof(*Calories) * Array.Count);
	token_array Rest = Array;
	idx NumElves = 0;

	do
	{
		elf Elf = ParseElf(Rest);
		Calories[NumElves++] = CalorieCount(Elf);
		TokenArray_Drop(&Rest, Elf.Count);
	}
	while (TokenArray_Drop(&Rest, 1));

	__m256i Max = _mm256_setzero_si256();

	for (idx Current = 0;
	     Current < NumElves;
	     ++Current)
	{
		__m256i Val = _mm256_set1_epi64x(Calories[Current]);
		__m256i Comp = _mm256_cmpgt_epi64(Val, Max);

		u8 Mask = _mm256_movemask_pd(_mm256_castsi256_pd(Comp)) & 0b111;

		if (Mask)
		{
			assert(Mask == 7 || Mask == 3 || Mask == 1);
			__m256i NewMax = _mm256_castpd_si256(_mm256_blend_pd(_mm256_castsi256_pd(Max),
									     _mm256_castsi256_pd(Val),
									     0b1000));
			__m256i Shuffled;
			switch (Mask)
			{
				break;case 7: Shuffled = _mm256_permute4x64_epi64(NewMax, 0b00'11'10'01);
				break;case 3: Shuffled = _mm256_permute4x64_epi64(NewMax, 0b00'10'11'01);
				break;case 1: Shuffled = _mm256_permute4x64_epi64(NewMax, 0b00'10'01'11);
				break;default: __builtin_unreachable();
			}

			Max = Shuffled;
		}


	}

	idx Sum = Max[0] + Max[1] + Max[2];

	fprintf(stderr, "Max: %ld\n", Sum);

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
