#include "aoc.h"

typedef enum move
{
	LEFT = '<',
	RIGHT = '>'
} move;

typedef enum rock
{
	VERT,
	PLUS,
	EL,
	HORI,
	SQ,
	COUNT
} rock;

typedef u64 block;

char const *AsString(block Block)
{
	// blocks are a area of 4 * 9 slots
	// represented by the last 36 bits of the
	// underlying integer representation.
	// Since this might be hard to visualise
	// this function returns a string representation
	// of the block.
	// Not safe to use in a regular program; for debugging
	// purposes only!
	static char Buffer[41] = {0};
	idx Current = 0;
	for (idx Y = 0;
	     Y < 4;
	     Y += 1)
	{
		for (idx X = 0;
		     X < 9;
		     X += 1)
		{
			bool IsBlock = Block & 1;
			char C = IsBlock ? '#' : '.';
			Buffer[Current++] = C;
			Block >>= 1;
		}
		Buffer[Current++] = '\n';
	}
	return Buffer;
}

block SidewaysBlock(block B, move Move)
{
	block Result;
	switch (Move)
	{
		break;case LEFT:
		{
			Result = B >> 1;
		}
		break;case RIGHT:
		{
			Result = B << 1;
		}
		break;default: __builtin_unreachable();
	}
	return Result;
}

block DownBlock(block B)
{
	block Result = B << 9;
	return Result;
}

block UpBlock(block B)
{
	block Result = B >> 9;
	return Result;
}

void PrintTower(idx Size, block Tower[static Size + 1])
{
	for (idx Y = 0;
	     Y <= Size;
	     Y += 1)
	{
		block Block = Tower[Size - Y];
		for (idx X = 0;
		     X < 9;
		     X += 1)
		{
			bool IsBlock = Block & 1;
			char C = IsBlock ? '#' : '.';
			fputc(C, stdout);
			Block >>= 1;
		}
		puts("");
	}
}

PART(Part1)
{
	u64 One = 1;

	block Shapes[] =
	{
		[VERT] = (One << 3) | (One << 4) | (One << 5) | (One << 6),
		[PLUS] = (One << 4) | (One << 12) | (One << 13) | (One << 14) | (One << 22),
		[EL]   = (One << 5) | (One << 14) | (One << 23) | (One << 22) | (One << 21),
		[HORI] = (One << 3) | (One << 12) | (One << 21) | (One << 30),
		[SQ]   = (One << 3) | (One << 4) | (One << 12) | (One << 13),
	};

	idx Heights[] =
	{
		[VERT] = 1,
		[PLUS] = 3,
		[EL]   = 3,
		[HORI] = 4,
		[SQ]   = 2,
	};

	block RightWall = (One << 8) | (One << 17) | (One << 26) | (One << 35);
	block LeftWall  = (One << 0) | (One << 9) | (One << 18) | (One << 27);
	block Wall      = LeftWall | RightWall;

	idx Capacity = 512;
	idx Size = 0;
	block *Tower = calloc(sizeof(*Tower), Capacity);
	// moving floor down = going up!
	for (idx I = 0; I < Capacity; ++I)
	{
		Tower[I] = Wall;
	}
	Tower[0] |= 0b011111110;
	Tower[1] |= DownBlock(Tower[0]);
	Tower[2] |= DownBlock(Tower[1]);
	Tower[3] |= DownBlock(Tower[2]);

	assert(Text.Length >= 0);
	if (Text.Content[Text.Length - 1] == '\n') Text.Length -= 1;

	idx CurrentJet = 0;
	idx SkippedSize = 0;

	for (idx Rock = 0;
	     Rock < 2022;
	     Rock += 1)
	{
		rock R = Rock % COUNT;
		block Current = Shapes[R];
		idx CurrentHeight = Size + 3 + Heights[R];
		if (Capacity <= CurrentHeight)
		{
			idx OldCapacity = Capacity;
			Capacity *= 2;
			assert(Capacity > CurrentHeight + 4);
			Tower = realloc(Tower, sizeof(*Tower) * Capacity);
			for (idx I = OldCapacity; I < Capacity; ++I)
			{
				Tower[I] = Wall;
			}
		}

		while (1)
		{
			move Move = Text.Content[CurrentJet++];
			assert(Move == '<' || Move == '>');
			if (CurrentJet == Text.Length) CurrentJet = 0;
			block Moved = SidewaysBlock(Current, Move);
			block Blockers = Tower[CurrentHeight];
			if (!(Blockers & Moved))
			{
				Current = Moved;
			}

			CurrentHeight -= 1;
			assert(CurrentHeight < Capacity);
			assert(CurrentHeight >= 0);
			block Top = Tower[CurrentHeight];
			if (Top & Current)
			{
				CurrentHeight += 1;
				assert(CurrentHeight >= Heights[R]);
				block Copy = DownBlock(Current);
				for (idx I = 1; I < 4; ++I)
				{
					Tower[CurrentHeight+I] |= Copy;
					Copy = DownBlock(Copy);
				}
				for (idx I = 0; I < Heights[R]; ++I)
				{
					Tower[CurrentHeight-I] |= Current;
					Current = UpBlock(Current);
				}
				if (CurrentHeight > Size) Size = CurrentHeight;
				break;
			}
		}
	}

	fprintf(stderr, "%ld", Size + SkippedSize);
	return 0;
}

bool IsFakeBottom(block B)
{
	// a fake bottom is a block through which no
	// tetris block can go; as such we can think of them
	// as a new start
	u64 Full = 0b111111111;
	u64 Result = B | (B >> 9) | (B >> 18) | (B >> 27);
	Result = Result & Full;
	return Result == Full;
}

typedef struct state
{
	idx Rock;
	idx Size;
} state;

typedef struct tower_state
{
	block Top;
	idx Jet;
} tower_state;

typedef struct stack
{
	idx Capacity, Count;
	state *State;
	tower_state *Tower;
} stack;

stack Stack_New(idx Capacity)
{
	stack New;

	New.Capacity = Capacity;
	New.Count     = 0;
	New.State     = malloc(sizeof(*New.State) * Capacity);
	New.Tower      = malloc(sizeof(*New.Tower) * Capacity);

	return New;
}

idx Stack_FindOrPush(stack *Stack, idx Rock, idx Size, block Top, idx Jet)
{
	if (Stack->Count >= Stack->Capacity)
	{
		idx NewCapacity = Stack->Capacity * 2;
		Stack->State = realloc(Stack->State, sizeof(*Stack->State) * NewCapacity);
		Stack->Tower = realloc(Stack->Tower, sizeof(*Stack->Tower) * NewCapacity);
		Stack->Capacity = NewCapacity;
	}
	for (idx I = 0; I < Stack->Count; ++I)
	{
		if (Stack->Tower[I].Top == Top && Stack->Tower[I].Jet == Jet)
		{
			return I;
		}
	}
	tower_state Tower = { .Top = Top, .Jet = Jet };
	state State = { .Rock = Rock, .Size = Size };
	Stack->Tower[Stack->Count] = Tower;
	Stack->State[Stack->Count] = State;
	Stack->Count += 1;
	return -1;
}


PART(Part2)
{
	u64 One = 1;

	block Shapes[] =
	{
		[VERT] = (One << 3) | (One << 4) | (One << 5) | (One << 6),
		[PLUS] = (One << 4) | (One << 12) | (One << 13) | (One << 14) | (One << 22),
		[EL]   = (One << 5) | (One << 14) | (One << 23) | (One << 22) | (One << 21),
		[HORI] = (One << 3) | (One << 12) | (One << 21) | (One << 30),
		[SQ]   = (One << 3) | (One << 4) | (One << 12) | (One << 13),
	};

	idx Heights[] =
	{
		[VERT] = 1,
		[PLUS] = 3,
		[EL]   = 3,
		[HORI] = 4,
		[SQ]   = 2,
	};

	block RightWall = (One << 8) | (One << 17) | (One << 26) | (One << 35);
	block LeftWall  = (One << 0) | (One << 9) | (One << 18) | (One << 27);
	block Wall      = LeftWall | RightWall;

	idx Capacity = 512;
	idx Size = 0;
	block *Tower = calloc(sizeof(*Tower), Capacity);
	// moving floor down = going up!
	for (idx I = 0; I < Capacity; ++I)
	{
		Tower[I] = Wall;
	}
	Tower[0] |= 0b011111110;
	Tower[1] |= DownBlock(Tower[0]);
	Tower[2] |= DownBlock(Tower[1]);
	Tower[3] |= DownBlock(Tower[2]);

	assert(Text.Length >= 0);
	if (Text.Content[Text.Length - 1] == '\n') Text.Length -= 1;

	idx CurrentJet = 0;

	stack Stack[COUNT];
	for (idx I = 0;
	     I < COUNT;
	     ++I)
	{
		Stack[I] = Stack_New(1024);
	}
	idx SkippedSize = 0;

	idx NumRocks = 1'000'000'000'000;
	for (idx Rock = 0;
	     Rock < NumRocks;
	     Rock += 1)
	{
		rock R = Rock % COUNT;
		block Current = Shapes[R];
		idx CurrentHeight = Size + 3 + Heights[R];
		if (Capacity <= CurrentHeight)
		{
			idx OldCapacity = Capacity;
			Capacity *= 2;
			assert(Capacity > CurrentHeight + 4);
			Tower = realloc(Tower, sizeof(*Tower) * Capacity);
			for (idx I = OldCapacity; I < Capacity; ++I)
			{
				Tower[I] = Wall;
			}
		}
		block Top = Tower[Size];
		if (SkippedSize == 0 && IsFakeBottom(Top))
		{
			idx Idx = Stack_FindOrPush(&Stack[R], Rock, Size, Top, CurrentJet);
			if (Idx >= 0)
			{
				idx LastSize = Stack[R].State[Idx].Size;
				idx LastRock = Stack[R].State[Idx].Rock;
				assert(Size > LastSize);
				assert(Rock > LastRock);
				idx SizeDiff = Size - LastSize;
				idx RockDiff = Rock - LastRock;
				idx RocksToGo = NumRocks - Rock;
				idx Repeats = RocksToGo / RockDiff;
				SkippedSize += SizeDiff * Repeats;
				Rock += Repeats * RockDiff;
				assert(Rock < NumRocks);
			}
		}

		while (1)
		{
			move Move = Text.Content[CurrentJet++];
			assert(Move == '<' || Move == '>');
			if (CurrentJet == Text.Length) CurrentJet = 0;
			block Moved = SidewaysBlock(Current, Move);
			block Blockers = Tower[CurrentHeight];
			if (!(Blockers & Moved))
			{
				Current = Moved;
			}

			CurrentHeight -= 1;
			assert(CurrentHeight < Capacity);
			assert(CurrentHeight >= 0);
			block Top = Tower[CurrentHeight];
			if (Top & Current)
			{
				CurrentHeight += 1;
				assert(CurrentHeight >= Heights[R]);
				block Copy = DownBlock(Current);
				for (idx I = 1; I < 4; ++I)
				{
					Tower[CurrentHeight+I] |= Copy;
					Copy = DownBlock(Copy);
				}
				for (idx I = 0; I < Heights[R]; ++I)
				{
					Tower[CurrentHeight-I] |= Current;
					Current = UpBlock(Current);
				}
				if (CurrentHeight > Size) Size = CurrentHeight;
				break;
			}
		}
	}
	fprintf(stderr, "%ld", Size + SkippedSize);
	return 0;
}


part *Parts[] = { &Part1, &Part2 };
