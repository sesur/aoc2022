stack Stack_New(idx Capacity)
{
	stack S;
	S.Items = malloc(Capacity * sizeof(*S.Items));
	S.Size = 0;
	S.Capacity = Capacity;
	return S;
}

void Stack_Push(stack *S, item Item)
{
	if (S->Capacity == S->Size)
	{
		S->Capacity *= 2;
		S->Items = realloc(S->Items, S->Capacity * sizeof(*S->Items));
	}
	S->Items[S->Size++] = Item;
}

bool Stack_Empty(stack S)
{
	return S.Size == 0;
}

item Stack_Pop(stack *S)
{
	assert(S->Size > 0);
	item Result = S->Items[--S->Size];
	return Result;
}
