#include "aoc.h"
#include <assert.h>

typedef struct token
{
	u8 Count, From, To;
} token;

#include "token_array.h"

typedef struct stack
{
	idx Size, Capacity;
	char *Values;
} stack;

typedef struct stacks
{
	idx Count;
	stack *Stacks;
} stacks;

stack Stack_New(idx Capacity)
{
	stack New;

	New.Capacity = Capacity;
	New.Size     = 0;
	New.Values   = malloc(sizeof(*New.Values) * Capacity);

	return New;
}

void Stack_PushMany(stack *Stack, idx Count, char *Values)
{
	if (Stack->Size + Count > Stack->Capacity)
	{
		idx NewCapacity = Stack->Capacity * 10 + Count;
		char *Values = realloc(Stack->Values, sizeof(*Stack->Values) * NewCapacity);
		assert(Values);
		Stack->Capacity = NewCapacity;
		Stack->Values   = Values;
	}
	assert(Stack->Size + Count <= Stack->Capacity);
	// memcpy(Stack->Values + Stack->Size, Values, Count);
	for (idx I = 0;
	     I < Count;
	     ++I)
	{
		Stack->Values[Stack->Size + I] = Values[Count - 1 - I];
	}
	Stack->Size += Count;
}

void Stack_AppendMany(stack *Stack, idx Count, char *Values)
{
	if (Stack->Size + Count > Stack->Capacity)
	{
		idx NewCapacity = Stack->Capacity * 10 + Count;
		char *Values = realloc(Stack->Values, sizeof(*Stack->Values) * NewCapacity);
		assert(Values);
		Stack->Capacity = NewCapacity;
		Stack->Values   = Values;
	}
	assert(Stack->Size + Count <= Stack->Capacity);
	memcpy(Stack->Values + Stack->Size, Values, Count);
	Stack->Size += Count;
}

void Stack_PushFront(stack *Stack, char Value)
{
	if (Stack->Size == Stack->Capacity)
	{
		idx NewCapacity = Stack->Capacity * 10 + 1;
		char *Values = realloc(Stack->Values, sizeof(*Stack->Values) * NewCapacity);
		assert(Values);
		Stack->Capacity = NewCapacity;
		Stack->Values   = Values;
	}
	assert(Stack->Size < Stack->Capacity);
	memmove(Stack->Values+1, Stack->Values, Stack->Size);
	Stack->Values[0] = Value;
	Stack->Size += 1;
}

stacks Stacks_New(idx Count)
{
	stacks Result;

	Result.Count = Count;
	Result.Stacks = malloc(sizeof(*Result.Stacks) * Count);

	for (idx Current = 0;
	     Current < Count;
	     ++Current)
	{
		Result.Stacks[Current] = Stack_New(16);
	}

	return Result;
}
stacks ParseStacks(str_view *View)
{
	str_view Line = NextLine(View);

	assert(((Line.Length + 1) & 0b11) == 0);
	idx NumStacks = (Line.Length + 1) / 4;
	stacks Result = Stacks_New(NumStacks);

	while (Line.Length != 0)
	{
		for (idx Stack = 0;
		     Stack < NumStacks;
		     ++Stack)
		{
			assert(4 * Stack + 1 < Line.Length);
			char Value = Line.Content[4 * Stack + 1];
			if (Value <= '9' && Value >= '1')
			{
				break;
			}
			else if (Value != ' ')
			{
				Stack_PushFront(&Result.Stacks[Stack], Value);
			}
		}
		Line = NextLine(View);
	}
	return Result;
}

token_array ParseInstructions(str_view *View)
{
	token_array Parsed = TokenArray_New(40);

	for (str_view Line = NextLine(View);
	     Line.Length != 0;
	     Line = NextLine(View))
	{
		token Tok;
		str_view _Move = ReadUntil(&Line, ' ');
		str_view Count = ReadUntil(&Line, ' ');
		str_view _From = ReadUntil(&Line, ' ');
		str_view From  = ReadUntil(&Line, ' ');
		str_view _To   = ReadUntil(&Line, ' ');
		str_view To    = Line;
		(void) _Move;
		(void) _From;
		(void) _To;
		Tok.Count = ReadInt(Count);
		Tok.From  = ReadInt(From);
		Tok.To    = ReadInt(To);
		TokenArray_Push(&Parsed, Tok);
	}

	return Parsed;
}

PART(Part1)
{
	stacks Stacks = ParseStacks(&Text);
	token_array Instructions = ParseInstructions(&Text);

	for (idx Ins = 0;
	     Ins < Instructions.Count;
	     ++Ins)
	{
		token Tok = Instructions.Tokens[Ins];
		idx Count = Tok.Count;
		assert(Tok.From > 0 && Tok.From <= Stacks.Count);
		assert(Tok.To > 0 && Tok.To <= Stacks.Count);
		stack *From = &Stacks.Stacks[Tok.From - 1];
		stack *To = &Stacks.Stacks[Tok.To - 1];
		assert(From->Size >= Count);
		Stack_PushMany(To, Count, &From->Values[From->Size - Count]);
		From->Size -= Count;
	}

	for (idx Stack = 0;
	     Stack < Stacks.Count;
	     ++Stack)
	{
		stack S = Stacks.Stacks[Stack];
		idx Last = S.Size - 1;
		assert(Last >= 0);
		printf("%c", S.Values[Last]);
	}
	puts("");

	return 0;
}

PART(Part2)
{
	stacks Stacks = ParseStacks(&Text);
	token_array Instructions = ParseInstructions(&Text);

	for (idx Ins = 0;
	     Ins < Instructions.Count;
	     ++Ins)
	{
		token Tok = Instructions.Tokens[Ins];
		idx Count = Tok.Count;
		assert(Tok.From > 0 && Tok.From <= Stacks.Count);
		assert(Tok.To > 0 && Tok.To <= Stacks.Count);
		stack *From = &Stacks.Stacks[Tok.From - 1];
		stack *To = &Stacks.Stacks[Tok.To - 1];
		assert(From->Size >= Count);
		Stack_AppendMany(To, Count, &From->Values[From->Size - Count]);
		From->Size -= Count;
	}

	for (idx Stack = 0;
	     Stack < Stacks.Count;
	     ++Stack)
	{
		stack S = Stacks.Stacks[Stack];
		idx Last = S.Size - 1;
		assert(Last >= 0);
		printf("%c", S.Values[Last]);
	}
	puts("");

	return 0;
}

part *Parts[] = { &Part1, &Part2 };
