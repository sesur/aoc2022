#include "aoc.h"

typedef struct pt
{
	idx X, Y;
} pt;
typedef struct token
{
	pt Sensor;
	pt Beacon;
} token;

typedef struct span
{
	idx Start, End;
} span;

typedef struct row
{
	idx Capacity, Size;
	span *Cols;
} row;

row Row_New(idx Capacity)
{
	row New;
	New.Capacity = Capacity;
	New.Size = 0;
	New.Cols = malloc(Capacity * sizeof(*New.Cols));
	return New;
}

void Row_MaybeEnlarge(row *Row)
{
	if (Row->Size == Row->Capacity)
	{
		Row->Capacity *= 2;
		Row->Cols = realloc(Row->Cols, Row->Capacity * sizeof(*Row->Cols));
	}
}

idx FindStart(idx Count, span Sorted[Count], span Span)
{
	idx Start = 0;
	idx End = Count;

	idx Val = Span.Start;

	while (Start < End)
	{
		idx Middle = (Start + End) / 2;
		if (Val <= Sorted[Middle].End)
		{
			End = Middle;
		}
		else if (Val > Sorted[Middle].End)
		{
			Start = Middle + 1;
		}
	}
	assert(Start == End);

	return Start;
}

idx FindEnd(idx Count, span Sorted[Count], span Span)
{
	idx Start = 0;
	idx End = Count;

	idx Val = Span.End;

	while (Start < End)
	{
		idx Middle = (Start + End) / 2;
		if (Val < Sorted[Middle].Start)
		{
			End = Middle;
		}
		else if (Val >= Sorted[Middle].Start)
		{
			Start = Middle + 1;
		}
	}
	assert(Start == End);

	return Start;
}

void Row_AssertOk(row *Row)
{
	assert(Row->Size <= Row->Capacity);
	assert(Row->Size >= 0);
	for (idx I = 0;
	     I < Row->Size - 1;
	     I += 1)
	{
		assert(Row->Cols[I].Start < Row->Cols[I].End);
		assert(Row->Cols[I].End < Row->Cols[I+1].Start);
	}
}

void Row_Add(row *Row, span Span)
{
	idx Start = FindStart(Row->Size, Row->Cols, Span);
	idx End = FindEnd(Row->Size, Row->Cols, Span);
	if (Start == End)
	{
			Row_MaybeEnlarge(Row);
			for (idx J = Row->Size;
			     J > Start;
			     J -= 1)
			{
				Row->Cols[J] = Row->Cols[J-1];
			}
			Row->Cols[Start] = Span;
			Row->Size += 1;

	}
	else
	{
		assert(Start < End);
		span SuperSpan = Span;
		span First = Row->Cols[Start];
		span Last = Row->Cols[End - 1];
		if (SuperSpan.Start > First.Start) SuperSpan.Start = First.Start;
		if (SuperSpan.End   < Last.End) SuperSpan.End = Last.End;

		Row->Cols[Start] = SuperSpan;
		idx Offset = End - Start - 1;
		for (idx I = End;
		     I < Row->Size;
		     I += 1)
		{
			Row->Cols[I-Offset] = Row->Cols[I];
		}
		Row->Size -= Offset;
	}

	Row_AssertOk(Row);
}


#include "token_array.h"

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (str_view Line = NextLine(&Text);
	     !Empty(Line);
	     Line = NextLine(&Text))
	{
		token Tok;
		SkipUntil(&Line, '=');
		Tok.Sensor.X = SkipNumber(&Line);
		SkipUntil(&Line, '=');
		Tok.Sensor.Y = SkipNumber(&Line);
		SkipUntil(&Line, '=');
		Tok.Beacon.X = SkipNumber(&Line);
		SkipUntil(&Line, '=');
		Tok.Beacon.Y = SkipNumber(&Line);

		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

idx Pt_Distance(pt Sensor, pt Beacon)
{
	idx Dx = Sensor.X - Beacon.X;
	idx Dy = Sensor.Y - Beacon.Y;

	if (Dx < 0) Dx = -Dx;
	if (Dy < 0) Dy = -Dy;

	idx Distance = Dx + Dy;

	return Distance;
}
PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	idx LineY = 2'000'000;
	if (Parsed.Count < 20)
	{
		LineY = 10;
	}
	row Signals = Row_New(1024);
	row Beacons = Row_New(1024);
	for (idx S = 0;
	     S < Parsed.Count;
	     S += 1)
	{
		token Tok = Parsed.Tokens[S];
		pt Sensor = Tok.Sensor;
		pt Beacon = Tok.Beacon;

		if (Beacon.Y == LineY)
		{
			span Singleton = { .Start = Beacon.X, .End = Beacon.X + 1 };
			Row_Add(&Beacons, Singleton);
		}

		idx Distance = Pt_Distance(Sensor, Beacon);

		idx Dy = Sensor.Y - LineY;
		if (Dy < 0) Dy = -Dy;

		if (Distance >= Dy)
		{
			idx Radius = (Distance - Dy);
			span NotThere = { .Start = Sensor.X - Radius, .End = Sensor.X + Radius + 1 };
			Row_Add(&Signals, NotThere);
		}
	}

	idx NumBlocked = 0;
	for (idx R = 0;
	     R < Signals.Size;
	     R += 1)
	{
		span NotThere = Signals.Cols[R];
		NumBlocked += NotThere.End - NotThere.Start;
	}
	for (idx R = 0;
	     R < Beacons.Size;
	     R += 1)
	{
		span There = Beacons.Cols[R];
		NumBlocked -= There.End - There.Start;
	}
	fprintf(stderr, "%ld\n", NumBlocked);
	return 0;
}

idx TuningFrequency(pt P)
{
	idx Result = 4'000'000 * P.X + P.Y;
	return Result;
}

typedef struct circle
{
	pt Center;
	idx Radius;
} circle;

typedef struct line
{
	pt Start;
	idx Length;
	idx Dy;
} line;

typedef struct vec
{
	idx Count, Capacity;
	line *Get;
} vec;

idx Maximum(idx A, idx B)
{
	return (A >= B) ? A : B;
}

line Restrict(line Line, idx Min, idx Max)
{
	line Result;
	pt Start = Line.Start;
	pt End   = Start;
	End.X += Line.Length;

	idx StartOffset = 0;
	idx EndOffset = 0;

	if (Start.X > Max)
	{
		goto return_empty;
	}
	if (End.X <= Min)
	{
		goto return_empty;
	}
	if (Start.X < Min) StartOffset = Maximum(StartOffset, Min - Start.X);
	if (End.X > Max + 1) EndOffset = Maximum(EndOffset, End.X - Max - 1);

	if (Line.Dy == -1)
	{
		End.Y -= Line.Length;
		if (Start.Y < Min)
		{
			goto return_empty;
		}
		if (End.Y >= Max)
		{
			goto return_empty;
		}

		if (Start.Y > Max) StartOffset = Maximum(StartOffset, Start.Y - Max);
		if (End.Y < Min - 1) EndOffset = Maximum(EndOffset, Min - 1 - End.Y);
	}
	else
	{
		assert(Line.Dy == 1);
		End.Y += Line.Length;
		if (Start.Y > Max)
		{
			goto return_empty;
		}
		if (End.Y <= Min)
		{
			goto return_empty;
		}

		if (Start.Y < Min) StartOffset = Maximum(StartOffset, Min - Start.Y);
		if (End.Y > Max + 1) EndOffset = Maximum(EndOffset, End.Y - Max - 1);
	}

	assert(StartOffset >= 0);
	Result.Length = Line.Length - EndOffset - StartOffset;
	Start.X += StartOffset;
	Start.Y += Line.Dy * StartOffset;
	Result.Start = Start;
	Result.Dy = Line.Dy;
	return Result;

return_empty:
	Result = (line){.Length = 0};
	return Result;
}

void GetOutlines(line Outlines[4], pt Middle, idx Radius)
{
	pt Start = Middle;
	Start.X -= Radius + 1;

	Outlines[0].Start = Start;
	Outlines[0].Dy = 1;
	Outlines[0].Length = Radius + 1;
	Outlines[1].Start = Start;
	Outlines[1].Dy = -1;
	Outlines[1].Length = Radius + 1;
	Outlines[2].Start = Middle;
	Outlines[2].Start.Y -= Radius + 1;
	Outlines[2].Dy = 1;
	Outlines[2].Length = Radius + 1;
	Outlines[3].Start = Middle;
	Outlines[3].Start.Y -= Radius + 1;
	Outlines[3].Dy = -1;
	Outlines[3].Length = Radius + 1;
}

span GetOverlap(pt Middle, idx Radius, line Line)
{
	pt Start = Line.Start;
	idx Dy = Middle.Y - Start.Y;
	idx Steps = Dy * Line.Dy;

	Start.X += Steps;
	Start.Y += Steps * Line.Dy;


	idx Offset = Start.X - Middle.X;
	idx Distance = (Offset >= 0) ? Offset : -Offset;
	span Result;
	if (Distance > Radius)
	{
		Result.Start = 0;
		Result.End = 0;
	}
	else
	{
		// LineNumber is the index of the relevant diagonal
		// counting from the left
		idx LineNumber = Offset + Radius;
		idx S = -(LineNumber / 2);
		idx L = Radius + (LineNumber % 2 == 0);
		Result.Start = S + Steps;
		Result.End   = Result.Start + L;
	}

	if (Result.Start < 0) Result.Start = 0;
	if (Result.End > Line.Length) Result.End = Line.Length;

	return Result;
}

void Test()
{
	{
		pt Middle = (pt) { 0, 0 };
		idx Radius = 4;

		line Line;
		Line.Dy = 1;
		Line.Length = 5;
		Line.Start = (pt) { -1, -2 };
		span S = GetOverlap(Middle, Radius, Line);
		assert(S.Start == 0);
		assert(S.End == 4);
	}

	{
		pt Middle = (pt) { 0, 0 };
		idx Radius = 4;

		line Line;
		Line.Dy = 1;
		Line.Length = 20;
		Line.Start = (pt) { -5, 0 };
		span S = GetOverlap(Middle, Radius, Line);
		assert(S.Start >= S.End);
	}

	{
		pt Middle = (pt) { 0, 0 };
		idx Radius = 4;

		line Line;
		Line.Dy = -1;
		Line.Length = 20;
		Line.Start = (pt) { -1, -2 };
		span S = GetOverlap(Middle, Radius, Line);
		assert(S.Start == 0);
		assert(S.End == 2);
	}


	{
		pt Middle = (pt) { 8, 7 };
		idx Radius = 9;

		line Line;
		Line.Dy = 1;
		Line.Length = 5;
		Line.Start = (pt) { 12, 9 };
		span S = GetOverlap(Middle, Radius, Line);
		assert(S.Start == 0);
		assert(S.End == 2);
	}
}
PART(Part2)
{
#if 0
	Test();
#else
	token_array Parsed = ParseInput(Text);
	idx Min = 0;
	idx Max = 4'000'000;
	if (Parsed.Count < 20)
	{
		Max = 20;
	}

	pt It;
	bool Found = false;
	row NotIt = Row_New(1024);
	line Outlines[4];
	for (idx S = 0;
	     S < Parsed.Count;
	     S += 1)
	{
		token Tok = Parsed.Tokens[S];
		pt Sensor = Tok.Sensor;
		pt Beacon = Tok.Beacon;

		idx Radius = Pt_Distance(Sensor, Beacon);

		GetOutlines(Outlines, Sensor, Radius);
		for (idx Outline = 0;
		     Outline < 4;
		     Outline += 1)
		{
			line Out = Restrict(Outlines[Outline], Min, Max);
			if (Out.Length <= 0) continue;
			NotIt.Size = 0;
			for (idx S2 = 0;
			     S2 < Parsed.Count;
			     S2 += 1)
			{
				if (S == S2) continue;
				token Tok = Parsed.Tokens[S2];
				pt Sensor2 = Tok.Sensor;
				pt Beacon2 = Tok.Beacon;

				idx Radius2 = Pt_Distance(Sensor2, Beacon2);

				span Overlap = GetOverlap(Sensor2, Radius2, Out);
				if (Overlap.Start < Overlap.End)
				{
					Row_Add(&NotIt, Overlap);
				}
			}

			if (NotIt.Size == 0)
			{
				assert(Out.Length == 1);
				It = Out.Start;
				Found = true;
				break;
			}
			else if (NotIt.Size > 1)
			{
				assert(NotIt.Size == 2);
				It = Out.Start;
				It.X += NotIt.Cols[0].End;
				It.Y += Out.Dy * NotIt.Cols[0].End;
				Found = true;
				break;
			}
			else
			{
				span Single = NotIt.Cols[0];
				if (Single.Start != 0)
				{
					It = Out.Start;
					Found = true;
					break;
				}
				else if (Single.End != Out.Length)
				{
					It = Out.Start;
					It.X += (Out.Length - 1);
					It.Y += Out.Dy * (Out.Length - 1);
					Found = true;
					break;
				}
			}
		}
	}
	assert(Found);

	fprintf(stderr, "%ld\n", TuningFrequency(It));
#endif
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
