#include "aoc.h"

typedef struct recipe
{
	idx Requirements[2];
} recipe;

typedef enum rock
{
	ORE,
	CLAY,
	OBSIDIAN,
	GEODE,
	COUNT
} rock;

typedef struct token
{
	idx Id;
	recipe Recipes[COUNT];
} token;

#include "token_array.h"

recipe ReadRecipe(str_view *Text)
{
	recipe Result = {0};
	idx Current = 0;

	idx Cursor = 0;

	char C;
#define UPDATE() do { Text->Content += Cursor; Text->Length -= Cursor; Cursor = 0; } while (0)

	while ((C = Text->Content[Cursor]) != '.')
	{
		assert(Cursor < Text->Length);
		if (C >= '0' && C <= '9')
		{
			UPDATE();
			assert(Current < 2);
			Result.Requirements[Current++] = SkipNumber(Text);
		}
		else
		{
			Cursor += 1;
		}
	}
	Cursor += 1;
	UPDATE();

	assert(Current >= 1);
	return Result;
#undef UPDATE
}

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (;
	     !Empty(Text);
	     SkipUntil(&Text, '\n'))
	{
		token Tok;
		SkipUntil(&Text, ' ');
		Tok.Id = SkipNumber(&Text);
		for (idx I = 0;
		     I < COUNT;
		     I += 1)
		{
			Tok.Recipes[I] = ReadRecipe(&Text);
		}
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct item
{
	i16 MinutesLeft, Upper;
	i16 NumRobots[COUNT];
	i16 ResourcesGathered[COUNT];
} item;

#include "stack.h"

static void SetupCosts(i16 ConstructionCosts[COUNT][COUNT],
		i16 Max[COUNT],
		token Blueprint)
{
	ConstructionCosts[ORE][ORE]      = Blueprint.Recipes[ORE].Requirements[0];
	ConstructionCosts[ORE][CLAY]     = 0;
	ConstructionCosts[ORE][OBSIDIAN] = 0;
	ConstructionCosts[ORE][GEODE]    = 0;

	ConstructionCosts[CLAY][ORE]      = Blueprint.Recipes[CLAY].Requirements[0];
	ConstructionCosts[CLAY][CLAY]     = 0;
	ConstructionCosts[CLAY][OBSIDIAN] = 0;
	ConstructionCosts[CLAY][GEODE]    = 0;

	ConstructionCosts[OBSIDIAN][ORE]      = Blueprint.Recipes[OBSIDIAN].Requirements[0];
	ConstructionCosts[OBSIDIAN][CLAY]     = Blueprint.Recipes[OBSIDIAN].Requirements[1];
	ConstructionCosts[OBSIDIAN][OBSIDIAN] = 0;
	ConstructionCosts[OBSIDIAN][GEODE]    = 0;

	ConstructionCosts[GEODE][ORE]      = Blueprint.Recipes[GEODE].Requirements[0];
	ConstructionCosts[GEODE][CLAY]     = 0;
	ConstructionCosts[GEODE][OBSIDIAN] = Blueprint.Recipes[GEODE].Requirements[1];
	ConstructionCosts[GEODE][GEODE]    = 0;
	Max[ORE] = MAX(MAX(ConstructionCosts[CLAY][ORE], ConstructionCosts[OBSIDIAN][ORE]),
		       ConstructionCosts[GEODE][ORE]);
	Max[CLAY] = ConstructionCosts[OBSIDIAN][CLAY];
	Max[OBSIDIAN] = ConstructionCosts[GEODE][OBSIDIAN];
	Max[GEODE] = INT16_MAX;
}

static idx ProductionTime(i16 Costs[COUNT],
		   i16 ProductionRate[COUNT],
		   i16 Have[COUNT])
{
	idx ProductionTime = 0;

	for (idx Type = 0;
	     Type < COUNT;
	     Type += 1)
	{
		idx Delta = Costs[Type] - Have[Type];
		if (Delta > 0)
		{
			idx Rate = ProductionRate[Type];
			assert(Rate >= 0);
			if (Rate > 0)
			{
				// divide rounding up!
				idx TimeNeeded = (Delta + Rate - 1) / Rate;
				ProductionTime = MAX(TimeNeeded, ProductionTime);
			}
			else
			{
				return INT64_MAX;
			}
		}

		assert(Costs[Type] <= Have[Type] + ProductionTime * ProductionRate[Type]);
	}

	return ProductionTime + 1;
}

static idx UpperLimit(item Current, idx ObsidianCost)
{
	// how many geodes could we crack if obsidian robots were free
	// and we had unlimited ore ?
	idx ObsidianCount = Current.ResourcesGathered[OBSIDIAN];
	idx GeodeCount = Current.ResourcesGathered[GEODE];

	idx ORobotCount = Current.NumRobots[OBSIDIAN];
	idx GRobotCount = Current.NumRobots[GEODE];

	LOOP(I, Current.MinutesLeft)
	{
		ObsidianCount += ORobotCount;
		GeodeCount += GRobotCount;
		if (ObsidianCount >= ObsidianCost)
		{
			GRobotCount += 1;
		}
		else
		{
			ORobotCount += 1;
		}
	}
	return GeodeCount;
}

idx FindMaxGeodes(idx TotalTime, token Blueprint)
{
	i16 ConstructionCosts[COUNT][COUNT];
	i16 MaxUsefulProduction[COUNT];
	SetupCosts(ConstructionCosts, MaxUsefulProduction, Blueprint);

	item Start =
	{
		.MinutesLeft = TotalTime,
		.Upper = 1, // just has to be > 0
		.NumRobots = { [ORE] = 1 },
		.ResourcesGathered = {0}
	};

	static stack S = {0};
	if (S.Items == NULL)
	{
		S = Stack_New(1024);
	}

	Stack_Push(&S, Start);
	idx MaxGeodes = 0;

	while (!Stack_Empty(S))
	{
		item Current = Stack_Pop(&S);
		assert(Current.MinutesLeft > 0);

		if (Current.Upper <= MaxGeodes) continue;

		bool HasNextStep = false;
		LOOP(I, COUNT)
		{
			// the later robots are worth more, so try starting with those
			idx NextProduced = COUNT - 1 - I;
			if (Current.NumRobots[NextProduced] >= MaxUsefulProduction[NextProduced]) continue;
			idx TimeNeeded = ProductionTime(ConstructionCosts[NextProduced],
							Current.NumRobots,
							Current.ResourcesGathered);
			if (TimeNeeded < Current.MinutesLeft)
			{
				HasNextStep = true;
				item Next = Current;
				Next.MinutesLeft -= TimeNeeded;
				Next.NumRobots[NextProduced] += 1;
				for (idx Type = 0;
				     Type < COUNT;
				     Type += 1)
				{
					Next.ResourcesGathered[Type] -= ConstructionCosts[NextProduced][Type];
					Next.ResourcesGathered[Type] +=
						TimeNeeded * Current.NumRobots[Type];
				}
				Next.Upper = UpperLimit(Next, ConstructionCosts[GEODE][OBSIDIAN]);
				if (Next.Upper <= MaxGeodes) continue;
				Stack_Push(&S, Next);
			}
		}

		if (!HasNextStep)
		{
			idx GeodeCount = Current.ResourcesGathered[GEODE]
				+ Current.NumRobots[GEODE] * Current.MinutesLeft;
			MaxGeodes = MAX(MaxGeodes, GeodeCount);
		}
	}

	return MaxGeodes;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);

	idx Quality = 0;
	for (idx Blueprint = 0;
	     Blueprint < Parsed.Count;
	     Blueprint += 1)
	{
		idx MaxGeodes = FindMaxGeodes(24, Parsed.Tokens[Blueprint]);
		idx Current = Parsed.Tokens[Blueprint].Id * MaxGeodes;
		Quality += Current;
	}

	fprintf(stderr, "%ld\n", Quality);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);

	Parsed.Count = MIN(3, Parsed.Count);
	idx Prod = 1;
	for (idx Blueprint = 0;
	     Blueprint < Parsed.Count;
	     Blueprint += 1)
	{
		idx MaxGeodes = FindMaxGeodes(32, Parsed.Tokens[Blueprint]);
		Prod *= MaxGeodes;
	}

	fprintf(stderr, "%ld\n", Prod);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
