#include "aoc.h"

typedef enum wind
{
	NONE,
	LEFT,
	RIGHT,
	UP,
	DOWN
} wind;
typedef struct grid
{
	idx Width, Height;
	idx Start, End;
	wind *Winds;
} grid;

grid ParseInput(str_view Text)
{
	idx Length = Text.Length;

	str_view Line = NextLine(&Text);
	idx Width = Line.Length - 2;
	idx Height = (Length + Line.Length) / (Line.Length + 1) - 2;
	idx Start = -1;
	LOOP(I, Width)
	{
		if (Line.Content[I+1] == '.')
		{
			Start = I;
			break;
		}
		else
		{
			assert(Line.Content[I+1] == '#');
		}
	}
	assert(Start != -1);

	wind (*Winds)[Width] = malloc(sizeof(*Winds) * Height);

	for (idx Y = 0;
	     Y < Height;
	     SkipUntil(&Text, '\n'), Y += 1)
	{
		Skip(&Text, 1);
		for (idx X = 0;
		     X < Width;
		     X += 1)
		{
			char C = Text.Content[X];
			wind W;
			switch (C)
			{
				break;case '<':
				{
					W = LEFT;
				}
				break;case '>':
				{
					W = RIGHT;
				}
				break;case '^':
				{
					W = UP;
				}
				break;case 'v':
				{
					W = DOWN;
				}
				break;case '.':
				{
					W = NONE;

				}
				break;default:
				{
					assert(0);
				}
			}
			Winds[Y][X] = W;
		}
	}
	idx End = -1;
	LOOP(I, Width)
	{
		if (Text.Content[I+1] == '.')
		{
			End = I;
			break;
		}
		else
		{
			assert(Text.Content[I+1] == '#');
		}
	}
	assert(End != -1);

	grid Result;
	Result.Width  = Width;
	Result.Height = Height;
	Result.Start  = Start;
	Result.End    = End;
	Result.Winds  = (wind *)Winds;
	return Result;
}

typedef struct pt
{
	i8 X, Y;
} pt;

typedef struct item
{
	pt P;
	i16 Minutes;
} item;

idx Manhattan(pt A, pt B)
{
	idx Dx = B.X - A.X;
	idx Dy = B.Y - A.Y;

	idx ADx = MAX(Dx, -Dx);
	idx ADy = MAX(Dy, -Dy);

	idx Sum = ADx + ADy;

	return Sum;
}

bool Eq(pt A, pt B)
{
	if (A.X == B.X && A.Y == B.Y) return true;
	else                          return false;
}

typedef struct queue
{
	idx Top, Bottom, Capacity;
	idx *Minutes;
	pt  *Points;

} queue;

queue New(idx Capacity)
{
	queue New;
	New.Top      = 0;
	New.Bottom   = 0;
	New.Capacity = Capacity;

	New.Minutes  = malloc(sizeof(*New.Minutes) * Capacity);
	New.Points   = malloc(sizeof(*New.Points) * Capacity);

	return New;
}

bool NotEmpty(queue *Q)
{
	return Q->Top != Q->Bottom;
}

item Dequeue(queue *Q)
{
	assert(NotEmpty(Q));
	idx Mask = Q->Capacity - 1;
	item I;
	I.Minutes = Q->Minutes[Q->Bottom & Mask];
	I.P       = Q->Points[Q->Bottom & Mask];
	Q->Bottom += 1;
	return I;
}

void Enqueue(queue *Q, idx Minutes, pt Cur)
{
	if (Q->Top == Q->Bottom + Q->Capacity)
	{
		// resize queue
		queue N = New(Q->Capacity * 2);
		idx Top = 0;
		for (idx I = Q->Bottom;
		     I < Q->Top;
		     I += 1)
		{
			N.Minutes[Top] = Q->Minutes[I & (Q->Capacity - 1)];
			N.Points[Top] = Q->Points[I & (Q->Capacity - 1)];
			Top += 1;
		}

		free(Q->Points);
		free(Q->Minutes);
		N.Top = Top;
		*Q = N;
	}

	idx Mask = Q->Capacity - 1;

	Q->Points[Q->Top & Mask] = Cur;
	Q->Minutes[Q->Top & Mask] = Minutes;
	Q->Top += 1;
}

bool Check(grid G, pt Pos, idx Minute)
{
	bool Possible;
	if (Pos.Y == -1 && Pos.X == G.Start)
	{
		Possible = true;
	}
	else if (Pos.Y == G.Height && Pos.X == G.End)
	{
		Possible = true;
	}
	else if (Pos.X < 0 || Pos.X >= G.Width || Pos.Y < 0 || Pos.Y >= G.Height)
	{
		Possible = false;
	}
	else
	{
		idx Left  = (Pos.X + Minute) % G.Width;
		idx Right = (Pos.X - Minute) % G.Width;
		idx Up    = (Pos.Y + Minute) % G.Height;
		idx Down  = (Pos.Y - Minute) % G.Height;

		wind (*Winds)[G.Width] = (wind (*)[G.Width]) G.Winds;
		if (Winds[Pos.Y][Left] == LEFT)
		{
			Possible = false;
		}
		else if (Winds[Pos.Y][(Right + G.Width) % G.Width] == RIGHT)
		{
			Possible = false;
		}
		else if (Winds[Up][Pos.X] == UP)
		{
			Possible = false;
		}
		else if (Winds[(Down + G.Height) % G.Height][Pos.X] == DOWN)
		{
			Possible = false;
		}
		else
		{
			Possible = true;
		}
	}
	return Possible;
}

int GreatestDiv(idx A, idx B)
{
    idx Temp;
    while (B != 0)
    {
        Temp = A % B;

        A = B;
        B = Temp;
    }
    return A;
}

idx LowestMult(idx A, idx B)
{
	idx G = GreatestDiv(A, B);
	idx NoCommonFactors = A / G;
	return B * NoCommonFactors;
}

typedef struct rep
{
	pt P;
	i16 Rep;
} rep;

typedef rep from;
typedef i16 to;

static rep const Map_Invalid = { .Rep = - 1 };
static i16 const Map_Default = INT16_MAX;

static inline bool Map_Valid(rep R)
{
	bool Valid = (R.Rep >= 0);
	return Valid;
}

static inline bool Map_Eq(rep A, rep B)
{
	return (A.Rep == B.Rep) && Eq(A.P, B.P);
}

#include "map.h"

idx FindPath(grid G, idx StartMinutes, pt Start, pt End)
{
	static queue Q = { .Capacity = 0 };
	idx LCM = LowestMult(G.Width, G.Height);

	if (Q.Capacity == 0)
	{
		Q = New(1024);
	}
	else
	{
		Q.Top = Q.Bottom = 0;
	}

	Enqueue(&Q, StartMinutes, Start);

	idx MinutesUsed = 0;

	pt Delta[5] =
	{
		{-1,  0},
		{ 1,  0},
		{ 0, -1},
		{ 0,  1},
		{ 0,  0}
	};

	map M = Map_New(1 << 18);
	while (1)
	{
		item Current = Dequeue(&Q);
		if (Eq(Current.P, End))
		{
			MinutesUsed = Current.Minutes;
			break;
		}
		else
		{
			idx Minutes = Current.Minutes + 1;
			LOOP(I, 5)
			{
				pt P = Current.P;
				P.X += Delta[I].X;
				P.Y += Delta[I].Y;

				if (Check(G, P, Minutes))
				{
					rep This;
					This.Rep = Minutes % LCM;
					This.P   = P;

					if (Map_TryInserting(&M, This))
					{
						Enqueue(&Q, Minutes, P);
					}
				}
			}
		}
	}
	free(M.From);
	free(M.To);

	return MinutesUsed;
}

PART(Part1)
{
	grid Parsed = ParseInput(Text);

	pt Start = { .X = Parsed.Start, .Y = -1 };
	pt End   = { .X = Parsed.End,   .Y = Parsed.Height - 1 };

	idx MinutesUsed = FindPath(Parsed, 0, Start, End) + 1;

	fprintf(stderr, "%ld\n", MinutesUsed);
	return 0;
}

PART(Part2)
{
	grid Parsed = ParseInput(Text);
	pt Start = { .X = Parsed.Start, .Y = -1 };
	pt End   = { .X = Parsed.End,   .Y = Parsed.Height };
	pt AlmostStart = { .X = Parsed.Start, .Y = 0 };
	pt AlmostEnd   = { .X = Parsed.End,   .Y = Parsed.Height - 1 };

	idx FirstTrip  = FindPath(Parsed, 0, Start, AlmostEnd) + 1;
	idx SecondTrip = FindPath(Parsed, FirstTrip, End, AlmostStart) + 1;
	idx ThirdTrip  = FindPath(Parsed, SecondTrip, Start, AlmostEnd) + 1;

	fprintf(stderr, "%ld\n", ThirdTrip);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
