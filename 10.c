#include "aoc.h"

typedef enum inst
{
	NOOP, ADDX
} inst;

idx Timings[] = { [NOOP] = 1, [ADDX] = 2 };

typedef struct token
{
	inst Inst;
	idx Val;
} token;

#include "token_array.h"

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (str_view Line = NextLine(&Text);
	     Line.Length != 0;
	     Line = NextLine(&Text))
	{
		token Tok;
		str_view Inst = ReadUntil(&Line, ' ');
		if (Inst.Content[0] == 'a')
		{
			idx Val = ReadInt(Line);
			Tok.Val = Val;
			Tok.Inst = ADDX;
		}
		else
		{
			Tok.Inst = NOOP;
			Tok.Val = 0;
		}
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);

	idx CurrentCycle = 1;
	idx NextRead = 20;
	idx Register = 1;
	idx SignalSum = 0;
	for (idx Instruction = 0;
	     Instruction < Parsed.Count;
	     Instruction += 1)
	{
		token Tok = Parsed.Tokens[Instruction];
		idx Timing = Timings[Tok.Inst];
		if (CurrentCycle + Timing > NextRead)
		{
			SignalSum += NextRead * Register;
			NextRead += 40;
			if (NextRead > 220) break;
		}
		CurrentCycle += Timing;
		Register += Tok.Val;
	}
	fprintf(stderr, "%ld\n", SignalSum);
	return 0;
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	idx PC = 0;
#define NextInstruction() Parsed.Tokens[PC++]
	idx Register = 1, Next = 1;
	idx Wait = 0;
	char Buffer[6 * 41 + 1];
	idx CurrentPixel = 0;
	for (idx CurrentCycle = 1;
	     CurrentCycle <= 240;
	     CurrentCycle += 1)
	{
		if (Wait == 0)
		{
			Register = Next;
			token Tok = NextInstruction();
			Wait = Timings[Tok.Inst];
			Next = Register + Tok.Val;
		}

		idx Pos = (CurrentCycle - 1) % 40 + 1;
		if (Register <= Pos && Pos <= Register + 2)
		{

			Buffer[CurrentPixel++] = '#';
		}
		else
		{
			Buffer[CurrentPixel++] = '.';
		}
		if (Pos == 40) Buffer[CurrentPixel++] = '\n';
		Wait -= 1;
	}
	Buffer[CurrentPixel] = '\0';
	fputs(Buffer, stderr);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
