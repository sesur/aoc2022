typedef struct token_array
{
	idx Count;
	idx Capacity;
	token *Tokens;
} token_array;

token_array TokenArray_New(idx Capacity);
void TokenArray_Push(token_array *Array, token Tok);
bool TokenArray_Drop(token_array *Array, idx Num);
