#include "aoc.h"

typedef struct token
{
	union
	{
		struct
		{
			idx X, Y, Z;
		};
		idx Pos[3];
	};
} token;

#include "token_array.h"

token_array ParseInput(str_view Text)
{
	token_array Result = TokenArray_New(100);

	for (;
	     !Empty(Text);
	     SkipUntil(&Text, '\n'))
	{
		token Tok;
		Tok.X = SkipNumber(&Text);
		Skip(&Text, 1);
		Tok.Y = SkipNumber(&Text);
		Skip(&Text, 1);
		Tok.Z = SkipNumber(&Text);
		TokenArray_Push(&Result, Tok);
	}

	return Result;
}

typedef struct
{
	idx Min, Max;
} range;

idx Normalize(idx X, range R)
{
	assert(R.Min <= X && X <= R.Max);
	return X - R.Min;
}

range ExpandToInclude(range R, idx X)
{
	R.Min = MIN(R.Min, X);
	R.Max = MAX(R.Max, X);
	return R;
}

idx Length(range R)
{
	idx Length = R.Max - R.Min + 1;
	Length = MAX(0, Length);
	return Length;
}

typedef struct lava_cube
{
	range Dims[3];
	bool *Droplets;
} lava_cube;

lava_cube MakeCube(token_array Droplets)
{
	range Dims[3] = {
		{ .Min = INT64_MAX,  .Max = INT64_MIN },
		{ .Min = INT64_MAX,  .Max = INT64_MIN },
		{ .Min = INT64_MAX,  .Max = INT64_MIN },
	};

	for (idx Cube = 0;
	     Cube < Droplets.Count;
	     Cube += 1)
	{
		token Tok = Droplets.Tokens[Cube];
		LOOP(I, 3)
		{
			Dims[I] = ExpandToInclude(Dims[I], Tok.Pos[I]);
		}
	}
	// give one unit of air around the cube.  This will make stuff easier
	// later, since we do not have to consider the "real" border as a special
	// case anymore
	Dims[0].Min -= 1;
	Dims[0].Max += 1;
	Dims[1].Min -= 1;
	Dims[1].Max += 1;
	Dims[2].Min -= 1;
	Dims[2].Max += 1;
	idx const NumX = Length(Dims[0]);
	idx const NumY = Length(Dims[1]);
	idx const NumZ = Length(Dims[2]);

	bool (*HasDroplet)[NumY][NumZ] =
		calloc(NumX, sizeof(*HasDroplet));
	for (idx Cube = 0;
	     Cube < Droplets.Count;
	     Cube += 1)
	{
		token Tok = Droplets.Tokens[Cube];
		idx X = Normalize(Tok.X, Dims[0]);
		idx Y = Normalize(Tok.Y, Dims[1]);
		idx Z = Normalize(Tok.Z, Dims[2]);
		HasDroplet[X][Y][Z] = true;
	}

	lava_cube Cube;
	LOOP(I, 3)
	{
		Cube.Dims[I] = Dims[I];
	}
	Cube.Droplets = (bool *) HasDroplet;
	return Cube;
}

idx SurfaceOf(lava_cube Cube, token_array Cubes)
{
	range XDim = Cube.Dims[0];
	range YDim = Cube.Dims[1];
	range ZDim = Cube.Dims[2];

	idx const NumX [[maybe_unused]] = Length(XDim);
	idx const NumY = Length(YDim);
	idx const NumZ = Length(ZDim);

	bool (*HasDroplet)[NumY][NumZ] = (bool (*)[NumY][NumZ]) Cube.Droplets;

	idx Surface = 0;
	for (idx Idx = 0;
	     Idx < Cubes.Count;
	     Idx += 1)
	{
		token Tok = Cubes.Tokens[Idx];
		idx X = Normalize(Tok.X, XDim);
		idx Y = Normalize(Tok.Y, YDim);
		idx Z = Normalize(Tok.Z, ZDim);
		Surface += !HasDroplet[X-1][Y][Z];
		Surface += !HasDroplet[X+1][Y][Z];
		Surface += !HasDroplet[X][Y-1][Z];
		Surface += !HasDroplet[X][Y+1][Z];
		Surface += !HasDroplet[X][Y][Z-1];
		Surface += !HasDroplet[X][Y][Z+1];
	}

	return Surface;
}

PART(Part1)
{
	token_array Parsed = ParseInput(Text);
	lava_cube Cube = MakeCube(Parsed);

	idx Surface = SurfaceOf(Cube, Parsed);

	fprintf(stderr, "%ld\n", Surface);
	return 0;
}

typedef struct pt
{
	idx X, Y, Z;
} pt;

typedef struct stack
{
	idx Capacity;
	idx Size;
	pt *Items;
} stack;

stack New(idx Capacity)
{
	stack S;
	S.Items = malloc(Capacity * sizeof(*S.Items));
	S.Size = 0;
	S.Capacity = Capacity;
	return S;
}

void Push(stack *S, idx X, idx Y, idx Z)
{
	if (S->Capacity == S->Size)
	{
		S->Capacity *= 2;
		S->Items = realloc(S->Items, S->Capacity * sizeof(*S->Items));
	}
	S->Items[S->Size++] = (pt) { .X = X, .Y = Y, .Z = Z };
}

pt Pop(stack *S)
{
	assert(S->Size > 0);
	pt Result = S->Items[--S->Size];
	return Result;
}

void EliminateAirPockets(lava_cube Cube)
{
	range XDim = Cube.Dims[0];
	range YDim = Cube.Dims[1];
	range ZDim = Cube.Dims[2];

	idx const NumX = Length(XDim);
	idx const NumY = Length(YDim);
	idx const NumZ = Length(ZDim);

	bool (*HasDroplet)[NumY][NumZ] = (bool (*)[NumY][NumZ]) Cube.Droplets;
	// Reachable = true means reachable from water / steam
	bool (*Reachable)[NumY][NumZ] = calloc(NumX, sizeof(*Reachable));
	stack S = New(1024);

#define PUSH(X, Y, Z) do { assert(Reachable[X][Y][Z] == false); Reachable[X][Y][Z] = true; Push(&S, X, Y, Z); } while (0)
#define TRY(X, Y, Z) do {						\
			if (!HasDroplet[X][Y][Z] && !Reachable[X][Y][Z])	\
			{						\
				PUSH(X, Y, Z);				\
			}						\
		} while (0)

	PUSH(0, 0, 0);
	while (S.Size > 0)
	{
		pt P = Pop(&S);
		idx X = P.X;
		idx Y = P.Y;
		idx Z = P.Z;

		idx Left = X-1;
		idx Right = X+1;
		idx Down = Y-1;
		idx Up = Y+1;
		idx Back = Z-1;
		idx Front = Z+1;

		if (Left >= 0)
		{
			TRY(Left, Y, Z);
		}
		if (Right < NumX)
		{
			TRY(Right, Y, Z);
		}
		if (Down >= 0)
		{
			TRY(X, Down, Z);
		}
		if (Up < NumY)
		{
			TRY(X, Up, Z);
		}
		if (Back >= 0)
		{
			TRY(X, Y, Back);
		}
		if (Front < NumZ)
		{
			TRY(X, Y, Front);
		}
		assert(S.Size >= 0);
	}

	LOOP(X, NumX)
	{
		LOOP(Y, NumY)
		{
			LOOP(Z, NumZ)
			{
				HasDroplet[X][Y][Z] = !Reachable[X][Y][Z];
			}
		}
	}
	free(Reachable);
}

PART(Part2)
{
	token_array Parsed = ParseInput(Text);
	lava_cube Cube = MakeCube(Parsed);
	EliminateAirPockets(Cube);

	idx Surface = SurfaceOf(Cube, Parsed);

	fprintf(stderr, "%ld\n", Surface);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
