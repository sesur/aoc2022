#include "aoc.h"
#include <x86intrin.h>

typedef struct grid
{
	idx Width, Height;
	i8 *Grid;
} grid;

static grid ParseInput(str_view View)
{
	idx Complete = View.Length;
	str_view Line = NextLine(&View);
	idx Width = Line.Length;
	idx MaxHeight = Complete / Width;

	i8 (*Grid)[Width] = malloc(MaxHeight * sizeof(*Grid));
	idx Height = 0;
	while (Line.Length != 0)
	{
		assert(Line.Length == Width);
		assert(Height < MaxHeight);
		idx Y = Height++;
		for (idx X = 0;
		     X < Width;
		     ++X)
		{
			Grid[Y][X] = Line.Content[X] - '0';
		}
		Line = NextLine(&View);
	}

	grid Result;
	Result.Width = Width;
	Result.Height = Height;
	Result.Grid = (i8 *) Grid;
	return Result;
}

PART(Part1)
{
	grid Grid = ParseInput(Text);

	bool (*Visibility)[Grid.Width] = calloc(Grid.Height, sizeof(*Visibility));
	i8 (*G)[Grid.Width] = (i8 (*)[Grid.Width]) Grid.Grid;
        // horizontal
	for (idx Y = 0;
	     Y < Grid.Height;
	     ++Y)
	{
		idx Left = -1;
		idx Right = -1;
		for (idx X = 0;
		     X < Grid.Width;
		     ++X)
		{
			if (G[Y][X] > Left)
			{
				Visibility[Y][X] = true;
				Left = G[Y][X];
			}
			if (G[Y][Grid.Width - 1 - X] > Right)
			{
				Visibility[Y][Grid.Width - 1 - X] = true;
				Right = G[Y][Grid.Width - 1 - X];
			}
		}
	}
        // vertical
	for (idx X = 0;
	     X < Grid.Width;
	     ++X)
	{
		idx Top = -1;
		idx Down = -1;
		for (idx Y = 0;
		     Y < Grid.Height;
		     ++Y)
		{
			if (G[Y][X] > Top)
			{
				Visibility[Y][X] = true;
				Top = G[Y][X];
			}
			if (G[Grid.Height - 1 - Y][X] > Down)
			{
				Visibility[Grid.Height - 1 - Y][X] = true;
				Down = G[Grid.Height - 1 - Y][X];
			}
		}
	}

	idx NumVisible = 0;
	for (idx Y = 0;
	     Y < Grid.Height;
	     ++Y)
	{
		for (idx X = 0;
		     X < Grid.Width;
		     ++X)
		{
			NumVisible += Visibility[Y][X];
		}
	}

	fprintf(stderr, "%ld", NumVisible);
	return 0;
}

static inline i8 Extract(__m128i Value, idx Pos)
{
	i8 Data[sizeof(Value)];
	assert(Pos >= 0 && Pos < 10);
	_mm_storeu_si128((__m128i *)Data, Value);
	return Data[Pos];
}

PART(Part2)
{
	str_view View = (str_view) { .Content = Text.Content, .Length = Text.Length };
	grid Grid = ParseInput(View);

	i16 (*Score)[Grid.Width] = malloc(Grid.Height * sizeof(*Score));
	i16 (*VScore)[Grid.Width] = malloc(Grid.Height * sizeof(*Score));
	i8 (*G)[Grid.Width] = (i8 (*)[Grid.Width]) Grid.Grid;
	i8 (*T)[Grid.Width] = calloc(Grid.Height, sizeof(*T));

	for (idx Y = 1;
	     Y < Grid.Height - 1;
	     ++Y)
	{
		for (idx X = 1;
		     X < Grid.Width - 1;
		     ++X)
		{
			T[X][Y] = G[Y][X];
		}
	}
	__m128i Numbers = _mm_setr_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0);
	__m128i One = _mm_set1_epi8(1);
        // horizontal
	for (idx Y = 1;
	     Y < Grid.Height - 1;
	     ++Y)
	{
		// left to right
		{
			__m128i Last = _mm_setzero_si128();
			i8 LastHeight = 0;
			for (idx X = 1;
			     X < Grid.Width - 1;
			     ++X)
			{
				__m128i Height = _mm_set1_epi8(LastHeight);
				LastHeight = G[Y][X];
				__m128i Mask = _mm_cmpgt_epi8(Numbers, Height);
				__m128i New  = _mm_and_si128(Mask, Last);
				__m128i Value = _mm_add_epi8(One, New);
				Score[Y][X] = Extract(Value, LastHeight);
				Last = Value;
			}
		}
		// right to left
		{
			__m128i Last = _mm_setzero_si128();
			i8 LastHeight = 0;
			for (idx X = Grid.Width - 2;
			     X > 0;
			     --X)
			{
				__m128i Height = _mm_set1_epi8(LastHeight);
				LastHeight = G[Y][X];
				__m128i Mask = _mm_cmpgt_epi8(Numbers, Height);
				__m128i New  = _mm_and_si128(Mask, Last);
				__m128i Value = _mm_add_epi8(One, New);
				Score[Y][X] *= Extract(Value, LastHeight);
				Last = Value;
			}
		}
	}
        // vertical
	for (idx X = 1;
	     X < Grid.Width - 1;
	     ++X)
	{
		// top to bottom
		{
			__m128i Last = _mm_setzero_si128();
			i8 LastHeight = 0;
			for (idx Y = 1;
			     Y < Grid.Height - 1;
			     ++Y)
			{
				__m128i Height = _mm_set1_epi8(LastHeight);
				LastHeight = T[X][Y];
				__m128i Mask = _mm_cmpgt_epi8(Numbers, Height);
				__m128i New  = _mm_and_si128(Mask, Last);
				__m128i Value = _mm_add_epi8(One, New);
				VScore[X][Y] = Extract(Value, LastHeight);
				Last = Value;
			}
		}

		// bottom to top
		{
			__m128i Last = _mm_setzero_si128();
			i8 LastHeight = 0;
			for (idx Y = Grid.Height - 2;
			     Y > 0;
			     --Y)
			{
				__m128i Height = _mm_set1_epi8(LastHeight);
				LastHeight = T[X][Y];
				__m128i Mask = _mm_cmpgt_epi8(Numbers, Height);
				__m128i New  = _mm_and_si128(Mask, Last);
				__m128i Value = _mm_add_epi8(One, New);
				VScore[X][Y] *= Extract(Value, LastHeight);
				Last = Value;
			}
		}
	}

	idx Max = 0;
	for (idx Y = 1;
	     Y < Grid.Height - 1;
	     ++Y)
	{
		for (idx X = 1;
		     X < Grid.Width - 1;
		     ++X)
		{
			idx S = Score[Y][X] * VScore[X][Y];
			if (S > Max)
			{
				Max = S;
			}
		}
	}

	fprintf(stderr, "%ld\n", Max);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
