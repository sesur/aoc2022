#include "aoc.h"
#include <x86intrin.h>

typedef struct column
{
	idx Capacity, Size;
	i16 *RowPos;
} column;

column Column_New(idx Capacity)
{
	column New;
	New.Capacity = Capacity;
	New.Size = 0;
	New.RowPos = malloc(Capacity * sizeof(*New.RowPos));
	return New;
}

idx Find(idx Count, i16 Sorted[Count], i16 Val)
{
	idx Start = 0;
	idx End = Count;

	while (Start < End)
	{
		idx Middle = (Start + End) / 2;
		if (Val < Sorted[Middle])
		{
			Start = Middle + 1;
		}
		else if (Val > Sorted[Middle])
		{
			End = Middle;
		}
		else
		{
			return Middle;
		}
	}
	assert(Start == End);

	return Start;
}

idx Column_Find(column *C, i16 Val)
{
	return Find(C->Size, C->RowPos, Val);
}

void Column_InsertAt(column *C, idx Pos, i16 Val)
{
	if (C->Capacity == C->Size)
	{
		C->Capacity *= 2;
		C->RowPos = realloc(C->RowPos, C->Capacity * sizeof(*C->RowPos));
		assert(C->RowPos);
	}

	for (idx I = C->Size;
	     I > Pos;
	     I -= 1)
	{
		C->RowPos[I] = C->RowPos[I-1];
	}

	C->RowPos[Pos] = Val;
	C->Size += 1;
}

void Column_Add(column *C, i16 Val)
{
	idx InsertionPoint = Find(C->Size, C->RowPos, Val);
	if (InsertionPoint == C->Size || C->RowPos[InsertionPoint] != Val)
	{
		Column_InsertAt(C, InsertionPoint, Val);
	}
}

typedef struct grid
{
	idx Middle, Capacity;
	idx Max;
	column *Columns;
} grid;

#define INV_COLUMN (INT16_MAX)
grid Grid_New(idx Capacity, idx Around)
{
	grid New;
	New.Capacity = Capacity;
	New.Middle = Around;
	New.Max = 0;
	New.Columns = calloc(Capacity, sizeof(*New.Columns));
	return New;
}

column *Grid_Get(grid *G, i16 Column)
{
	idx Start = G->Middle - G->Capacity / 2;
	idx Index = Column - Start;
	if (Index >= G->Capacity || Index < 0)
	{
		assert(0);
	}

	if (G->Columns[Index].Capacity == 0)
	{
		G->Columns[Index] = Column_New(100);
	}

	return G->Columns + Index;
}

grid ParseInput(str_view Text)
{
	grid Result = Grid_New(512, 500);

	for (str_view Line = NextLine(&Text);
	     !Empty(Line);
	     Line = NextLine(&Text))
	{
		str_view Start = ReadUntil(&Line, ' ');
		Skip(&Line, 3);
		str_view Num = ReadUntil(&Start, ',');
		idx X = ReadInt(Num);
		idx Y = ReadInt(Start);
		while (!Empty(Line))
		{
			str_view Pair = ReadUntil(&Line, ' ');
			Skip(&Line, 3);
			idx NewX = ReadInt(ReadUntil(&Pair, ','));
			idx NewY = ReadInt(Pair);
			if (NewX == X)
			{
				column *C = Grid_Get(&Result, X);
				idx Min = (Y >= NewY) ? NewY : Y;
				idx Max = Y + NewY - Min;
				if (Max > Result.Max) Result.Max = Max;
				for (idx I = Min;
				     I <= Max;
				     I += 1)
				{
					Column_Add(C, I);
				}
			}
			else
			{
				assert(NewY == Y);
				if (Y > Result.Max) Result.Max = Y;
				idx Min = (X >= NewX) ? NewX : X;
				idx Max = X + NewX - Min;
				for (idx I = Min;
				     I <= Max;
				     I += 1)
				{
					column *C = Grid_Get(&Result, I);
					Column_Add(C, Y);
				}
			}
			X = NewX;
			Y = NewY;
		}
	}

	return Result;
}

PART(Part1)
{
	grid Parsed = ParseInput(Text);
	idx X = 500;
	idx Y = 0;
	idx NumRested = 0;
	column *Spawn = Grid_Get(&Parsed, X);
	column *C = Spawn;
	idx Size = 0;
	i16 *StackX = malloc(Parsed.Max * sizeof(*StackX));
	i16 *StackY = malloc(Parsed.Max * sizeof(*StackY));
#define PUSH(X, Y) do { assert(Size < Parsed.Max); StackX[Size] = (X); StackY[Size++] = (Y); } while (0)
#define POP(X, Y) do { assert(Size > 0); X = StackX[--Size]; Y = StackY[Size]; } while (0)

	PUSH(X, Y);
	while (1)
	{
		idx Found = Column_Find(C, Y);
		if (Found == 0)
		{
			// we are now free falling
			break;
		}
		else
		{
			idx NewY = C->RowPos[Found-1];
			column *Left = Grid_Get(&Parsed, X - 1);
			idx LeftIdx = Column_Find(Left, NewY);
			if (Left->RowPos[LeftIdx] != NewY)
			{
				PUSH(X, Y);
				X -= 1;
				Y = NewY;
				C = Left;
				continue;
			}
			column *Right = Grid_Get(&Parsed, X + 1);
			idx RightIdx = Column_Find(Right, NewY);
			if (Right->RowPos[RightIdx] != NewY)
			{
				PUSH(X, Y);
				X += 1;
				Y = NewY;
				C = Right;
				continue;
			}

			Column_InsertAt(C, Found, NewY - 1);
			NumRested += 1;
			if (Size > 0)
			{
				POP(X, Y);
				C = Grid_Get(&Parsed, X);
			}
			else
			{
				X = 500;
				Y = 0;
				C = Spawn;
			}
		}
	}

	fprintf(stderr, "%ld\n", NumRested);
	return 0;
}

typedef struct span
{
	i16 Start, End;
} span;

typedef struct pair
{
	span Left, Right;
} pair;

pair Split(span Wood, span Axe)
{
	span Left = Wood;
	if (Left.End > Axe.Start) Left.End = Axe.Start;
	span Right = Wood;
	if (Right.Start < Axe.End) Right.Start = Axe.End;

	pair Result = { .Left = Left, .Right = Right };
	return Result;
}
typedef struct row
{
	idx Capacity, Size;
	span *Blocked;
} row;

row Row_New(idx Capacity)
{
	row New;
	New.Capacity = Capacity;
	New.Size = 0;
	New.Blocked = malloc(Capacity * sizeof(*New.Blocked));
	return New;
}

void Row_MaybeEnlarge(row *Row)
{
	if (Row->Size == Row->Capacity)
	{
		Row->Capacity *= 2;
		Row->Blocked = realloc(Row->Blocked, Row->Capacity * sizeof(*Row->Blocked));
	}
}

void Row_Add(row *Row, span Span)
{
	for (idx I = 0;
	     I < Row->Size;
	     ++I)
	{
		span Current = Row->Blocked[I];
		if (Span.Start <= Current.End && Current.Start <= Span.End)
		{
			span Fused = Span;
			if (Current.Start < Fused.Start) Fused.Start = Current.Start;
			if (Current.End > Fused.End) Fused.End = Current.End;
			Row->Blocked[I] = Fused;
			return;
		}
		else if (Span.End < Current.Start)
		{
			// insert here
			Row_MaybeEnlarge(Row);
			for (idx J = Row->Size;
			     J > I;
			     J -= 1)
			{
				Row->Blocked[J] = Row->Blocked[J-1];
			}
			Row->Blocked[I] = Span;
			Row->Size += 1;
			return;
		}
	}

	Row_MaybeEnlarge(Row);
	Row->Blocked[Row->Size] = Span;
	Row->Size += 1;
}

// grit = grid transposed
typedef struct grit
{
	idx Count, Capacity;
	row *Rows;
} grit;

#define INV_ROW (INT16_MAX)
grit Grit_New(idx Capacity)
{
	assert((Capacity & (Capacity - 1)) == 0);
	grit New;
	New.Capacity = Capacity;
	New.Count = 0;
	New.Rows = malloc(Capacity * sizeof(*New.Rows));
	return New;
}

row *Grit_Get(grit *G, i16 Row)
{
	if (Row > G->Capacity)
	{
		G->Capacity = Row * 2;
		G->Rows = realloc(G->Rows, G->Capacity);
		assert(G->Rows);
	}

	if (Row >= G->Count)
	{
		for (idx I = G->Count;
		     I <= Row;
		     ++I)
		{
			G->Rows[I] = Row_New(32);
		}
		G->Count = Row + 1;
	}

	return G->Rows + Row;
}

grit ParseInput2(str_view Text)
{
	grit Result = Grit_New(512);

	for (str_view Line = NextLine(&Text);
	     !Empty(Line);
	     Line = NextLine(&Text))
	{
		str_view Start = ReadUntil(&Line, ' ');
		Skip(&Line, 3);
		str_view Num = ReadUntil(&Start, ',');
		idx X = ReadInt(Num);
		idx Y = ReadInt(Start);
		while (!Empty(Line))
		{
			str_view Pair = ReadUntil(&Line, ' ');
			Skip(&Line, 3);
			idx NewX = ReadInt(ReadUntil(&Pair, ','));
			idx NewY = ReadInt(Pair);
			if (NewX == X)
			{
				span Singleton = { .Start = X, .End = X+1 };
				idx Min = (Y >= NewY) ? NewY : Y;
				idx Max = Y + NewY - Min;
				for (idx I = Min;
				     I <= Max;
				     I += 1)
				{
					row *R = Grit_Get(&Result, I);
					Row_Add(R, Singleton);
				}
			}
			else
			{
				assert(NewY == Y);
				row *R = Grit_Get(&Result, Y);
				idx Min = (X >= NewX) ? NewX : X;
				idx Max = X + NewX - Min;
				span Span = { .Start = Min, .End = Max + 1 };
				Row_Add(R, Span);
			}
			X = NewX;
			Y = NewY;
		}
	}

	return Result;
}

PART(Part2)
{
	idx NumRested = 0;
	grit Parsed = ParseInput2(Text);
	row Current = Row_New(100);
	row Next    = Row_New(100);
	span Singleton = { .Start = 500, .End = 501 };
	Row_Add(&Current, Singleton);
	idx MaxY = Parsed.Count + 1;
	for (idx Y = 1;
	     Y <= MaxY;
	     ++Y)
	{
		idx CurrentBlocker = 0;
		row Blocker = *Grit_Get(&Parsed, Y);
		for (idx I = 0;
		     I < Current.Size;
		     ++I)
		{
			span Sand = Current.Blocked[I];
			NumRested += Sand.End - Sand.Start;
			Sand.Start -= 1;
			Sand.End += 1;
			while (CurrentBlocker < Blocker.Size && Sand.Start < Sand.End)
			{
				span Axe = Blocker.Blocked[CurrentBlocker];
				pair Destroyed = Split(Sand, Axe);
				span Left = Destroyed.Left;
				span Right = Destroyed.Right;

				if (Left.Start < Left.End)
				{
					Row_Add(&Next, Left);
				}

				if (Right.Start < Right.End)
				{
					CurrentBlocker += 1;
				}
				Sand = Right;
			}

			if (Sand.Start < Sand.End)
			{
				Row_Add(&Next, Sand);
			}
		}
		row Tmp = Current;
		Current = Next;
		Next = Tmp;
		Next.Size = 0;
	}

	fprintf(stderr, "%ld\n", NumRested);
	return 0;
}

part *Parts[] = { &Part1, &Part2 };
