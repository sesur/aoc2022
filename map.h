typedef struct map
{
	// assumed to be a power of two
	u32 Shift;
	idx Capacity;
	idx Occupancy;
	idx MaxPSL;
	from *From;
	to *To;
} map;

bool Map_Valid(from F);
bool Map_Eq(from A, from B);
extern from const Map_Invalid;
extern to   const Map_Default;
static inline map Map_New(idx Capacity);
static inline to *Map_Get(map *Map, from F);
static inline bool Map_TryInserting(map *Map, from F);
