typedef struct stack
{
	idx Capacity;
	idx Size;
	item *Items;
} stack;

stack Stack_New(idx Capacity);
void Stack_Push(stack *S, item Item);
item Stack_Pop(stack *S);
bool Stack_Empty(stack S);
